#include <iostream>

using namespace std;

float form(float a, float b){
	float	
		n1 = a + b,
		n2 = n1 * n1,
		n3 = a * a,
		n4 = 2 * a * b,
		n5 = n2 + n4,
		n6 = n2 - n5,
		n7 = b * b;
	return n6/n7;
}

double form(double a, double b){
	double	
		n1 = a + b,
		n2 = n1 * n1,
		n3 = a * a,
		n4 = 2 * a * b,
		n5 = n2 + n4,
		n6 = n2 - n5,
		n7 = b * b;
	return n6/n7;
}

int main(){
	float af = 1000,
	bf = 0.0001;
	double ad = 1000,
	bd = 0.0001;
	cout<<"double: "<< form(ad, bd) << endl;
	cout<<"float: "<<form(af, af)<<endl;
		
	int n, m;
	
	cout<<endl<<"n = ";
	cin>>n;
	cout<<"m = ";
	cin>>m;
	cout << endl;
	cout << "m++ + n = " << (m+++n) << endl;
	cout << "(1 - true, 0 - false)" << endl;
	cout << "m-- > n = " << (m-- > n) << endl;
	cout << "n-- > m = " << (n-- > m) <<endl;
}
