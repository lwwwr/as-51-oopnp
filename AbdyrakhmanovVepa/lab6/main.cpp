﻿#include "pch.h"
#include "Persona.h"
#include "HeadOfDed.h"
#include "Prepod.h"
#include "Student.h"

int main() {
	Student a(19, "oleg", "FEIS", 2);
	Student b(a);
	HeadOfDed c(27, "Shewcow C.A.", "math");
	Prepod d(26, "Sweclow B.A.", "english language");
	a.Push();
	b.Push();
	c.Push();
	d.Push();
	PersonList::ViewList();
}