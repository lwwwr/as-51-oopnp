#include "mainwindow.h"
#include "ui_mainwindow.h"

double rad=10;
double f=100;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
   myUi(new Ui::MainWindow)
{
    myUi->setupUi(this);
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
    timer->start(100);
    setFixedSize(800,800);
}
void MainWindow::paintEvent(QPaintEvent *pEvent)
{

    Q_UNUSED(pEvent);
    QString     nameLab = "Лабораторная работа №8";
    QPainter    task1(this);
    QFont font("Yu Gothic");
    task1.setFont(font);
    task1.translate(5, 10);
    for (int i = 1; i < nameLab.size(); i+=2) {
        nameLab.insert(i, '\n');
    }
    task1.drawText(rect(),Qt::AlignLeft,nameLab);

    QPainter task2(this);


    double pointX = rad * sin(f);
    double pointY = rad * cos(f);
    rad+=25;
    f -= f/100;

    task2.setWindow(-700, -700, 1550, 1550);


    QPen pen;
    pen.setWidth(3);
    pen.setColor(Qt::red);
    task2.setPen(pen);


    QBrush brush, brush2;
    brush.setColor(Qt::green);
    brush.setStyle(Qt::SolidPattern);
    brush2.setColor(Qt::white);
    brush2.setStyle(Qt::SolidPattern);


    QPolygon polygon;
    polygon << QPoint(pointX+0, pointY+85) << QPoint(pointX+75, pointY+75)
             << QPoint(pointX+100, pointY+10) << QPoint(pointX+125, pointY+75)
             << QPoint(pointX+200, pointY+85) << QPoint(pointX+150, pointY+125)
             << QPoint(pointX+160, pointY+190) << QPoint(pointX+100, pointY+150)
             << QPoint(pointX+40, pointY+190) << QPoint(pointX+50, pointY+125)\
             << QPoint(pointX+0, pointY+85);


    QPainterPath path;
    path.addPolygon(polygon);
    task2.drawPolygon(polygon);
    task2.fillPath(path,brush);


    QPainterPath path2;
    path2.addEllipse(QPoint(pointX+100, pointY+110), 10,10);
    task2.fillPath(path2,brush2);
    task2.drawEllipse(QPoint(pointX+100, pointY+110), 10,10);
}

MainWindow::~MainWindow(){
    delete myUi;
}
