# include <iostream>
# include <cstring>

using namespace std;

const int MAX = 255;

void AllSymbols(const char* word, char * rez){
	
	int k=0;
	
	for (int i=0; word[i]!='.'; i++)
		if((i==0||word[i-1]==' ')&&word[i] != ' ')
			for(int j=i; word[j] != ' '&&word[j] != '.'&&word[j] != '.'; j++){
				rez[k]=word[j];
				k++;
			}
	rez[k]='\0';
}

bool IsPolindrom(const char* word){
	char inv[MAX];
	for(int i=0, j=strlen(word)-1; word[i] != '.'; i++, j--)
		inv[j]=word[i];
	inv[strlen(word)]='\0';
	
	cout << word << "(�������� ��� ��������)" << endl;
	cout << inv << "(�����-�������)" << endl<<endl;
	
	for(int i=0;word[i]!='\0';i++)
		if(word[i]!=inv[i])
			return false;
	return true;
}

int main(){
	setlocale(LC_ALL,"rus");
	
	char word[MAX];
	char rez[MAX];
	
	cout<<"������� ����������, �������������� ������:\n";
	cin.getline(word, MAX);
	
	AllSymbols(word, rez);
	
	IsPolindrom(rez) ? cout<<"��������� �������� �����������\n" : cout<<"��������� �� �������� �����������\n";
}
