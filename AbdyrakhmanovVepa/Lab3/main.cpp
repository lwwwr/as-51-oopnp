#include <QString>
#include <QTextStream>
#include <iostream>

QTextStream Qcin(stdin),
            Qcout(stdout);

bool IsPolindrom(QString);


int main()
{
    std :: cout << "Input setntens:\n";
    QString str = Qcin.readLine();

    IsPolindrom(str) ? std :: cout << "Polindrom" : std :: cout << "Not Polindrom";

    return 0;
}

bool IsPolindrom(QString str){
    QStringList alphas = str.split(QRegExp("\\W"));// убираем все лишние знаки и заполняем список по словам
    QString NotRev = alphas.join(""),//получаем исходную строку без пробелов
            Rev(NotRev);

    std::reverse(Rev.begin(), Rev.end());//реверсируем полученную строку и потом сравниваем, приведя к нижнему регистру

    return Rev.toLower() == NotRev.toLower();
}
