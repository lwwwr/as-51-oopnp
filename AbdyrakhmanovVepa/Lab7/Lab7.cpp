﻿#include "pch.h"
#include "ATD.h"
#include <iostream>

void test() {
	using std::cout;
	using std::endl;

	ATD mas1, mas2;
	cout << "mas1: ";
	mas1.inPut();
	cout << "\nmas2: ";
	mas2.inPut();
	cout << endl << endl << "mas1 + 'j': ";
	(mas1 + 'j').print();
	cout << endl << "mas1 + mas2: ";
	(mas1 + mas2).print();
	cout << endl << "mas1 == mas2: " << (mas1 == mas2);
}

int main() {
	test();
}