#pragma once

#include <iostream>

class ATD {
private:
	int size;
	static int MAX_SIZE;
	char *pMas;
	void add(char elem);
public:
	ATD();
	ATD(const ATD&);
	ATD(const char*, int);
	ATD& operator + (char);
	ATD& operator + (const ATD&);
	bool operator == (const ATD&);
	~ATD();
	void inPut();
	void print();
};

int ATD::MAX_SIZE = 225;

ATD::ATD() : pMas(nullptr), size(0) {}

ATD::ATD(const ATD &other) : ATD(other.pMas, other.size) {}

ATD::ATD(const char* pMas, int size) {
	if (this->pMas) delete[] this->pMas;
	this->pMas = new char[size];
	this->size = size;
	for (int i = 0; i < size; i++)
		this->pMas[i] = pMas[i];
}

ATD::~ATD() { delete[] pMas; }

void ATD::add(char elem) {
	char *newMas = new char[size + 1];
	bool push = true;
	for (int i = 0; i < size; i++) {
		if (pMas[i] == elem) push = false;
		newMas[i] = pMas[i];
	}

	if(push) newMas[size++] = elem;
	delete[] pMas;
	pMas = newMas;
}

ATD& ATD::operator + (char elem) {
	ATD *newObj = new ATD(*this);
	newObj->add(elem);
	return *newObj;
}

ATD& ATD::operator + (const ATD& other) {
	ATD *newObj = new ATD(*this);
	for (int i = 0; i < other.size; i++)
		newObj->add(other.pMas[i]);
	return *newObj;
}

bool ATD::operator == (const ATD& other) {
	if (size != other.size) return false;
	bool itsRavno = false;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < other.size; j++)
			if (pMas[i] == other.pMas[j]) {
				itsRavno = true;
				break;
			}
		if (!itsRavno) return false;
		else itsRavno = false;
	}
	return true;
}

void ATD::inPut() {
	char elem;
	while (std::cin >> elem && size < MAX_SIZE) {
		if (elem == '.') return;
		add(elem);
	}
}

void ATD::print() {
	for (int i = 0; i < size; i++)
		std::cout << pMas[i] << " ";
	std::cout << std::endl;
}