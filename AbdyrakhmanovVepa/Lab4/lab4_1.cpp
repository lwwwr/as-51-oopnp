﻿// lab4_1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <fstream>

using namespace std;

int main() {
	setlocale(LC_ALL, "rus");
	ifstream fin("input.txt");
	ofstream fout("output.txt", ios::trunc);
	fin.seekg(0, ios_base::end);
	int size = fin.tellg();
	fin.seekg(0, ios_base::beg);
	char *file = new char[size + 1];
	fin.read(file, size);
	int i = 0;
	int count = 1;
	while (i < size) {
		if (file[i] == '/n') {
			count = 1;
			i++;
			continue;
		}
		if (file[i] == ' ' && (file[i - 1] != ' '|| i - 1 == 0)) {
			int spases = 0;
			while (file[i] == ' ') {
				spases++;
				i++;
				count++;
			}
			int countBottom = count - spases;
			cout << count << " " << spases << " " << countBottom << endl;
			cout << endl << endl;
			int ostatok = countBottom > 8 ? count / 8 : countBottom % 8;
			fout << "\t";
			spases -= ostatok;
			for (int j = 0; j < spases; j++) fout << " ";
		}
		fout << file[i];
		i++;
		count++;
	}
	fout.close();
	fin.close();
	delete[] file;
}