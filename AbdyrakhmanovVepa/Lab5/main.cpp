#include <iostream>
#include <QVector>

void task1() {
    using std :: cout;
    using std :: endl;
    cout << "-------->TASK1\n\n";

    QVector<int> vec1 = {0, 1, 5, 0, 4, 8, -3, 0 -10, 0, 33};
    QVector<int> vec2;

    int elem;

    foreach (elem, vec1)
        if (elem != 0) vec2 += elem;
    cout << "initidl array: ";
    foreach (elem, vec1) cout << elem << " ";
    cout << endl << "filal array: ";
    foreach (elem, vec2) cout << elem << " ";
}

void task2() {
    using std :: cout;
    using std :: endl;
    cout << endl << endl << "-------->TASK2\n\n";

    QVector<int> vec1 = {1,2,3,4};

    int elem, max, min, a, b;
    int rezult = 1;

    if (vec1.isEmpty()) {
        cout << "vector is empty";
        return;
    }
    max = min = vec1.first();
    foreach (elem, vec1) {
        if (max < elem) max = elem; // ������� ������������ ������� � �������
        if (min > elem) min = elem; // �����������
    }
    if (vec1.indexOf(max) < vec1.indexOf(min)) {
        a = vec1.indexOf(max); //�� �������, �� ������� �������� �� ���, ��� ��� ����� �������
        b = vec1.indexOf(min);
    }
    else {
        b = vec1.indexOf(max);
        a = vec1.indexOf(min);
    }

    for (a; a <= b; a++) rezult *= vec1[a]; //�������  ������������
    cout << "initidl array: ";
    foreach (elem, vec1) cout << elem << " ";
    cout << "rezult: " << rezult;
}

int main() {
    task1();
    task2();
    return 0;
}
