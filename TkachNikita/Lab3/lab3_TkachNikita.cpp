#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QTextStream>

using namespace std;
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString str;
    QString massStr[256];
    QTextStream inp(stdin);
    cout <<"Made in QT Creator"<<endl<< "Enter string" << endl;
    str = inp.readLine();
    str += ' ';
    int indexForStr = 0;
    int indexForMassStr = 0;
    while (indexForStr != str.length()) {
        while (str[indexForStr] != ' ') {
            if (indexForStr != str.length()) {
                massStr[indexForMassStr] += str[indexForStr];
                indexForStr++;
            }
            else
                break;
        }
        for (int i = 0; i < massStr[indexForMassStr].size(); i++) {
            if (massStr[indexForMassStr][i].isPunct() && massStr[indexForMassStr][i] != '_') {
                massStr[indexForMassStr] = "";
                indexForMassStr--;
                break;
            }
        }
        if (!massStr[indexForMassStr].isEmpty()) {
            if (!massStr[indexForMassStr][0].isLetter() ) {
                if(massStr[indexForMassStr][0] != '_'){
                    massStr[indexForMassStr] = "";
                    indexForMassStr--;
                }
            }
        }
        indexForMassStr++;
        indexForStr++;
    }
    string str2;
    for (int i = 0; i < sizeof(massStr)/sizeof(massStr[0]); i++){
        if (!massStr[i].isEmpty())
            str2.append(massStr[i].toStdString() + ' ');
        else
            break;
    }
    cout << str2 << endl;
    return a.exec();
}