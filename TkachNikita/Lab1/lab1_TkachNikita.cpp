#include "stdafx.h"
#include <iostream> 
#include <math.h>

using namespace std;

int main(){
	// task 1
	float a = 1000, b = 0.0001;
	float q, w, e, r, t, numerator1, denominator1, result1;
	q = pow((a - b), 3);
	w = pow(a, 4) + 4*pow(a,3)*b;
	e = 6 * pow(a, 2)*pow(b, 2);
	r = 4 * a*pow(b, 3);
	t = pow(b, 4);
	numerator1 = q - w;
	denominator1 = e + r + t;
	result1 = numerator1 / denominator1;
	cout << "result with float =" <<result1 << endl;

	double c = 1000, d = 0.0001;
	double z, x, v, i, o, numerator2, denominator2, result2;
	z = pow((c - d), 3);
	x = pow(c, 4) + 4 * pow(c, 3)*d;
	v = 6 * pow(c, 2)*pow(d, 2);
	i = 4 * a*pow(d, 3);
	o = pow(d, 4);
	numerator2 = z - x;
	denominator2 = v + i + o;
	result2 = numerator2 / denominator2;
	cout <<"result with double ="<< result2 << endl;
	// task 2
	cout << "Enter 2 numbers" << endl;
	int n=0, m=0;
	cin >> n >> m;
	cout << "Result expressions" << endl;
	int l = ++n*++m;
	cout <<"Expression ++n*++m = "<< l<<endl;
	int k = m++ << n;
	cout << "Expression m++<<n = " << k << endl;
	int j = n++ >> m;
	cout << "Expression n++>>m = " << j << endl;
	system("pause");
	return 0;
}

