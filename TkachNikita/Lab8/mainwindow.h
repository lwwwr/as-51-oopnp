﻿#pragma once

#include <QMainWindow>
#include "QPainter"
#include "QPen"
#include "QTimer"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *pParent = nullptr);
    ~MainWindow();
    void    paintEvent(QPaintEvent *pEvent);

private:
    Ui::MainWindow  *pUi;
    QTimer          *pTimer;
};
