﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

double gCoordinate = 7;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    pUi(new Ui::MainWindow)
{
    pUi->setupUi(this);
    setFixedSize(600,600);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
    pTimer->start(100);
}

void MainWindow::paintEvent(QPaintEvent *pEvent) {

    QString     nameLab = " LABORATORY  WORK  8";
    QFont       font;
    QPainter    label(this);
    font.setPixelSize(20);
    font.setBold(true);
    label.setFont(font);
    label.translate(5, 10);
    for (int i = 1; i < nameLab.size(); i+=2) {
        nameLab.insert(i, '\n');
    }
    label.drawText(rect(),Qt::AlignLeft,nameLab);

    QMatrix matrix;
    matrix.scale(7, 7);
    QPainter painter(this);

    double pointX = gCoordinate*3;
    double pointY = -pow(pointX,3)*0.1/5;
    gCoordinate = gCoordinate - 0.5;

    painter.setWindow(-600, -600, 1200, 1200);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setMatrix(matrix);
    painter.setPen(QPen(Qt::black, 1));
    painter.setBrush(Qt::white);

    QRect redRectangle(pointX, pointY, 20, 40);
    redRectangle.moveTo(QPoint(pointX, pointY));
    painter.drawRect(redRectangle);
    painter.setBrush(Qt::red);
    painter.translate(QPoint(30, 30));
    painter.drawEllipse(QPoint(pointX, pointY), 10,10);
    painter.setBrush(Qt::NoBrush);
    painter.translate(-300, -300);
    QRect presentationArea(0, 0, 200, 200);
    if (redRectangle.bottom() > presentationArea.bottom()) {
        gCoordinate = 7;
    }

}

MainWindow::~MainWindow()
{
    delete pUi;
}
