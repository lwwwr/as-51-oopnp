﻿#include "stdafx.h"
#include "Goods.h"
#include "DairyProduct.h"
#include "Toy.h"
#include "ProductList.h"

void Task();

int main() {
	Task();
	system("pause");
	return 0;
}

void Task() {
	std::cout << "Grocery list:" << std::endl << std::endl;
	DairyProduct milk("Burenka", "Russia", 2.5, "Milk");
	milk.Add();
	Goods bed("Skrip", "Kazahstan", 240.0, "Tree");
	bed.Add();
	Goods chair("Skral", "Ukraine", 37.0, "Tree");
	chair.Add();
	Toy bear("Mishka", "Belarus", 50.0, "Cloth", "Soft");
	bear.Add();
	Toy cards("Tuz", "Poland", 3.0, "Paper", "Solid");
	cards.Add();
	DairyProduct sourСream("Domashnjaja", "Russia", 1.4, "SourCream");
	sourСream.Add();
	Toy toy;
	toy.Add();
	DairyProduct dairy;
	dairy.Add();
	ProductList::View();
}

