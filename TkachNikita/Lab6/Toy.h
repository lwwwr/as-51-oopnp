﻿#pragma once
#include <iostream>
#include <string>
#include "Goods.h"
#include "ProductList.h"

class Toy : public Goods {
	protected:
		std::string aTypeToy;
	public:
		Toy();
		Toy (
			std::string, 
			std::string, 
			float, 
			std::string, 
			std::string
		);
		Toy(const Toy&);
		~Toy();
		void Show();
		void Add(); 
};

Toy::Toy() { aTypeToy = "ConstructorToy"; }
Toy::Toy(
		std::string		name, 
		std::string		madeIn, 
		float			price, 
		std::string		material, 
		std::string		typeToy
) : Goods (
		name, 
		madeIn, 
		price, 
		material
) {
	aTypeToy = typeToy;
}
Toy::Toy(const Toy& rToy) { }
Toy::~Toy() { }
void Toy::Show() {
	std::cout << "This is a Toy {" << std::endl	
		<< "\tName->\t\t\t" << Product::aName
		<< "\n\tmade in->\t\t" << Product::aMadeIn
		<< "\n\tprice->\t\t\t" << Product::aPrice
		<< "\n\tmaterial->\t\t" << Goods::aMaterial
		<< "\n\ttype of toys->\t\t" << this->aTypeToy 
		<< "\n}" << std::endl;
}
void Toy::Add() {
	list *pTemp = new list;
	pTemp->pData = new Toy(
		aName,
		aMadeIn,
		aPrice,
		aMaterial,
		aTypeToy
	);
	pTemp->pNext = nullptr;
	if (ProductList::asList.GetHead() != nullptr) {
		ProductList::asList.GetTail()->pNext = pTemp;
		ProductList::asList.SetTail(pTemp);
	} else {
		ProductList::asList.SetHead(pTemp);
		ProductList::asList.SetTail(pTemp);
	}
}