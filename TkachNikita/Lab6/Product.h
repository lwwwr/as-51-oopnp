﻿#pragma once
#include <iostream>
#include <string>

class Product {	
	protected:
		std::string		aName;
		std::string		aMadeIn;		
		float			aPrice;			
	public:
		Product();
		Product (
			std::string, 
			std::string, 
			float
		);
		Product(const Product&);
		~Product(); 
		virtual void Show() = 0;
		virtual void Add()	= 0;

};

Product::Product() { 
	aName	= "ConstructorProduct";
	aMadeIn	= "ConstructorProduct";
	aPrice	= -1.0f;
}
Product::Product(std::string name, std::string madeIn, float price) {
	aName	= name;
	aMadeIn	= madeIn;
	aPrice	= price;
}
Product::Product(const Product& rProduct) { }
Product::~Product() { }