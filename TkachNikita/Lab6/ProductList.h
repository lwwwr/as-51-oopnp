﻿#pragma once
#include <iostream>

struct list {
	Product		*pData;
	list		*pNext;
};

class ProductList {
	private:
		list			*apTail;
		static list		*aspHead;
	public:		
		static ProductList		asList;

		ProductList();	  
		ProductList(const ProductList&);
		~ProductList();
		list		*GetHead() const;
		void		SetHead(list*);
		list		*GetTail() const;
		void		SetTail(list*);
		static void	View();				
};

ProductList		ProductList::asList			= { };
list			*ProductList::aspHead		= nullptr;
ProductList::ProductList() {  
	aspHead = nullptr;
	apTail = nullptr;
}
ProductList::ProductList(const ProductList& rProductList) { }
ProductList::~ProductList() {
	while (aspHead != nullptr) {
		list *pTemp = aspHead->pNext;
		delete aspHead;
		aspHead = pTemp;
	}
}
list	*ProductList::GetHead()	const { return aspHead; }
void	ProductList::SetHead(list *pHead) { this->aspHead = pHead; }
list	*ProductList::GetTail()	const		{ return apTail; }
void	ProductList::SetTail(list *pTail)	{ this->apTail = pTail; }
void	ProductList::View() {
	if (nullptr == ProductList::aspHead) {
		std::cout << "No items in list!" << std::endl;
	} else {
		list *pTemp = new list;
		pTemp = ProductList::aspHead;
		while (pTemp != nullptr) {
			pTemp->pData->Show();
			pTemp = pTemp->pNext;
		}
	}
}