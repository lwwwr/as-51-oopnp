﻿#pragma once
#include <iostream>
#include <string>
#include "Product.h"
#include "ProductList.h"

class DairyProduct : public Product {	
	protected:
		std::string aTypeDairyProduct;
	public:
		DairyProduct();
		DairyProduct (
			std::string, 
			std::string, 
			float, 
			std::string
		);
		DairyProduct(const DairyProduct&);
		~DairyProduct();
		void Show();
		void Add();
};

DairyProduct::DairyProduct() { 
	aTypeDairyProduct = "ConstructorDairyProduct"; 
}
DairyProduct::DairyProduct(
		std::string		name, 
		std::string		madeIn, 
		float			price, 
		std::string		typeDairyProduct
) : Product (
		name, 
		madeIn, 
		price
) {
	aTypeDairyProduct = typeDairyProduct;
}
DairyProduct::DairyProduct(const DairyProduct& rDairyProduct) { }
DairyProduct::~DairyProduct() { }
void DairyProduct::Show() {
	std::cout << "This is a Dairy Product {" << std::endl	
		<< "\tName->\t\t\t" << Product::aName
		<< "\n\tmade in->\t\t" << Product::aMadeIn
		<< "\n\tprice->\t\t\t" << Product::aPrice
		<< "\n\ttype of dairy product->\t"<< this->aTypeDairyProduct
		<<	"\n}" << std::endl;
}
void DairyProduct::Add() {
	list *pTemp = new list;
	pTemp->pData = new DairyProduct(
		aName,
		aMadeIn,
		aPrice,
		aTypeDairyProduct
	);
	pTemp->pNext = nullptr;
	if (ProductList::asList.GetHead() != nullptr) {
		ProductList::asList.GetTail()->pNext = pTemp;
		ProductList::asList.SetTail(pTemp);
	}
	else {
		ProductList::asList.SetHead(pTemp);
		ProductList::asList.SetTail(pTemp);
	}
}