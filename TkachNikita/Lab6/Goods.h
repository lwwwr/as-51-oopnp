﻿#pragma once
#include <iostream>
#include <string>
#include "Product.h"
#include "ProductList.h"

class Goods : public Product {	
	protected:
		std::string aMaterial;
	public:
		Goods();
		Goods (
			std::string, 
			std::string, 
			float, 
			std::string
		);
		Goods(const Goods &);
		~Goods();
		void Show();
		void Add();	
};

Goods::Goods() { aMaterial = "ConstructorGoods"; }
Goods::Goods (
		std::string		name, 
		std::string		madeIn, 
		float			price, 
		std::string		material
) : Product (
		name, 
		madeIn, 
		price
) {
	aMaterial = material;
}
Goods::Goods(const Goods& rGoods) { }
Goods::~Goods() { }
void Goods::Show() {
	std::cout << "This is a Goods {" << std::endl
		<< "\tName->\t\t\t" << Product::aName
		<< "\n\tmade in->\t\t" << Product::aMadeIn
		<< "\n\tprice->\t\t\t" << Product::aPrice
		<< "\n\tmaterial->\t\t" << this->aMaterial 
		<< "\n}" << std::endl;
}
void Goods::Add() {
	list *pTemp	= new list;
	pTemp->pData = new Goods(
		aName,
		aMadeIn,
		aPrice,
		aMaterial
	);
	pTemp->pNext = nullptr;
	if (ProductList::asList.GetHead() != nullptr) {
		ProductList::asList.GetTail()->pNext = pTemp;
		ProductList::asList.SetTail(pTemp);
	}
	else {
		ProductList::asList.SetHead(pTemp);
		ProductList::asList.SetTail(pTemp);
	}
}