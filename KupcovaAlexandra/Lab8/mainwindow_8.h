﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();
	void paintEvent(QPaintEvent *event);

protected:
	void changeEvent(QEvent *e);

private:
	Ui::MainWindow *ui;
	QTimer *pTimer;
	double x=0,y=0,r=1;
	int dx = 3000, dy = 3000, dr = 1;
};

#endif // MAINWINDOW_H
