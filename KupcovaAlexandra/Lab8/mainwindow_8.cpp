﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	{
		ui->setupUi(this);
		setFixedSize(600,600);
		pTimer = new QTimer();
		connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
		pTimer->setInterval(100);
		pTimer->start();
	}
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}
void MainWindow::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
	QFont font;
	font.setBold(true);
	font.setPixelSize(25);
	p.setFont(font);
	p.setPen( QPen( Qt::blue, 3 ) );
	p.rotate(90);
	p.drawText(QPoint(10,-20),"QTPainter");
	p.rotate(-90);

	x = sin(dx)*dx*0.1;
	y = cos(dy)*dy*0.1;
	r = 1;
	if(dx < 5)
		dx = dy = 0;
	dx-=20;
	dy-=20;
	QRect r1(0, 0, 20, 60);
	QRect r2(0, 60, 40, 20);
	p.setPen( QPen( Qt::red, 3 ) );
	r1.moveTo(QPoint(x + 310, y + 300));
	r2.moveTo(QPoint(x + 300, y + 360));//x+300, y+300
	p.drawRect(r1);
	p.setPen( QPen( Qt::green, 3 ) );
	p.drawRect(r2);

}
