#pragma once
#include<iostream>
class Lab7 {
private:
	static int MAX_SIZE;
	int Size;
	char *myArr;
public:
	operator int();
	Lab7 operator + (char);
	Lab7 operator * (const Lab7&);
	void Print();
	void Input();
	void Add(char);
	bool check(char) const;
	Lab7();
	Lab7(const Lab7&);
	~Lab7();
};

int Lab7::MAX_SIZE = 225;

Lab7::Lab7() : myArr(nullptr), Size(0) {}
Lab7::Lab7(const Lab7 &oth) {
	Size = oth.Size;
	myArr = new char[Size];
	for (int i = 0; i < Size; i++)
		myArr[i] = oth.myArr[i];
}
bool Lab7::check(char ch) const{
	for (int i = 0; i < Size; i++)
		if (myArr[i] == ch) return 1;
	return 0;
}
Lab7::~Lab7() {
	delete[] myArr;
}
void Lab7::Add(char ch) {
	if (check(ch) || Size == MAX_SIZE) return;
	char *pTmp = new char[Size + 1];
	for (int i = 0; i < Size; i++)
		pTmp[i] = myArr[i];
	pTmp[Size++] = ch;
	delete[] myArr;
	myArr = pTmp;
}
void Lab7::Input() {
	char ch;
	while (std::cin >> ch && Size < MAX_SIZE && ch != '.')
		Add(ch);
}
void Lab7::Print() {
	for (int i = 0; i < Size; i++)
		std::cout << myArr[i] << " ";
	std::cout << std::endl;
}
Lab7::operator int() {
	return Size;
}
Lab7 Lab7::operator+(char ch) {
	Lab7 newSet;
	for (int i = 0; i < Size; i++)
		newSet.Add(myArr[i]);
	newSet.Add(ch);
	return newSet;
}
Lab7 Lab7::operator*(const Lab7 &oth) {
	Lab7 newSet;
	for (int i = 0; i < Size; i++)
		for (int j = 0; j < oth.Size; j++)
			if (myArr[i] == oth.myArr[j])
				newSet.Add(myArr[i]);
	return newSet;
}