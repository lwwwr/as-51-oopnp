
#include <iostream>

using namespace std;

void Words(char* str) {
    char* start = str;
    int c = 0;    //счетчик цифр

    while (true) {

        if (*str >= '0' && *str <= '9') {
            c++;
            str++;
            continue;
        }


        if (*str == ' ' || *str == '\0') {

            if (c == 1) { //в случае в слове 1-ой цифры

                while (start < str) {  //выводим слово
                    cout << *start;
                    start++;
                }
                cout <<"; ";
            }

            if (*str == '\0')  //завершение поиска по нахождению конца строки
                break;

            Words(++str); //поиск следующего слова
            break;
        }

        str++;
    }
}

int main()
{
    char str[256];
    cout << "Enter words: ";
    cin.getline(str, 255);
    str[255] = '\0';
    cout << endl;
    cout<<"Received string (255 characters): "<<str<<endl;
    cout << endl;
    cout<<"Result: ";
    Words(str);

}
