#include <iostream>
#include <QVector>
#include <cmath>

void task1();
void task2();
using namespace std;
int main()
{
    std::cout << "Task 1" << std::endl;
    task1();
    std::cout << std::endl << "Task 2" << std::endl;
    task2();
    return 0;
}

void task1(){
    QVector<int> numbers{1,2,-3,4,5,6,-7,9,10};
    int size = numbers.size();
    QVector<int> resNumbers(size, 0);
    auto iter = numbers.begin();
    auto resIter = resNumbers.begin();
        for (int i = 0; i < size / 2+1; i++) {
            if (i == size/2) {
                *resIter = *iter;
                cout << *resIter << endl;
                ++resIter;
            } else {
                *resIter = *iter;
                cout << *resIter << endl;
                iter += 2;
                ++resIter;
            }
        }
        iter = numbers.begin();
        for (int i = 0; i < size/2; i++) {
            if (i == 0) {
                ++iter;
            }
            else if (i == size / 2 -1) {
                iter += 2;
                *resIter = *iter;
                cout << *resIter << endl;
                ++resIter;
            }
            else {
                *resIter = *iter;
                cout << *resIter << endl;
                iter += 2;
                ++resIter;
            }
        }
    }

void task2(){

    QVector<int> numbers{ 4,-12, 8, 3, 6, 11, 4, -2, 5 };
        auto iterI = numbers.begin();
        auto iterHighest = numbers.begin();
        auto iterSmallest = numbers.begin();
        int result = 1;
        for (iterI; iterI != numbers.end();++iterI) {
            if (abs(*iterHighest) < abs(*iterI)) {
                iterHighest = iterI;
            }
        }
        iterI = numbers.begin();
        for (iterI; iterI != numbers.end(); ++iterI) {
            if (abs(*iterSmallest) > abs(*iterI)) {
                iterSmallest = iterI;
            }
        }
        if (iterHighest > iterSmallest) {
            --iterHighest;
            while (iterHighest != iterSmallest) {
                result = result * *iterHighest;
                --iterHighest;
            }
        }
        else if (iterHighest < iterSmallest) {
            ++iterHighest;
            while (iterHighest != iterSmallest) {
                result = result * *iterHighest;
                ++iterHighest;
            }
        }
        cout << result << endl;
}



















