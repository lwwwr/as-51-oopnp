#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Product {
protected:
	static int start;
	static void counterStart();
	virtual void setName() = 0;
	virtual void setID() = 0;
	virtual void setMass() = 0;
	virtual void setDate() = 0;
	virtual void setPrice() = 0;
	virtual void show() = 0;
	virtual void add() = 0;
	virtual void check() = 0;
};

class Unit : public Product {
protected:
	/*static int begin;
	static void counterBegin();*/
	vector<double> Price;
	vector<double> Mass;
	vector<int> Year;
	vector<int> Month;
	vector<int> Day;
	vector<int> ID;
	vector<string> Name;
	double mass;
	double price;
	int id;
	int year;
	int month;
	int day;
	string name;
	//private:
	void setName() override {
		Name.push_back(name);
	}
	void setID() override {
		ID.push_back(id);
	}
	void setMass() override {
		Mass.push_back(mass);
	}
	void setDate() override {
		Year.push_back(year);
		Month.push_back(month);
		Day.push_back(day);
	}
	void setPrice() override {
		Price.push_back(price);
	}
	void show() override {
		cout << "Name: " << Name[start] << "\tPrice($): " << Price[start] << "\nMass(kg): " << Mass[start]
			<< "\nID: " << ID[start] << "\t\tData: " << Day[start] << "/" << Month[start] << "/" << Year[start] << endl;
	}
	void add() override {

		cout << "Name: ";
		cin >> name;
		setName();

		cout << "Price($): ";
		cin >> price;
		setPrice();

		cout << "Mass(kg): ";
		cin >> mass;
		setMass();

		cout << "ID: ";
		cin >> id;
		setID();

		cout << "Year: ";
		cin >> year;
		cout << "Month: ";
		cin >> month;
		cout << "Day: ";
		cin >> day;
		setDate();
	}
public:
	Unit(double price, double mass, int id, int year, int month, int day, string name) {
		this->price = price;
		this->year = year;
		this->month = month;
		this->day = day;
		this->name = name;
		this->mass = mass;
		this->id = id;
		setName();
		setID();
		setMass();
		setDate();
		setPrice();
	}
	void check() override {
		int allWork = 1;
		int addWork = 0;
		int test = 0;
		while (allWork != 0) {
			for (string n : Name) {
				cout << "~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
				show();
				counterStart();
				cout << "~~~~~~~~~~~~~~~~~~~~~~" << endl;
			}
			start = 0;
			cout << " enter 1 to Add or 0 to end: ";
			cin >> addWork;
			if (addWork == 0) {
				allWork = addWork;
			}
			else {
				while (addWork != 0) {
					add();
					cout << "Enter 1 to continue or 0 to Show: ";
					cin >> addWork;
					test++;
				}
			}
		}
	}
};

class Detail final : public Unit {
protected:
	vector<string> Material;
	string material;
	void setName() override {
		Unit::setName();
		Material.push_back(material);
	}
	void setID() override {
		Unit::setID();
	}
	void setMass() override {
		Unit::setMass();
	}
	void setDate() override {
		Unit::setDate();
	}
	void setPrice() override {
		Unit::setPrice();
	}
	void show() override {
		Unit::show();
		cout << "\t\tMaterial:" << Material[start] << endl;
	}
	void add() override {
		cout << "Name: ";
		cin >> name;
		cout << "Material: ";
		cin >> material;
		setName();

		cout << "Price($): ";
		cin >> price;
		setPrice();

		cout << "Mass(kg): ";
		cin >> mass;
		setMass();

		cout << "ID: ";
		cin >> id;
		setID();

		cout << "Year: ";
		cin >> year;
		cout << "Month: ";
		cin >> month;
		cout << "Day: ";
		cin >> day;
		setDate();
	}
public:
	Detail(double price, double mass, int id, int year, int month, int day, string name, string material) :Unit(price, mass, id, year, month, day, name) {
		this->material = material;
		Material.push_back(material);
	}
	void check() override {
		/*int allWork = 1;
		int addWork = 0;
		int test = 0;
		while (allWork != 0) {
			for (string n : Material) {
				cout << "~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
				show();
				counterStart();
				cout << "~~~~~~~~~~~~~~~~~~~~~~" << endl;
			}
			start = 0;
			cout << " enter 1 to AddInf or 0 to end: ";
			cin >> addWork;
			if (addWork == 0) {
				allWork = addWork;
			}
			else {
				while (addWork != 0) {
					add();
					cout << "Enter 1 to continue or 0 to ShowInf: ";
					cin >> addWork;
					test++;
				}
			}
		}*/
		Unit::check();
	}
};

class Mechanism final : public Product {
protected:
	vector<string> Purpose;
	vector<string> Name;
	vector<string> SerialNumber;
	vector<double> Size;
	vector<double> Price;
	vector<int> Year;
	vector<int> Month;
	vector<int> Day;
	double price;
	double size;
	string serialNumber;
	string purpose;
	string name;
	int year;
	int month;
	int day;
private:
	void setName() override {
		Name.push_back(name);
	}
	void setID() override {
		SerialNumber.push_back(serialNumber);
	}
	void setMass() override {
		Size.push_back(size);
	}
	void setDate() override {
		Year.push_back(year);
		Month.push_back(month);
		Day.push_back(day);
	}
	void setPrice() override {
		Price.push_back(price);
	}
	void show() override {
		cout << "\t\t" << Name[start] << "\n\t\t" << SerialNumber[start] << "\n" << Size[start] << "\t\t\t"
			<< Price[start] << "\n\n\t\t\t\t" << Year[start] << "." << Month[start] << "." << Day[start] << endl;
	}
	void add() override {

		cout << "\tName: ";
		cin >> name;
		setName();

		cout << "\tSerial Number: ";
		cin >> serialNumber;
		setMass();

		cout << "Size: ";
		cin >> size;
		setID();

		cout << "Price: ";
		cin >> price;
		setPrice();

		cout << "\tYear: ";
		cin >> year;
		cout << "\tMonth: ";
		cin >> month;
		cout << "\tDay: ";
		cin >> day;
		setDate();
	}
public:
	Mechanism(double price, double size, string serialNumber, string purpose, string name,
		int year, int month, int day) {
		this->name = name;
		this->price = price;
		this->size = size;
		this->serialNumber = serialNumber;
		this->purpose = purpose;
		this->year = year;
		this->month = month;
		this->day = day;
		setName();
		setID();
		setMass();
		setDate();
		setPrice();
	}
	void check() override {
		int allWork = 1;
		int addWork = 0;
		int test = 0;
		while (allWork != 0) {
			for (string n : Name) {
				cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
				show();
				counterStart();
				cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			}
			start = 0;
			cout << " enter 1 to Add or 0 to end: ";
			cin >> addWork;
			if (addWork == 0) {
				allWork = addWork;
			}
			else {
				while (addWork != 0) {
					add();
					cout << "Enter 1 to continue or 0 to Show: ";
					cin >> addWork;
					test++;
				}
			}
		}
	}
};

int Product::start = 0;
void Product::counterStart() {
	start++;
}
int main() {
	int work = 1;
	int choiseAddShow = 0;
	Unit first(15.2, 34.6, 20180, 2018, 10, 18, "Unit");
	Detail second(15.2, 34.6, 20180, 2018, 10, 18, "Detail_1", "Metall");
	Mechanism third(12, 12, "AABBC", "Privet", "Mechanism_1", 10, 12, 11);
	while (work != 0) {
		cout << "Enter 0 to cheak Unit, 1 to cheak Detail and 2 to cheak Mechanism: ";
		cin >> choiseAddShow;
		switch (choiseAddShow) {
		case 0:
			first.check();
			break;
		case 1:
			second.check();
			break;
		case 2:
			third.check();
			break;
		}
		cout << "Enter 1 to continue or 0 to end: ";
		cin >> work;
	}
	system("pause");
	return 0;
}