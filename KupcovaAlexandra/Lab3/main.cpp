#include <iostream>
#include <QString>
#include <QStringList>
#include <QTextStream>
QTextStream Qcout(stdout);
QTextStream Qcin(stdin);

using namespace std;

void words(QString in)
{
    int k = 0;
    QStringList words = in.split(' ');
    for (int i = 0; i<words.count(); i++){
        for(int j = 0; j < words.at(i).length(); j++)
            if(words.at(i)[j].isDigit())
                k++;
        if(k<1||k>1)
                words[i].clear();
        k = 0;
    }
    for(int i = 0; i< words.count(); i++)
        Qcout << words.at(i) << " ";
}

int main()
{
    QString str;
    cout << "Enter words: ";
    Qcin.readLineInto(&str,255);
    cout << endl;
    cout<<"Result: ";
    words(str);

}
