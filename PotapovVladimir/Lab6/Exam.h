#ifndef Exam_h
#define Exam_h
#include "Smth.h"

class Exam : public Trial {
protected:
    int aMark;
public:
    Exam() {
        Exam("default", 0);
    }
    Exam(string newname, int mark) : Trial(newname) {
        aMark=mark;
    }
    Exam(const Exam &exam) {
        Exam(exam.aName, exam.aMark);
    }
    ~Exam() {
        
    }
    void PrintInfo() {
        cout << "Exam          -> " << aName << endl;
        cout << "Mark/question -> " << aMark << endl;
    }
};
#endif
