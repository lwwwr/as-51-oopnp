#ifndef Test_h
#define Test_h
#include "Smth.h"

class Test :
public Trial {
private:
    int aQuestionQuantity;
public:
    Test() {
        Test("default", 0);
    }
    Test(const Test &test) {
        Test(test.aName, test.aQuestionQuantity);
    }
    Test(string newname, int newquantity) : Trial (newname) {
        aQuestionQuantity = newquantity;
    }
    ~Test() {}
    void PrintInfo() {
        cout << "Test              -> " << aName << endl;
        cout << "Question quantity -> " << aQuestionQuantity << endl;
    }
};
#endif


