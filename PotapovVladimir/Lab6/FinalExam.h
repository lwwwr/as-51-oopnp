#ifndef FinalExam_h
#define FinalExam_h
#include "Exam.h"
class FinalExam : public Exam {
protected:
    int aPassingScore;
public:
    FinalExam(const FinalExam &finalexam) {
        FinalExam(finalexam.aName, finalexam.aMark, finalexam.aPassingScore);
    }
    FinalExam(string newname, int newqant, int newpassingScore) {
        Exam(newname, newqant);
        aPassingScore = newpassingScore;
    }
    FinalExam() {
        FinalExam("default", 0,0);
    }
    ~FinalExam() {}
    void PrintInfo() {
        cout << "Final exam    -> " << aName <<endl;
        cout << "Mark/question -> " << aMark <<endl;
        cout << "Passing score -> " << aPassingScore << endl;
    }
};
#endif
