#ifndef Smth_h
#define Smth_h
#include "iostream"
#include <string>
using namespace std;

class Trial {
protected:
    string aName;
public:
    Trial() : Trial ("default") {};
    Trial(string name) {
        aName = name;
    }
    Trial(const Trial &trial) : Trial (trial.aName) {};
    ~Trial() {
        
    }
    virtual void PrintInfo() = 0;
    void Add();
};

struct Node {
public:
    Node *apNext;
    Trial *apTrial;
    Node(Trial *trial) {
        apNext = nullptr;
        apTrial = trial;
    }
    Node() {
        apNext=nullptr;
        apTrial=nullptr;
    }
};

class TrialList {
    friend Trial;
private:
    static Node *head;
    static void AddInList(Trial*);
public:
    static void View();
};

Node* TrialList::head = nullptr;
void TrialList::View() {
        Node *pTemp = head;
        while (pTemp) {
                pTemp->apTrial->PrintInfo();
                pTemp = pTemp->apNext;
            }
    }
void Trial::Add() {
    TrialList::AddInList(this);
}
void TrialList::AddInList(Trial *el) {
    Node *pTemp = new Node(el);
    if (!head) head = pTemp;
    else {
        pTemp->apNext = head;
        head = pTemp;
    }
}

#endif
