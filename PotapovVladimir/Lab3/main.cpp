#include <QCoreApplication>
#include <qstring.h>
#include <string>
#include <iostream>
using namespace std;

void swapping (QString &s,const int &i,const int &j)
{
    QString buf;
    buf[0] = s[i];
    s[i] = s[j];
    s[j] = buf [0];
}

void sort (QString &s, int len)
{
    for(int i=0; i < len-1; i++)
    {
        if(s[i].isLetter())
        {
            for(int j= i+1; j< len; j++)
            {
                if(s[j].isLetter())
                if(s[j] < s[i])
               {
                    swapping(s, i,j);
               }
            }
        }
    }
}

void cleaningup (QString &s, int len)
{
    QString s1;
    int newlen=0;
    for(int j=0, i=0; i < len;j++, i++)
    {
        if(s[i].isLetter())
        {
            s1[j] = s[i];
            newlen = j+1;
        }
        else
        {
            j--;
            newlen = j+1;
        }
    }
    sort (s1,newlen);
    cout << "finally -> " << s1.toStdString() << endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString s,n;
    char str[256];
    int len;
    cout << "source -> ";
    cin.getline (str, 256, '.');
    s = str;
    len = s.length();
    cleaningup (s,len);
    return a.exec();
}
