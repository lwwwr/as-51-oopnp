#include <iostream>
#include <fstream>
using namespace std;

int cmp (const string &s1, const string &s2)
{
    int i=0,j=0;
    while (i < s1.length() && j < s2.length())
    {
        if (s1[i] != s2[j])
        {
            return (i+1);
        }
        i++;
        j++;
    }
    return 0;
}

int main()
{
    ifstream file1("input1.txt");
    ifstream file2("input2.txt");
    string s1,s2;
    int l=0,ch=0,k=0;
    
    if (!file1.is_open() || !file2.is_open())
    {
        cout << "File opening fail" << endl;
        return 0;
    }
    
    while (file1 && file2)
    {
        l++;
        std::getline(file1,s1);
        std::getline(file2,s2);
        
        ch = cmp (s1,s2);
        if (ch)
        {
            k++;
            break;
        }
    }
    file1.close();
    file2.close();
    
    if(k)
    {
        cout << "Несовпадение начинается с " << l << " строки " << ch << " символа"<< endl;
    }
    else
    {
        cout << "Строки идентичны" << endl;
    }
    
    
    return 0;
}
