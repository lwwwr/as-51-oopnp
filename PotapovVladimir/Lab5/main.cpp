#include <QCoreApplication>
#include <QVector>
#include <iostream>
using namespace std;
void firsttask ()
{
    cout << "First task" << endl;
    //input part
    QVector <int> vec = {6,7,4,3,0,8,0,87,56,0,6,43,3,0,0,0,8,7};
    QVector <int> numpart, otherpart;
    cout << "source -> ";
    foreach(int character, vec)
        cout << character << " ";
    cout << endl;

    //processing part
    foreach(int character, vec)
    {
        if(character != 0)
        {
            otherpart += character;
        }
        else
        {
            numpart += character;
        }
    }
    cout << "num -> ";
    foreach(int character, numpart)
        cout << character << " ";
    cout << endl;
    cout << "other -> ";
    foreach(int character, otherpart)
        cout << character << " ";
    cout << endl;
    numpart += otherpart;

    //result part
    cout << "finally -> ";
    foreach(int character, numpart)
        cout << character << " ";
    cout << endl;
}

void secondtask()
{
    cout << "Second task" << endl;
    //input part
    int result=0;
    QVector <int> vec = {6,7,4,3,0,8,0,87,56,0,6,43,3,0,0,0,8,7};
    QVector <int> :: iterator iterbegin, iterend;
    iterbegin = vec.begin();
    iterend = vec.end();
     cout << "source -> ";
     foreach(int character, vec)
         cout << character << " ";
     cout << endl;

     //process part
    for(iterbegin; iterbegin < vec.end(); ++iterbegin )
    {
        if(*iterbegin > 0)
        break;
    }
    for(iterend; iterend < vec.begin(); ++iterend)
    {
        if(*iterend > 0)
        break;
    }
    while(iterbegin != iterend)
    {
        result += *iterbegin;
        ++iterbegin;
    }

    //result part
    cout << "finally -> " << result << endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    firsttask();
    secondtask();
    return a.exec();
}
