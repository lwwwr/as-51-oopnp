#include <iostream>
#include <math.h>
using namespace std;

double task_1_double ()
{
    double a = 1000, b = 0.0001;
    double n1,n2,n3,n_full,dn1,dn2,dn3,dn_full, res;
    
    n1 = a-b;
    n2 = pow(n1,3);
    n3 = pow(a,3);
    n_full = n2 - n3;
    
    dn1 = pow(b,3);
    dn2 = 3*a*b*b;
    dn3 = 3*a*a*b;
    dn_full = dn1 - dn2 - dn3;
    
    res = n_full / dn_full;
    
    return res;
}

float task_1_float ()
{
    float a = 1000, b = 0.0001;
    float n1,n2,n3,n_full,dn1,dn2,dn3,dn_full, res;
    
    n1 = a-b;
    n2 = pow(n1,3);
    n3 = pow(a,3);
    n_full = n2 - n3;
    
    dn1 = pow(b,3);
    dn2 = 3*a*b*b;
    dn3 = 3*a*a*b;
    dn_full = dn1 - dn2 - dn3;
    
    res = n_full / dn_full;
    
    return res;
}

void task_2 ()
{
    int m, n, res1;
    bool res2,res3;
    
    cout << "значение m -->";
    cin >> m;
    cout << "значение n -->";
    cin >> n;
    cout << endl;
    
    res1 = m+--n;
    cout << "m+--n   --> " << res1 << endl;
    
    res2 = m++<++n? true: false;
    cout << "m++<++n --> " << res2 << endl;
    
    res3 = n--<--m? true: false;
    cout << "n--<--m --> " << res3 << endl;
}

int main()
{
    cout << "Значения с переменными double типа --> " << task_1_double() << endl;
    cout << "Значения с переменными float  типа --> " << task_1_float() << endl;
    
    cout << endl << "Задание 2:" << endl;
    task_2();
    return 0;
}
