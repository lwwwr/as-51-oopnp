#include <iostream>
using namespace std;

void exchange (string &s,const int &id_1,const int &id_2)
{
    s[id_1] = s[id_1] - s[id_2];
    s[id_2] = s[id_1] + s[id_2];
    s[id_1] = -s[id_1] + s[id_2];
}

void sort (string &s, int &len)
{
    cout << ">> sorting.." << endl;
    int i = 0, j = 0, min_id=0, min=0;
    while (i < len)
    {
        min = (int)s[i];
        j = i+1;
        while (j < len)
        {
            if(isalpha(s[j]))
                if((int)s[j] <=min)
                {
                    min=(int)s[j];
                    min_id = j;
                }
            j++;
        }
        if(min < (int)s[i])
        exchange(s,min_id,i);
        i++;
    }
}

int main()
{
    std::string s;
    do
    {
        cout << ">> ";
        std::getline (std::cin,s);
        
    }while (s.length() >255);
    int len = s.length();
    
    sort(s, len);
    cout << ">> " << s << endl;
    
    return  0;
}

