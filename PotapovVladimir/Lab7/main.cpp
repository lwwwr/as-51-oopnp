#include <iostream>
#include <string>
#include "Element.h"
#include "List.h"

using namespace std;

int main() {
    list firstlist,secondlist;
    cout << "First list (Entering ends by dot)" << endl;
    cout << "> ";
    firstlist.filling();
    cout << "First list now -> ";
    firstlist.show();
    cout << endl;
    cout << "Second list (Entering ends by dot)" << endl;
    cout << "> ";
    secondlist.filling();
    cout << "Second list now -> ";
    secondlist.show();
    cout << endl;
    
    cout << "1) First list '--' -> ";
    --firstlist;
    firstlist.show();
    cout << endl;
    
    cout << "2) First list + Second list -> ";
    (firstlist+secondlist).show();
    cout << endl;
    
    if(firstlist==secondlist) {
        cout << "3) First list == second list" << endl;
    }
    else {
        cout << "3) First list != second list" << endl;
    }
    
    
    return 0;
}
