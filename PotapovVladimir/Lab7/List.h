#ifndef List_h
#define List_h
#include "Struct.h"
#include <iostream>
#include <string>
using namespace std;

class list {
private:
    element *head;
    int size;
public:
    //constructors
    list() {
        size =0;
        head = nullptr;
        //cout << "Constructed by default" << endl;
    }
    list(char newval) {
        list();
        AddToList(newval);
        //cout << "Constructed by value" << endl;
    }
    list(const list &List) {
        list();
        element *temp;
        temp = List.head;
        while (temp) {
            AddToList(temp->val);
            temp = temp->next;
        }
        //cout << "Constructed by object" << endl;
    }
    ~list() {
        while (head) {
            DelHead();
        }
        //cout << "Deconstructed" << endl;
    }
    void AddToList (char newval) {
        if(head) {
            AddTail(newval);
        }
        else {
            AddHead(newval);
        }
    }
    void AddHead(char newval) {
        element *temp;
        temp = new element(newval);
        temp->next=head;
        head = temp;
        size++;
    }
    void AddTail(char newval) {
        element *temp1,*temp2;
        temp1 = new element(newval);
        temp2 = head;
        while (temp2->next) {
            temp2 = temp2->next;
        }
        temp2->next = temp1;
        temp2->next->next=nullptr;
        size++;
    }
    void DelHead () {
        element *temp;
        temp = head;
        head=head->next;
        delete temp;
        size--;
    }
    void show () {
        element *temp;
        temp = head;
        while (temp) {
            cout << temp->val << " ";
            temp=temp->next;
        }
        cout << endl;
    }
    void filling () {
        char newval;
        while (cin >> newval) {
            if((int)newval == 46) {
                break;
            }
            AddToList(newval);
        }
    }
    //operators
    list& operator--() {
        DelHead();
        return *this;
    }
    list& operator+(const list &List) {
        list *mainlist;
        element *secondaryhead;
        mainlist=new list(*this);
        secondaryhead=List.head;
        
        while (secondaryhead) {
            mainlist->AddToList(secondaryhead->val);
            secondaryhead=secondaryhead->next;
        }
        return *mainlist;
    }
    bool operator==(const list &List) {
        bool desicion;
        element *firsthead, *secondaryhead;
        firsthead=head;
        secondaryhead=List.head;
        if(size == List.size) {
            while(firsthead && secondaryhead) {
                if(firsthead->val != secondaryhead->val) {
                    desicion=false;
                    break;
                }
                else {
                    firsthead=firsthead->next;
                    secondaryhead=secondaryhead->next;
                    if(!firsthead) {
                        desicion=true;
                    }
                }
            }
        }
        else {
            desicion = false;
        }
        return desicion;
    }
};
#endif
