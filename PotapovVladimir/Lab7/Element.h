#ifndef Element_h
#define Element_h
#include <iostream>
#include <string>
using namespace std;

struct element {
    char val;
    element *next;
    
    //constructors
    element() {
        next=nullptr;
        //cout << "Constructed by default" << endl;
    }
    element(char newval) {
        next=nullptr;
        val= newval;
        //cout << "Constructed by value" << endl;
    }
    element(element *newelement) {
        next=nullptr;
        val=newelement->val;
        //cout << "Constructed by object" << endl;
    }
};
#endif
