#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

MainWindow::MainWindow(QWidget *parent):QMainWindow(parent), pUi(new Ui::MainWindow) {
    pUi->setupUi(this);
    setFixedSize(700,400);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()), Qt::QueuedConnection);
    pTimer->setInterval(50);
    pTimer->start();
}
MainWindow::~MainWindow() {
    delete pUi;
}
void MainWindow::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    QFont font ("Courier New", 28);
    QPen textPen(Qt::black,0);
    QPen circlePen (Qt::blue,10);
    QPoint thatPlace (15,-15);
    painter.setFont(font);
    painter.setPen(textPen);
    painter.rotate(90);
    painter.drawText(thatPlace,"Lab_8_PotapovVladimir");

    painter.rotate(-90);
    painter.setPen(circlePen);
    painter.drawEllipse(xc,yc,50,50);
    painter.drawEllipse(xc-25,yc+50,100,100);
    painter.drawEllipse(xc-50,yc+150,150,150);
    xc+=5;
    yc=50 + (10*cos(xc+5));
}
