#include <QMainWindow>
#include <QPainter>
#include <QTimer>
#include <QApplication>
namespace Ui {
class MainWindow;
}

class MainWindow:public QMainWindow {
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);
private:
    int xc=75;
    int yc=0;
    Ui::MainWindow *pUi;
    QTimer *pTimer;
};
