#include <iostream>
#define MAX 255
template<typename T>
class slist {
    struct node {
        node *next;
        T     val;
    };
private:
    node *lst;
    static const int MAX_SIZE = MAX;
    int size = 0;
    void Add(const T& val){
        node *p = new node;
        p->val  = val;
        p->next = lst;
        lst = p;
    }

    void clear(void){
        node *t;
        while(lst != NULL){
            t = lst;
            lst = lst->next;
            delete t;
        }
    }
public:
    slist(void):lst(NULL){}
    ~slist(){
        this->clear();
    }

    void Input(){
        do {
            char ch;
            std::cin >> ch;
            Add(ch);
            size++;
        } while((std::cin.peek() != '\n')  && (size < MAX_SIZE));
    }

    void Print(){
        node *p = lst;
        while(p != NULL){
            std::cout<<p->val<<" ";
            p = p->next;
        }
    }

    bool empty(void) const { return (lst == NULL); }

    slist& operator + (const T& val){
        node *p = new (std::nothrow) node();
        if(p != NULL){
            p->val  = val;
            p->next = lst;
            lst = p;
        }
        return *this;
    }

    bool operator == (slist obj){
        if(size != obj.size) return false;
        while (lst != NULL && obj.lst != NULL)
            {
                if (lst->val != obj.lst->val) return false;
                lst = lst->next;
                obj.lst = obj.lst->next;
            }
        return true;
    }

    slist& operator -- (void){
        node *t;
        if(lst != NULL){
            t   = lst;
            lst = lst->next;
            delete t;
        }
        return *this;
    }

    T& operator *(void) const { return lst->val; }
    T& operator *(void) { return lst->val; }
};


