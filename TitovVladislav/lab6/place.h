#ifndef PLACE_H
#define PLACE_H
#include <iostream>
#include <cstring>

class Place {
    protected:
        std::string		Name;
        int             Area;
        int             Population;
        std::string     Head;
    public:
        Place();
        Place (std::string,std::string, int, int);
        virtual ~Place();
        virtual void Show() = 0;
        void Add();

};

Place::Place() {
    Name        = "Undefined";
    Head        = "Undefined";
    Area        = 0;
    Population	= 0;
}

Place::~Place(){};

Place::Place(std::string name, std::string head, int area, int population) {
    Name        = name;
    Area        = area;
    Population	= population;
    Head        = head;
}
struct Node {
    Node(Place*);
    Place *apPl;
    Node *apNext;
};

Node::Node(Place *el) : apNext(nullptr), apPl(el) {}

class List {
private:
    static Node *apHead;
public:
    static void AddInList(Place*);
};

void List::AddInList(Place *el) {
    Node *pNewNode = new Node(el);
    if (!apHead) apHead = pNewNode;
    else {
        pNewNode->apNext = apHead;
        apHead = pNewNode;
    }
}
Node* List::apHead = nullptr;
void Place::Add() {
    List::AddInList(this);
}

#endif // PLACE_H
