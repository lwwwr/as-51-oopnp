#include <iostream>
#include <place.h>
#include <region.h>
#include <city.h>
#include <megapolis.h>
using namespace std;

int main()
{
    Region a;
    cout << "Region: ";
    a.Add();
    a.Show();
    a.~Region();
    cout << endl;
    cout << "City: ";
    City b("Brest", "Rogachuk", 104, 343985, "28.07.1019") ;
    b.Add();
    b.Show();
    b.~City();
    cout << endl;
    cout << "Megapolis: ";
    Megapolis c;
    c.Show();
    c.~Megapolis();
    cout << endl;
    cout << "Megapolis: ";
    Megapolis Moscow("Moscow", "Sobyanin" , 2511, 12500000, "06.06.1147", 4926);
    Moscow.Add();
    Moscow.Show();
    Moscow.~Megapolis();
    return 0;

}
