#ifndef REGION_H
#define REGION_H
#include <iostream>
#include <cstring>
#include "place.h"
class Region : public Place
{
protected:
    int NumberOfCities;
    int NumberOfDisticts;
public:
    Region(std::string, std::string, int, int, int, int);
    Region();
    void Show();
};
Region::Region(std::string name, std::string head, int area, int population, int numberOfCities, int numberOfDistricts)
    : Place (name, head, area, population){
    NumberOfCities = numberOfCities;
    NumberOfDisticts = numberOfDistricts;
}
Region::Region() : Place (){
    NumberOfCities   = 0;
    NumberOfDisticts = 0;
}

    void Region::Show() {
        std::cout << "Name: "  << Place::Name
                  << " |Head: " << Place::Head
                  << " |Area:  " << Place::Area << "sq.km"
                  << " |Population " << Place::Population
                  << " |Number of Cities: " << NumberOfCities
                  << " |Number of districts " << NumberOfDisticts
                  << std::endl;
}

#endif // REGION_H
