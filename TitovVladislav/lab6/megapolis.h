#ifndef MEGAPOLIS_H
#define MEGAPOLIS_H
#include <iostream>
#include <place.h>
#include <city.h>
class Megapolis : public City
{
protected:
    double PopulationDensity;
public:
    Megapolis(std::string, std::string, int, int, std::string , double );
    Megapolis();
    ~Megapolis();
    void Show();
};

Megapolis::Megapolis(std::string name, std::string head, int area, int population, std::string foundationDay, double populationDensity)
    : City (name, head, area, population, foundationDay){
    PopulationDensity = populationDensity;
}
Megapolis::Megapolis() : City (){
    PopulationDensity = 0;
}
Megapolis::~Megapolis(){}
void Megapolis::Show() {
    std::cout << "Name: "  << Place::Name
              << " |Head: " << Place::Head
              << " |Area:  " << Place::Area << "sq.km"
              << " |Population " << Place::Population
              << " |Foundation day " << FoundationDay
              << " |Population density " << PopulationDensity << " p/sq.km"
              << std::endl;
}

#endif // MEGAPOLIS_H
