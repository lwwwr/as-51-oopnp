#ifndef CITY_H
#define CITY_H
#include <iostream>
#include <cstring>
#include "place.h"
class City : public Place
{
protected:
    std::string FoundationDay;
public:
    City(std::string, std::string, int, int, std::string);
    City();
    ~City();
    void Show();
};
City::City(std::string name, std::string head, int area, int population, std::string foundationDay)
    : Place (name, head, area, population){
    FoundationDay = foundationDay;
}
City::City() : Place (){
    FoundationDay = "Undefined";
}
City::~City(){}

void City::Show() {
    std::cout << " |Name: "  << Place::Name
              << " |Head: " << Place::Head
              << " |Area: " << Place::Area << "sq.km"
              << " |Population " << Place::Population
              << " |Foundation day " << FoundationDay
              << std::endl;
}

#endif // CITY_H
