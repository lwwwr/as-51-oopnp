#include <iostream>
#include <QVector>
#include <cmath>

void task1();
void task2();
int main()
{
    std::cout << "Task 1" << std::endl;
    task1();
    std::cout << std::endl << "Task 2" << std::endl;
    task2();
    return 0;
}

void task1(){
    QVector<int> vector1;
    QVector<int> vector2;
    int a = 0;
    int b = 0;
    int temp = 0;
    std::cout << "Enter a and b ";
    std::cin >> a >> b;

    do
    {
        std::cin >> temp;
        vector1.append(temp);
        if(abs(temp) >= a && abs(temp) <= b )
        {
            vector2.append(temp);
            std::cout << temp << ' ';
        }
    } while (std::cin.peek() != '\n');
}

void task2(){
    QVector<int> vector1;
    int temp = 0;
    int mult = 1;
    do
    {
        std::cin >> temp;
        vector1.append(temp);
    } while (std::cin.peek() != '\n');
    QVectorIterator<int> i(vector1);
    while(i.hasNext()){
        if(i.next() == 0){
            while (i.peekNext() != 0){
                mult *= i.next();
            }
            break;
        }
    }
    std::cout << mult;
}

