#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

//double qBegin = 14;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    pUi(new Ui::MainWindow)
{
    pUi->setupUi(this);
    setFixedSize(740,740);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
    pTimer->setInterval(50);
    pTimer->start();
}

void MainWindow::paintEvent(QPaintEvent *) {
    QString     string = "Lab 8 Titov Vladislav";
    QFont       font;
    font.setFamily("Algerian");
    font.setPixelSize(20);
    QPainter    text(this);

    text.setFont(font);
    text.translate(20, 10);
    for (int i = 1; i < string.size(); i+=2) {
        string.insert(i, '\n');
    }
    text.setPen(Qt::lightGray);
    text.drawText(rect(), string);
    QPainter painter(this);
    X+=4;
    double Y = 10*sin(pow(X,2)-1);

    painter.setWindow(-500, -500, 500, 500);
    painter.setPen(Qt::red);
    painter.setBrush(Qt::yellow);
    QRect circle(X-450, Y-90, 50, 50);
    QRect circle1(X-400, Y-90, 50, 50);
    QRect circle2(X-350, Y-90, 50, 50);
    painter.drawEllipse(circle1);
    painter.drawEllipse(circle);
    painter.drawEllipse(circle2);

}

MainWindow::~MainWindow()
{
    delete pUi;
}
