#include <iostream>
using namespace std;
const int a = 100;
const float b1 = 0.001;
const double b2 = 0.001;
void floatt (int, float);
void ddouble(int, double);
void task2();
int main()
{
    cout << "float:  ";
    floatt(a, b1);
    cout << "double: ";
    ddouble(a,b2);
    task2();
    return 0;
}
void floatt(int a, float b1)
{
    float f1,f2,f3,f4,f5,f6,f7,f8,fr;
    f1 = (a+b1)*(a+b1)*(a+b1)*(a+b1);
    f2 = a*a*a*a;
    f3 = 4*a*a*a*b1;
    f4 = 6*a*a*b1*b1;
    f5 = 4*a*b1*b1*b1;
    f6 = b1*b1*b1*b1;
    f7 = f1 - (f2+f3+f4);
    f8 = f5 + f6;
    fr = f7/f8;
    cout << fr << endl;
}

void ddouble (int a, double b2)
{
    float d1,d2,d3,d4,d5,d6,d7,d8,dr;
    d1 = (a+b2)*(a+b2)*(a+b2)*(a+b2);
    d2 = a*a*a*a;
    d3 = 4*a*a*a*b2;
    d4 = 6*a*a*b2*b2;
    d5 = 4*a*b2*b2*b2;
    d6 = b2*b2*b2*b2;
    d7 = d1 - (d2+d3+d4);
    d8 = d5 + d6;
    dr = d7/d8;
    cout << dr << endl;
}

void task2(){
    int m,n = 0;
cout << "Enter m and n: ";
cin >> m >> n;
cout << "n++-m " << n++-m << endl;
cout << "m-- > n " << (m-- >n) << endl;
cout << "n-- > m " << (n-- > m) << endl;
}
