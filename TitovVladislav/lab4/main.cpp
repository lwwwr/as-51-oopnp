#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <sstream>
#include <stdlib.h>

int main()
{
    char ch = '0';
    int  i = 1;
    std::string S;
    std::cout << "Enter file names: ";
    std::getline(std::cin, S);
    system("cls");
    std::istringstream iss(S);
    std::vector<std::string> names;
    std::string name;
    while (iss >> name)
    {
        names.push_back(name);
    }
    for(auto name : names)
    {
        std::cout << "File name: " << name << std::endl;
        std::cout << "Page number " << i << std::endl;
        std::cout << "The contents of the file: " << std::endl;
        i++;
        std::ifstream file(name);
        while (!file.eof())
        {
            ch = file.get();
            std::cout << ch;
        }
        file.close();
        std::cout << "\n\n" << "press Enter to continue";
        std::cin.get();
        system("cls");
    }
    return 0;
}

