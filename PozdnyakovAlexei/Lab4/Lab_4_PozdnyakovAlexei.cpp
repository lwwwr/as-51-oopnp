#include "stdafx.h"
#include <iostream>
#include <cstring>
#include <fstream>

#define cout std::cout
#define endl std::endl
#define ifstream std::ifstream
#define EXIT_SUCCESS 0


ifstream finput("input.txt");
int iterator = 1;
int counter = 0;
int line = 1;
int wordNumber = 1;
int bufferNumb = 0;
char uniqueWords[1000][50], buff[50];



static void input()
{
	if (!finput)
	{
		cout << "File does not exist!" << endl;
		exit(EXIT_SUCCESS);
	}
	finput >> uniqueWords[0];
	while (finput >> buff)
	{
		for (int j = 0; j < iterator; j++)
		{
			if (!(strcmp(buff, uniqueWords[j]))) counter = 1;
		}
		if (!counter) { strcpy_s(uniqueWords[iterator], buff); iterator++; }
		counter = 0;
	}
}

static void calculations()
{
	for (int j = 0; j < iterator; j++)
	{
		bufferNumb = 0;
		cout << wordNumber << "." << uniqueWords[j] << ":";
		finput.clear();
		finput.seekg(0);
		while (finput >> buff)
		{
			if (!(strcmp(uniqueWords[j], buff)))
			{
				if (bufferNumb != line) cout << line << " ";
				bufferNumb = line;
			}
			if (finput.get() == '\n') line++;
		}
		cout << endl;
		line = 1;
		wordNumber++;
	}
}

static void intersectionInFile()
{
	input();
	calculations();

}

int main()
{
	intersectionInFile();
	return 0;
}