#include "stdafx.h"
#include <iostream>
#include "Factory.h"
#include "InsuranceCo.h"
#include "ShipbuildingCo.h"
#include "OrganizationList.h"
#define cin std::cin
#define cout std::cout
#define endl std::endl

void solution() 
{
	cout << "Organizations:" << endl << endl;
	InsuranceCo strah;
	strah.Add();
	InsuranceCo strah2("AAA", "Insurance", 4);
	strah2.Add();
	Factory mebel;
	mebel.Add();
	Factory mebel2("OOO", "BelMebelCorparated",  400);
	mebel2.Add();
	ShipbuildingCo ship;
	ship.Add();
	ShipbuildingCo ship2("СOOO", "ShipBuild", 200,  "Ship");
	ship2.Add();
	OrganizationList::View();
}

int main() {
	solution();
	system("pause");
	return 0;
}
