#pragma once
#include <iostream>
#include <string>
#include "OrganizationList.h"
#include "InsuranceCo.h"
#define cin std::cin
#define cout std::cout
#define endl std::endl

class DepartmentOfICo : public InsuranceCo
{
protected:
	int numberOf;
public:
	DepartmentOfICo();
	DepartmentOfICo(string, string, int, int);
	DepartmentOfICo(const DepartmentOfICo &);
	~DepartmentOfICo();
	void Show();
	void Add();
	
};

DepartmentOfICo::DepartmentOfICo() { numberOf = 0; }
DepartmentOfICo::DepartmentOfICo(string typeOfOrganization2, string nameOfOrganization2, int departmNum2, int numberOf2) :
	InsuranceCo(typeOfOrganization2, nameOfOrganization2, departmNum2) {
	numberOf = numberOf2;
}
DepartmentOfICo::DepartmentOfICo(const DepartmentOfICo &rDepartmentOfICo) { }
DepartmentOfICo::~DepartmentOfICo() { }
void DepartmentOfICo::Show()
{
	cout << ">Departments:" << endl << endl << Organization::typeOfOrganization
		<< "  *  " << Organization::nameOfOrganization << "  *  " << departmNum << " *  number of:"<< this->numberOf << endl << endl;
}
void DepartmentOfICo::Add()
{
	list *pTemp = new list;
	pTemp->pData = new DepartmentOfICo(typeOfOrganization, nameOfOrganization, departmNum, numberOf);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) {
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	}
	else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}