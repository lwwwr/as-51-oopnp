#pragma once
#include <iostream>
#include <string>
#include "Organization.h"
#include "OrganizationList.h"
#define cin std::cin
#define cout std::cout
#define endl std::endl

class InsuranceCo : public Organization 
{
protected:
	int departmNum;
public:
	InsuranceCo();
	InsuranceCo (string, string, int);
	InsuranceCo(const InsuranceCo &);
   ~InsuranceCo();
	void Show();
	void Add();
};

InsuranceCo::InsuranceCo() { departmNum = 0; }
InsuranceCo::InsuranceCo( string typeOfOrganization2, string nameOfOrganization2, int departmNum2) : 
Organization (typeOfOrganization2, nameOfOrganization2) { departmNum = departmNum2; }
InsuranceCo::InsuranceCo(const InsuranceCo &rInsuranceCo) { }
InsuranceCo::~InsuranceCo() { }
void InsuranceCo::Show() 
{
	cout << ">Insurance companies:"	<< endl << endl << Organization::typeOfOrganization
	<< "  *  "<< Organization::nameOfOrganization<< "  *  department number:"<< this->departmNum<< endl << endl;
}
void InsuranceCo::Add() 
{
	list *pTemp = new list;
	pTemp->pData = new InsuranceCo(typeOfOrganization, nameOfOrganization, departmNum);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) {
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	} else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}
