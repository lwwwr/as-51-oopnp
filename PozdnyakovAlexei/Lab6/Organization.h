#pragma once
#include <iostream>
#include <string>
#define string std::string
#define cin std::cin
#define cout std::cout
#define endl std::endl

class Organization 
{
protected:
	string typeOfOrganization;
	string nameOfOrganization;
public:
	Organization();
	Organization (string, string);
	Organization(const Organization &);
	~Organization();
	virtual void Show() = 0;
	virtual void Add()	= 0;
};

Organization::Organization() { typeOfOrganization = "ConstructorTypeOfOrganization"; nameOfOrganization = "ConstructorNameOfOrganization"; }
Organization::Organization(string typeOfOrganization2, string nameOfOrganization2) { typeOfOrganization = typeOfOrganization2; nameOfOrganization = nameOfOrganization2; }
Organization::Organization(const Organization &organization) { }
Organization::~Organization() { }
