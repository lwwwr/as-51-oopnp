#pragma once
#include <iostream>
#include <string>
#include "Factory.h"
#include "OrganizationList.h"
#define cin std::cin
#define cout std::cout
#define endl std::endl


class ShipbuildingCo : public Factory 
{
protected:
	string specialization;
public:
	ShipbuildingCo();
	ShipbuildingCo (string, string, int, string);
	ShipbuildingCo(const ShipbuildingCo &);
    ~ShipbuildingCo();
	void Show();
	void Add();
};

ShipbuildingCo::ShipbuildingCo() { specialization = "ConstructorShipbuildingCo"; }
ShipbuildingCo::ShipbuildingCo(string typeOfOrganization2, string nameOfOrganization2,int amountOfWorkers2, string specialization2) :
Factory (typeOfOrganization2, nameOfOrganization2,amountOfWorkers2) { specialization = specialization2; }
ShipbuildingCo::ShipbuildingCo(const ShipbuildingCo &rShipbuildingCo) { }
ShipbuildingCo::~ShipbuildingCo() { }
void ShipbuildingCo::Show() 
{
	cout << ">Shipbuilding " << endl << endl
		<< Organization::typeOfOrganization<<"  *  "<< Organization::nameOfOrganization
		<<"  *  number of workers:" << amountOfWorkers<<"  *  "
		<<"specialization: "<< this->specialization<< endl << endl;
}
void ShipbuildingCo::Add() 
{
	list *pTemp  = new list;
	pTemp->pData = new ShipbuildingCo (typeOfOrganization, nameOfOrganization, amountOfWorkers, specialization);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) 
	{
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	} else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}
