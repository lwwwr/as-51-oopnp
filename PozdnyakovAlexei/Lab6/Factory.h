#pragma once
#include <iostream>
#include <string>
#include "Organization.h"
#include "OrganizationList.h"
#define string std::string
#define cin std::cin
#define cout std::cout
#define endl std::endl

class Factory : public Organization {
protected:
	int amountOfWorkers;
public:
	Factory();
	Factory (string, string, int);
	Factory(const Factory &);
    ~Factory();
	void Show();
	void Add();
};

Factory::Factory() { amountOfWorkers = 0; }
Factory::Factory (string typeOfOrganization2, string nameOfOrganization2, int amountOfWorkers2) : 
Organization (typeOfOrganization2, nameOfOrganization2) { amountOfWorkers = amountOfWorkers2; }
Factory::Factory(const Factory &rFactory) { }
Factory::~Factory() { }
void Factory::Show() 
{
	cout << ">This is a Factories: " << endl << endl << Organization::typeOfOrganization
		<< "  *  "<< Organization::nameOfOrganization <<"  *  number of workers:" << this->amountOfWorkers << endl << endl;
}
void Factory::Add() 
{
	list *pTemp = new list;
	pTemp->pData = new Factory(typeOfOrganization, nameOfOrganization, amountOfWorkers);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) 
	{
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	} else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}
