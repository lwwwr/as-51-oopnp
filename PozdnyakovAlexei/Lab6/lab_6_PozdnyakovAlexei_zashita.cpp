#include "stdafx.h"
#include <iostream>
#include "Factory.h"
#include "InsuranceCo.h"
#include "ShipbuildingCo.h"
#include "OrganizationList.h"
#include "DepartmentOfICo.h"
#define cin std::cin
#define cout std::cout
#define endl std::endl

void solution()
{
	cout << "Organizations:" << endl << endl;
	InsuranceCo strah;
	strah.Add();
	InsuranceCo strah2("AAA", "Insurance", 4);
	strah2.Add();
	strah2.showNumber();
	Factory mebel;
	mebel.Add();
	Factory mebel2("OOO", "BelMebelCorparated", 400);
	mebel2.Add();
	ShipbuildingCo ship;
	ship.Add();
	ShipbuildingCo ship2("СOOO", "ShipBuild", 200, "Ship");
	ship2.Add();
	DepartmentOfICo dc;
	dc.Add();
	DepartmentOfICo dc2("gdfg", "ddsgfds", 2, 3);
	dc2.Add();
	dc2.showNumber();
	OrganizationList::View();
	


}

int main() {
	solution();
	system("pause");
	return 0;
}
