#include "stdafx.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstring>
#define cin std::cin
#define cout std::cout
#define endl std::endl

typedef char* String;

class Abstract
{
public:
	virtual void input() = 0;
private:
	virtual void freeMemory(String result) = 0;
	virtual void concat(String string1, String string2) = 0;
	virtual void comparing(String string1, String string2) = 0;
};

class Inherit
{
public:
	void input()
	{
		String s1 = (String)malloc(sizeof(void*));
		String s2 = (String)malloc(sizeof(void*));
		cout << "Enter first string: ";
		cin >> s1;
		cout << "Enter second string: ";
		cin >> s2;
		comparing(s1, s2);
		concat(s1, s2);
		free(s1);
		free(s2);
	}
private:
	void comparing(String string1, String string2)
	{
		if (strcmp(string1, string2) == 0)
			cout << "Strings are equal!" << endl;
		if (strcmp(string1, string2) > 0)
			cout << string1 << " is bigger than " << string2 << endl;
		if (strcmp(string1, string2) < 0)
			cout << string1 << " is less than " << string2 << endl;
	}

	void concat(String &s1, String &s2)
	{
		String result = (String)malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
		if (result == NULL) { cout << "Memory was not allocated"; }
		strcpy(result, s1);
		strcat(result, s2);
		cout << result << endl;
		freeMemory(&result);
	}
	void freeMemory(String *result)
	{
		free(*result);
	}

};


int main()
{
	Inherit inherit;
	inherit.input();
	return 0;
}