#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

double qStart = 10;

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
pUi(new Ui::MainWindow)
{
pUi->setupUi(this);
setFixedSize(800,600);
pTimer = new QTimer();
connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
pTimer->start(150);
}

void MainWindow::paintEvent(QPaintEvent *pEvent) {

QString nameLab = "QT Graphics";
QFont font("OldEnglish", 30, QFont::Normal, false);
QPainter label(this);
label.setFont(font);
label.translate(10, 10);
for (int i = 1; i < nameLab.size(); i+=2) {
nameLab.insert(i, '\n');
}
label.drawText(rect(),Qt::AlignLeft,nameLab);

QMatrix matrix;
matrix.scale(8, 8);
QPainter painter(this);

double pX = -qStart*5;
double pY = 10*sin(pX+1);
qStart = qStart - 0.5;

painter.setWindow(-600, -600, 1200, 1200);
painter.setRenderHint(QPainter::Antialiasing, true);
painter.setMatrix(matrix);
painter.setPen(QPen(Qt::blue, 1));
painter.setBrush(Qt::green);

QRect rectangular(pX, pY, 20, 10);
rectangular.moveTo(QPoint(pX, pY));
painter.drawRect(rectangular);
painter.setBrush(Qt::white);
painter.translate(QPoint(0, -10));
QRect circle(pX, pY, 10, 10);
painter.drawEllipse(circle);
painter.setBrush(Qt::NoBrush);
painter.translate(10, 10);
QRect presentationArea(0, 0, 200, 200);
if (rectangular.bottom() > presentationArea.bottom()) {
qStart = 10;
}

}

MainWindow::~MainWindow()
{
delete pUi;
}
