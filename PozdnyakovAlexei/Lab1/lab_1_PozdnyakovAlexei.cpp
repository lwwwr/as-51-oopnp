#include "stdafx.h"
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	double a1 = 1000, b1 = 0.0001, d1;
	float a2 = 1000, b2 = 0.0001, f1;
	d1 = (pow(a1 - b1, 3) - (pow(a1, 3) - 3 * pow(a1, 2)*b1)) / (pow(b1, 3) - 3 * pow(a1, 2)*b1);
	f1 = (pow(a2 - b2, 3) - (pow(a2, 3) - 3 * pow(a2, 2)*b2)) / (pow(b2, 3) - 3 * pow(a2, 2)*b2);
	cout << d1 << endl << f1 << endl;
	int m = 4, n = 5, k1, k2, k3;
	k1 = --m - ++n;
	k2 = m * n < n++;
	k3 = n-- > m++;
	cout << k1 << " " << k2 << " " << k3 << " " << endl;
	return 0;
}