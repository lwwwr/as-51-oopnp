#pragma once
#include "Person.h"
#include "List.h"

class Teacher :public Person {
protected:
	std::string subject;
public:
	Teacher();

	Teacher(
		std::string,
		std::string,
		std::string,
		std::string
	);
	void Add();
	void Show();
	Teacher(const Teacher&);
	~Teacher();
};
Teacher::Teacher() {
	subject = "UndefinedSubject";
};

Teacher::Teacher(std::string _firstName, std::string _secondName, std::string _lastName, std::string _subject)
	:Person(_firstName, _secondName, _lastName) {
	subject = _subject;
};
void Teacher::Show() {
	std::cout << "Teacher:\n\t" << Person::secondName << "  " << Person::firstName << "  " << Person::lastName << "  |  Subject: " << subject << std::endl<<std::endl;
};
Teacher::Teacher(const Teacher& _Teacher) { };
Teacher::~Teacher() {};
void Teacher::Add() {
	list *Temp = new list;
	Temp->Data = new Teacher(
		firstName,
		secondName,
		lastName,
		subject
	);
	Temp->Next = nullptr;
	if (List::_List.GetHead() != nullptr) {
		List::_List.GetTail()->Next = Temp;
		List::_List.SetTail(Temp);

	}
	else {
		List::_List.SetHead(Temp);
		List::_List.SetTail(Temp);

	}
}