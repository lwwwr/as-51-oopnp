#include "pch.h"
#include <iostream>
#include <cmath>

using namespace std;

int main() {
	float af = 1000, bf=0.0001, fres, f1, f2, f3, f4;
	double ad = 1000, bd = 0.0001, dres, d1, d2, d3, d4;
	//для float
	f1 = pow(af + bf, 2);//(a+b)^2
	f2 = pow(af, 2);//a^2
	f3 = 2 * af*bf;//2*a*b
	f4 = pow(bf, 2);//b^2
	fres = (f1 - (f2 + f3)) / f4;//((a+b)^2-(a^2-2*a*b))/b^2
	//для double
	d1 = pow(ad + bd, 2);//(a+b)^2
	d2 = pow(ad, 2);//a^2
	d3 = 2 * ad*bd;//2*a*b
	d4 = pow(bd, 2);//b^2
	dres = (d1 - (d2 + d3)) / d4;//((a+b)^2-(a^2-2*a*b))/b^2
	cout << "*****Zadanie 1*****\n";
	cout << "Znachenie float: " << fres << endl;
	cout << "Znachenie double: " << dres << endl;
	cout << "*****Zadanie 2*****\n";
	int c,m, n;
	bool bl;
	cout << "Vvedite m:";
	cin >> m;
	cout << "Vvedite n:";
	cin >> n;
	c = m++ + n;
	cout << "m++ + n = " << c << endl;
	cout << "m= " << m << endl;
	bl = m-- > n;
	cout << "m-- > n = " <<boolalpha<< bl << endl;
	cout << "m= " << m << endl;
	return 0;
}