#include "mainwindow.h"
#include "ui_mainwindow.h"

double df=100;
double pi=2*asin(1);
double r=10;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
   ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(1000,1000);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
    pTimer->start(100);
}
void MainWindow::paintEvent(QPaintEvent *pEvent)
{

    Q_UNUSED(pEvent);
    QString     nameLab = "Графические примитивы в библиотеке QT";
    QPainter    text(this);
    text.setFont(QFont("Yu Gothic"));
    text.translate(5, 10);
    for (int i = 1; i < nameLab.size(); i+=2) {
        nameLab.insert(i, '\n');
    }
    text.drawText(rect(),Qt::AlignLeft,nameLab);

    QPainter polygon(this);

//i use function of circle and gradually increase radius
    double pointX = r * cos(df);
    double pointY = r * sin(df);
    df -= df/100;
    r+=10;
// set preferences
    polygon.setWindow(-600, -600, 1600, 1600);
    polygon.setRenderHint(QPainter::Antialiasing, true);
    polygon.setPen(QPen(Qt::black, 5));
    polygon.setBrush(Qt::red);

//Create and draw polygon
    QPolygon poly;
    poly<<QPoint(pointX,pointY);
    poly<<QPoint(pointX,pointY+240);
    poly<<QPoint(pointX+120,pointY+40);
    poly<<QPoint(pointX-80,pointY+120);
    poly<<QPoint(pointX+120,pointY+200);
    polygon.drawPolygon(poly);
}

MainWindow::~MainWindow(){
    delete ui;
}
