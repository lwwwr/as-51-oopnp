#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

char str[100], fline[100];

void readFile(char fileName[]) {
	ifstream fin(fileName);
	if (!fin) { cout << "File ne otkrit" << endl; }
	fin.getline(str, 100);
	fin.close();
}
void updateFile(char fileName[]) {
	ofstream fin(fileName);
	if (!fin) { cout << "File ne otkrit" << endl; }
	fin << fline;
	fin.close();
}
void task(char str[]) {
	int num = 0,
		fspace = 0,
		lspace = 0,
		tnum = 0,
		snum = 0;
	for (int i = 0; i < 100; i++) {
		if (isalpha(str[i]) && str[i + 1] == ' ') {
			fspace = i + 1; 
		}
		if (str[i] == ' ' && isalpha(str[i + 1])) {
			lspace = i + 1; 
		}
	}
	num = lspace - fspace; //Узнаем число пробелов
	tnum = num / 4; //Узнаем число возможных табуляция(1 таб = 4 пробела)
	snum = num - tnum * 4; //Узнаем сколько пробелов осталось
	for (int i = 0, j = 0; i < 100; i++, j++) {
		if (i < fspace) {
			fline[i] = str[j];
			continue; 
		}
		if (tnum > 0) { 
			fline[i] = '_'; 
			tnum--; j += 3; 
			continue; 
		} // Для видимости проделанной работы, вместо символов "\t", использовал "_"
		if (snum > 0) {
			fline[i] = ' ';
			snum--; 
			continue;
		}
		if (j > lspace - 1) {
			fline[i] = str[j];
		}
	}
	cout << str << endl;
	cout << fline << endl;
}
int main() {
	char fileName[25];
	cout << "Vvedite put` i imia schitivaemogo faila" << endl;
	cin.getline(fileName, 25);
	readFile(fileName);
	task(str);
	updateFile(fileName);
	return 0;
}