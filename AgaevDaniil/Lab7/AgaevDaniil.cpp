#pragma once
#include "pch.h"
#include "MyChar.h"


int main()
{
	std::cout << "Testing:\n";
	std::cout << "Testing create of obj:\n";
	MyChar firstObj;
	std::cout << "Testing copy of obj:\n";
	MyChar secondtObj(firstObj);
	std::cout << "Testing 'Input' function:\n";
	firstObj.Input();
	std::cout << "Testing operator '+ char' function:\n";
	secondtObj + 'f';
	std::cout << "Testing operator '+ obj' function:\n";
	secondtObj + firstObj;
	std::cout << "Testing 'Print' function:\n";
	firstObj.Print();
	secondtObj.Print();
	std::cout << "Testing operator '==' function:\n";
	
	if (firstObj == secondtObj) {
		std::cout << "firstObj == secondObj : true.\n";
	}
	else {
		std::cout << "firstObj == secondObj : false.\n";
	}
	std::cout << "End of testing, bye ;)\n";
	return 0;
}
