#pragma once
#include "pch.h"
#include <iostream>
#define MAX 255


class MyChar {
private:
	static const int	maxSize;
	int					currentSize;
	char				*charArr;
	MyChar&				Copy(const MyChar&);
	void				Add(char);
public:
	MyChar();
	~MyChar();
	void				Input(),
		Print();
	MyChar&				operator=(MyChar&);
	void				operator+(char),
		operator+(MyChar&);
	bool				operator==(MyChar&);
	MyChar(const MyChar& _MyChar);
};


const int MyChar::maxSize = MAX;


MyChar::MyChar() {
	std::cout << "Creating new object of MyChar!\n";
	currentSize = 0;
	charArr = new char[maxSize];
}


MyChar::~MyChar() {
	delete charArr;
}


void MyChar::Input() {
	char buffChar = NULL;
	while (currentSize < maxSize) {
		buffChar = std::cin.get();
		if ('\n' == buffChar) {
			break;
		}
		else {
			charArr[currentSize] = buffChar;
			currentSize++;
		}
	}
}


void MyChar::Print() {
	std::cout << "Print MyChar:\n";
	for (int i = 0; i < currentSize; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << '\n';
}


MyChar& MyChar::Copy(const MyChar& _MyChar) {
	charArr = new char[maxSize];
	if (_MyChar.currentSize != 0) {
		currentSize = _MyChar.currentSize;
		for (int i = 0; i < _MyChar.currentSize; i++) {
			charArr[i] = _MyChar.charArr[i];
		}
	}
	else {
		std::cout << "Nothing to copy :(\nJust create obj :)\n";
		currentSize = 0;
		charArr = new char[maxSize];
	}
	return *this;
}


void MyChar::Add(char buffChar) {
	charArr[currentSize] = buffChar;
	currentSize++;
}


MyChar::MyChar(const MyChar& _MyChar) {
	Copy(_MyChar);
}


inline MyChar& MyChar::operator=(MyChar& _MyChar) {
	return Copy(_MyChar);
}


inline void MyChar::operator+(char buffChar) {
	if (currentSize + 1 <= maxSize) {
		std::cout << "Add char '" << buffChar;
		std::cout << "' to ";
		if (currentSize == 0) {
			std::cout << "empty array.";
		} else {
			std::cout << "'";
			for (int i = 0; i < currentSize; i++) {
				std::cout << charArr[i];
			}
			std::cout << "'";
		}
		std::cout << '\n';
		Add(buffChar);
	}
	else {
		std::cout << "Total size of array bigger then max available size :( \n";
	}
}


inline void MyChar::operator+(MyChar& _MyChar) {
	int totalSize = currentSize + _MyChar.currentSize;
	if (totalSize <= maxSize) {
		std::cout << "Add elements '";
		for (int i = 0; i < _MyChar.currentSize; i++) {
			std::cout <<_MyChar.charArr[i];
			Add(_MyChar.charArr[i]);
		}
		std::cout << "' to '";
		for (int i = 0; i < currentSize-_MyChar.currentSize; i++) {
			std::cout << charArr[i];	
		}
		std::cout << "'\n";
	}
	else {
		std::cout << "Total size of two arrays bigger, then max available size :( \n";
	}
}


inline bool MyChar::operator==(MyChar& _MyChar) {
	bool exp = false;
	if (currentSize == _MyChar.currentSize) {
		for (int i = 0; i < _MyChar.currentSize; i++) {
			if (charArr[i] == _MyChar.charArr[i]) {
				exp = true;
			}
			else {
				exp = false;
			}
		}
	}
	return exp;
}