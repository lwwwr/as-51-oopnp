#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QTextStream>

QTextStream Qcin(stdin);
QTextStream Qout(stdout);
using namespace std;

void polindrom() {
	cin.get();
	QString str;
	cout << "Vvedite stroky:\n";
	str = Qcin.readLine();
	str = str.toUpper();

	int a = 0;
	for (int i = 0, j = str.size() - 1; i < j; i++, j--) {
		if (str[i] != str[j]) { cout << "Stroka ne polindrom.\n"; ++a; break; }
	}
	if(a!=1)cout<< "Stroka polindrom.\n";
}

int main() {

	int x;
	do{
		cout << "1-> Proverit` polindrom li stroka.\n";
		cout << "2-> Exit.\n";
		cin >> x;
		switch (x)
		{
			case 1: polindrom(); break;
			default: break;
		}
	}while (x != 2);
	return 0;
}
