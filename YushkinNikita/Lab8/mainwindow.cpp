#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

double qBegin = 25;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    pUi(new Ui::MainWindow)
{
    pUi->setupUi(this);
    setFixedSize(740,740);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
    pTimer->start(80);
}

void MainWindow::paintEvent(QPaintEvent *) {
    QString     labName = "Graphic primitives in the QT library";
    QFont       font("Franklin Gothic Medium", 12, QFont::Light, false);
    QPainter    label(this);
    font.setCapitalization(QFont::SmallCaps);
    font.setStyleHint(QFont:: Serif);
    label.setFont(font);
    label.translate(350, 10);
    for (int i = 1; i < labName.size(); i+=2) {
        labName.insert(i, '\n');
    }
    label.drawText(rect(),labName);
    QMatrix matrix;
    matrix.scale(6, 6);
    QPainter painter(this);
    double X = -qBegin*15 - 25 * sin(-qBegin);
    double Y = 15 - 25 * sin(-qBegin);
    qBegin = qBegin - 1;
    painter.setWindow(-500, -500, 1000, 1000);
    painter.setMatrix(matrix);
    painter.setPen(Qt::black);
    painter.setBrush(Qt::blue);
    QRect circle(X-25, Y-55+35, 25, 25);
    QRect circle_1(X, Y-30+35, 25, 25);
    QRect circle_2(X, Y-80+35, 25, 25);
    painter.drawEllipse(circle);
    painter.drawEllipse(circle_1);
    painter.drawEllipse(circle_2);
    if (-5>qBegin) {
       qBegin = 25;
    }
}

MainWindow::~MainWindow()
{
    delete pUi;
}
