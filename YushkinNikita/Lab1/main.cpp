#include <iostream>

using namespace std;


int main()
{
    float ResFloat, rf1, rf2, rf3, rf4, rf5, rf6, rf7, rf8, rf9, af=100, bf=0.001;
    double ResDouble, rd1, rd2, rd3, rd4, rd5, rd6,rd7, rd8,rd9, ad=100, bd=0.001;

    rf1=(af-bf)*(af-bf)*(af-bf)*(af-bf);
    rf2=af*af*af*af;
    rf3=4*af*af*af*bf;
    rf4=rf2-rf3;
    rf5=rf1-rf4;
    rf6=6*af*af*bf*bf;
    rf7=4*af*bf*bf*bf;
    rf8=bf*bf*bf*bf;
    rf9=rf6-rf7+rf8;
    ResFloat=rf5/rf9;

    rd1=(ad-bd)*(ad-bd)*(ad-bd)*(ad-bd);
    rd2=ad*ad*ad*ad;
    rd3=4*ad*ad*ad*bd;
    rd4=rd2-rd3;
    rd5=rd1-rd4;
    rd6=6*ad*ad*bd*bd;
    rd7=4*ad*bd*bd*bd;
    rd8=bd*bd*bd*bd;
    rd9=rd6-rd7+rd8;
    ResDouble=rd5/rd9;

    cout<<"Result float = "<<ResFloat<<endl;
    cout<<"Result double = "<<ResDouble<<endl;

    int n, m, res1, res2, res3;
    cout<<"Enter n: ";
	cin >> n;
	cout <<"Enter m: ";
	cin>>m;

	res1 = n++*m;
	res2 = n++<m;
	res3 = n-->m;

	cout <<"n++*m = "<< res1 << endl;
	cout <<"n++<m = "<< res2 << endl;
	cout <<"n-->m = "<< res3 << endl;

    return 0;
}
