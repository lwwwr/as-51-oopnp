#include <iostream>
#include <string>
#include <vector>

using namespace std;
//��������//
class Document {
protected:
	static int begin;
	static void counterBegin();
	virtual void setNameDocument() = 0;
	virtual void setNameOfService() = 0;
	virtual void setNameReceiver() = 0;
	virtual void setDateOfReceiving() = 0;
	virtual void setPayment() = 0;
	virtual void showInf() = 0;
	virtual void addInf() = 0;
	virtual void check() = 0;
};
//���������//
class Receipt : public Document {
protected:
	/*static int begin;
	static void counterBegin();*/
	vector<double> Price;
	vector<int> Year;
	vector<int> Month;
	vector<int> Day;
	vector<string> Name;
	vector<string> Surname;
	vector<string> NameDocument;
	vector<string> NameOfService;
	double price;
	int year;
	int month;
	int day;
	string name;
	string surname;
	string nameDocument;
	string nameOfService;
//private:
	void setNameDocument() override {
		NameDocument.push_back(nameDocument);
    }
	void setNameOfService() override {
		NameOfService.push_back(nameOfService);
	}
	void setNameReceiver() override {
		Name.push_back(name);
		Surname.push_back(surname);
	}
	void setDateOfReceiving() override {
		Year.push_back(year);
		Month.push_back(month);
		Day.push_back(day);
	}
	void setPayment() override {
		Price.push_back(price);
	}
	void showInf() override {
		cout << "\t\t" << NameDocument[begin] << "\n\nName: " << Name[begin] << "\nSurname: " << Surname[begin]
			<< "\n" << NameOfService[begin] << "\t" << Price[begin] << "\n\n\t\t\t" << Year[begin] << "." << Month[begin]
			<< "." << Day[begin] << endl;
	}
	void addInf() override {
		cout << "\tDocument: ";
		cin >> nameDocument;
		setNameDocument();
		cout << "Name: ";
		cin >> name;
		cout << "Surname: ";
		cin >> surname;
		setNameReceiver();
		cout << "Name of service: ";
		cin >> nameOfService;
		setNameOfService();
		cout << "\t\tYear: ";
		cin >> year;
		cout << "\t\tMonth: ";
		cin >> month;
		cout << "\t\tDay: ";
		cin >> day;
		setDateOfReceiving();
		cout << "Price: ";
		cin >> price;
		setPayment();
	}
public:
	Receipt( double price, int year, int month, int day, string name,
		string surname, string nameDocument, string nameOfService ) {
		this->price = price;
		this->year = year;
		this->month = month;
		this->day = day;
		this->name = name;
		this->surname = surname;
		this->nameDocument = nameDocument;
		this->nameOfService = nameOfService;
		setNameDocument();
		setNameOfService();
		setNameReceiver();
		setDateOfReceiving();
		setPayment();
	}
	void check() override {
		int allWork = 1;
		int addWork = 0;
		int test = 0;
		while (allWork != 0) {
			for (string n: Surname) {
				cout << "------------------------" << endl << endl;
				showInf();
				counterBegin();
				cout << "------------------------" << endl;
			}
			begin = 0;
			cout << " enter 1 to AddInf or 0 to end: ";
			cin >> addWork;
			if (addWork == 0) {
				allWork = addWork;
			}
			else {
				while (addWork != 0) {
					addInf();
					cout << "Enter 1 to continue or 0 to ShowInf: ";
					cin >> addWork;
					test++;
				}
			}
		}
	}
};
//���//
class Check final : public Receipt {
protected:
	vector<int> SerialNumber;
	int serialNumber;
	void setNameDocument() override {
		Receipt::setNameDocument();
	}
	void setNameOfService() override {
		NameOfService.push_back(nameOfService);
		SerialNumber.push_back(serialNumber);
	}
	void setNameReceiver() override {
		Receipt::setNameReceiver();
	}
	void setDateOfReceiving() override {
		Receipt::setDateOfReceiving();
	}
	void setPayment() override {
		Receipt::setPayment();
	}
	void showInf() override {
		Receipt::showInf();
		cout << "\t\tID:" << SerialNumber[begin] << endl;
	}
	void addInf() override {
		cout << "\tDocument: ";
		cin >> nameDocument;
		setNameDocument();
		cout << "Name: ";
		cin >> name;
		cout << "Surname: ";
		cin >> surname;
		setNameReceiver();
		cout << "Name of service: ";
		cin >> nameOfService;
		cout << "\t\tID:";
		cin >> serialNumber;
		setNameOfService();
		cout << "Price: ";
		cin >> price;
		setPayment();
		cout << "\t\tYear: ";
		cin >> year;
		cout << "\t\tMonth: ";
		cin >> month;
		cout << "\t\tDay: ";
		cin >> day;
		setDateOfReceiving();
	}
public:
	Check(double price, int year, int month, int day, string nameDocument,string nameOfService, string name,
		  string surname, int serialNumber) :Receipt ( price, year, month, day, nameDocument, nameOfService,
		  name, surname) {
		this->serialNumber = serialNumber;
		SerialNumber.push_back(serialNumber);
	}
	void check() override {
		int allWork = 1;
		int addWork = 0;
		int test = 0;
		while (allWork != 0) {
			for (int n:Day) {
				cout << "------------------------" << endl << endl;
				showInf();
				cout << "begin-" << begin << endl;
				counterBegin();
				cout << "------------------------" << endl;
			}
			begin = 0;
			cout << " enter 1 to AddInf or 0 to end: ";
			cin >> addWork;
			if (addWork == 0) {
				allWork = addWork;
			}
			else {
				while (addWork != 0) {
					addInf();
					cout << "Enter 1 to continue or 0 to ShowInf: ";
					cin >> addWork;
					test++;
				}
			}
		}
	}
};
//���������//
class Invoice final : public Document {
protected:
	vector<string> Product;
	vector<string> SenderName;
	vector<string> Serviece;
	vector<double> Price;
	vector<int> Year;
	vector<int> Month;
	vector<int> Day;
	string product;
	string serviece;
	string senderName;
	double price;
	int year;
	int month;
	int day;
private:
	void setNameDocument() override {
		Product.push_back(product);
	}
	void setNameOfService() override {
		Serviece.push_back(serviece);
	}
	void setNameReceiver() override {
		SenderName.push_back(senderName);
	}
	void setDateOfReceiving() override {
		Year.push_back(year);
		Month.push_back(month);
		Day.push_back(day);
	}
	void setPayment() override {
		Price.push_back(price);
	}
	void showInf() override {
		cout << "\t\t" << Product[begin] << "\n\t\t" << SenderName[begin] << "\n" << Serviece[begin] << "\t\t\t"
			<< Price[begin] << "\n\n\t\t\t\t" << Year[begin] << "." << Month[begin] << "." << Day[begin] << endl;
	}
	void addInf() override {
		cout << "\tProduct: ";
		cin >> product;
		setNameDocument();
		cout << "\tName of Sender: ";
		cin >> senderName;
		setNameReceiver();
		cout << "Serviece: ";
		cin >> serviece;
		setNameOfService();
		cout << "Price: ";
		cin >> price;
		setPayment();
		cout << "\tYear: ";
		cin >> year;
		cout << "\tMonth: ";
		cin >> month;
		cout << "\tDay: ";
		cin >> day;
		setDateOfReceiving();
	}
public:
	Invoice(string product, string serviece, string senderName, double price,
		int year, int month, int day) {
		this->product = product;
		this->serviece = serviece;
		this->senderName = senderName;
		this->price = price;
		this->year = year;
		this->month= month;
		this->day = day;
		setNameDocument();
		setNameOfService();
		setNameReceiver();
		setDateOfReceiving();
		setPayment();
	}
	void check() override {
		int allWork = 1;
		int addWork = 0;
		int test = 0;
		while (allWork != 0) {
			for (string n : Product) {
				cout << "------------------------" << endl << endl;
				showInf();
				counterBegin();
				cout << "------------------------" << endl;
			}
			begin = 0;
			cout << " enter 1 to AddInf or 0 to end: ";
			cin >> addWork;
			if (addWork == 0) {
				allWork = addWork;
			}
			else {
				while (addWork != 0) {
					addInf();
					cout << "Enter 1 to continue or 0 to ShowInf: ";
					cin >> addWork;
					test++;
				}
			}
		}
	}
};

int Document::begin = 0;
void Document::counterBegin() {
	begin++;
}

int main() {
	int work = 1;
	int choiseAddShow = 0;
	Receipt first(15.2, 2018, 10, 18, "Petya", "Petrov", "Anime Kruto", "Woda");
	Check second(13.1, 2019, 3, 4, "Petya", "Parker", "Spider-Man", "Network", 11111);
	Invoice therd("Pepsi", "Dostavka", "Company Pepsi", 1454.4, 2019, 8, 5);
	while (work != 0) {
		cout << "Enter 0 to cheak Receipt, 1 to cheak Chek and 2 to cheak Invoice: ";
		cin >> choiseAddShow;
		switch (choiseAddShow) {
		case 0: 
			first.check();
			break;
		case 1:
			second.check();
			break;
		case 2:
			therd.check();
			break;
		}
		cout << "Enter 1 to continue or 0 to end: ";
		cin >> work;
	}
	system("pause");
	return 0;
}