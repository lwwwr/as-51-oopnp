#include <iostream>
#include <Qvector>

using namespace std;

void task_1();
void task_2();

int main() {
	setlocale(LC_ALL, "rus");
	cout << "Task 1: " << endl;
	task_1();
	cout << "Task 2: " << endl;
	task_2();
	system("pause");
	return 0;
}

void task_1() {
	Qvector<int> numb = { -4,2,-10,-1,-3,4,7,-15,23,0 };
	int size = numb.size();
	int var = 0;
	Qvector<int> betaNumb(size, 1);
	auto iter = numb.begin();
	auto iter2 = betaNumb.begin();
	while (var < size) {
		if (*iter < 0 && var != size - 1) {
			*iter2 = *iter;
			++iter2;
			++iter;
		}
		else if (*iter >= 0 && var != size - 1)
			++iter;
		else if (var == size - 1 && *iter < 0) {
			*iter2 = *iter;
			++iter2;
			iter = numb.begin();
			var = 0;
			while (var < size) {
				if (*iter >= 0 && var != size - 1) {
					*iter2 = *iter;
					++iter2;
					++iter;
				}
				else if (*iter < 0 && var != size - 1) {
					++iter;
				}
				var++;
			}
		}
		else if (var == size - 1 && *iter >= 0) {
			iter = numb.begin();
			var = 0;
			while (var < size) {
				if (*iter >= 0 && var != size) {
					*iter2 = *iter;
					++iter2;
					++iter;
				}
				else if (*iter < 0 && var != size) {
					++iter;
				}
				var++;
			}
		}
		var++;
	}
	for (int i = 0; i < size; i++) {
		cout << betaNumb[i] << endl;
	}
}

void task_2() {
	Qvector<int> numb = { -4,2,-10,-1,-3,4,7,-15,23,0 };
	auto iter = numb.begin();
	int counter = 0;
	int var = 0;
	while (iter != numb.end()) {
		if (*iter > 0) {
			counter++;
		}
		else if (counter == 1) {
			var = *iter + var;
		}
		++iter;
	}
	cout << var << endl;
}