#include <iostream>
#include <list>
#include <vector>

using namespace std;

class MyClass {
protected:
	virtual void showInf() = 0;
};

class Result : public MyClass {
protected:
public:
	list<char> El = { 'a','b','c','d','e' };
	void showInf() override {
		cout << "Our list: ";
		for (char n : El) {
			cout << n << " ";
		}
		cout << endl;
	}
	char& operator [] (const int index);
	list<char>& operator +(const list<char> second) {
		auto iter2 = second.begin();
		for (char n : second) {
			El.emplace_back(*iter2);
			++iter2;
		}
		return El;
	}
	bool operator ==(const list<char> second) {
		auto iter = El.begin();
		auto iter2 = second.begin();
		for (char n : second) {
			if (*iter != *iter2) {
				return false;
			}
		}
		return true;
	}
	int begin;
};

char& Result::operator[](const int index) {
	auto iter = El.begin();
	for (int i = 0; i < index; i++) {
		iter++;
	}
	return *iter;
}


int main() {
	Result List;
	Result result;
	list<char> second = { 'v','n','p','s' };
	list<char> me = { 'a','b','c','d','e','v','n','p','s' };
	List.showInf();
	List + second;
	List.showInf();
	if (List == second) {
		cout << "oh yes!!" << endl;
	}
	else
		cout << "Oh No!!" << endl;
	if (List == me) {
		cout << "oh yes!!" << endl;
	}
	else
		cout << "Oh No!!" << endl;
	cout << List[6] << endl;
	system("pause");
	return 0;
}