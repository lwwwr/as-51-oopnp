#include <iostream>

using namespace std;

void ShowMinMax(char *word){
	int max_pos = 0,
		min_pos = 0, 
		max = 0, 
		min = 225;
		
	for (int i = 0; word[i] != '.'; i++) {
		if ((word[i - 1] == ' ' || i == 0) && word[i] != ' ') {
			int count = 0;
			for (int j = i; word[j] != ' ' && word[j] != '.'; j++)
				count++;
			if (max < count) {
				max = count;
				max_pos = i;
			}
			if (min > count) {
				min = count;
				min_pos = i;
			}
		}
	}
	cout << "минимальное: ";
	for (int i = min_pos; word[i] != ' ' && word[i] != '.'; i++)
		cout << word[i];
	cout << endl << "максимальное: ";
	for (int i = max_pos; word[i] != ' ' && word[i] != '.'; i++)
		cout << word[i];
	
}

int main() {
	setlocale (LC_ALL, "rus");
	
	char word[225];
	
	cin.getline(word, 225);
		
	ShowMinMax(word);
}
