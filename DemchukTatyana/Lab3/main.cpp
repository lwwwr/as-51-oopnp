#include <QString>
#include <iostream>
#include <QTextStream>

QTextStream Qin(stdin);

using std :: cin;
using std :: cout;
using std :: endl;


QString max_word(QStringList list){
    int max = 0;
    QString MaxWord;
    for(int i = 0; i < list.count(); i++)
        if(max <= list[i].size()){
            max = list[i].size();
            MaxWord = list[i];
        }
    return MaxWord;
}
QString min_word(QStringList list){
    int min = list[0].size();
    QString MinWord;
    for(int i = 0; i < list.count(); i++)
        if(min >= list[i].size() && list[i].size() != 0){
            min = list[i].size();
            MinWord = list[i];
        }
    return MinWord;
}

int main()
{
    QString str;

    cout << "Input string: \n";
    str = Qin.readLine();

    QStringList list = str.split(QRegExp("\\W+"));

    cout << "max word: " << max_word(list).toStdString() << endl;
    cout << "min word: " << min_word(list).toStdString() << endl;

}
