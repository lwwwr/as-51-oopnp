#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QPainter"
#include "QPen"
#include "QTimer"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void    paintEvent(QPaintEvent *pEvent);
    ~MainWindow();
private:
    Ui::MainWindow *pUi;
    QTimer *pTimer;
};

#endif // MAINWINDOW_H
