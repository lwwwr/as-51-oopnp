#pragma once
#include "Worker.h"
class Ingeneer :
	public Worker {
protected:
	int aCategory;
public:
	Ingeneer(const Ingeneer&);
	Ingeneer();
	Ingeneer(string, int, int);
	~Ingeneer();
	void Print() override;
};

Ingeneer::Ingeneer(const Ingeneer &el) : Ingeneer(el.aName, el.aWorkExperience, el.aCategory) {}
Ingeneer::Ingeneer(string name, int experience, int category) : Worker(name, experience), aCategory(category) {}
Ingeneer::Ingeneer() : Ingeneer("???", 0, 0) {}
Ingeneer::~Ingeneer() {}
void Ingeneer::Print() {
	cout << "ingeneer. " << "name: " << aName << "; work Experince: " << aWorkExperience <<
		"; category: " << aCategory << endl;
}