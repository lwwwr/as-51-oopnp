#pragma once
#include "Person.h"
class Employe :
	public Person {
protected:
	int aExperience;
public:
	Employe(const Employe&);
	Employe();
	Employe(string, int);
	~Employe();
	void Print() override;
};

Employe::Employe(const Employe &el) : Employe(el.aName, el.aExperience) {}
Employe::Employe(string name, int experience) : Person(name), aExperience(experience) {}
Employe::Employe() : Employe("???", 0) {}
Employe::~Employe() {}
void Employe::Print() {
	cout << "Employe. " << "name: " << aName  << "; expence: " << aExperience << endl;
}