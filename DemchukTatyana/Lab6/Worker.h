#pragma once
#include "Person.h"
class Worker :
	public Person {
protected:
	int aWorkExperience;
public:
	Worker(const Worker&);
	Worker();
	Worker(string, int);
	~Worker();
	void Print() override;
};

Worker::Worker(const Worker &el) : Worker(el.aName, el.aWorkExperience) {}
Worker::Worker(string name, int experience) : Person(name), aWorkExperience(experience) {}
Worker::Worker() : Worker("???", 0) {}
Worker::~Worker() {}
void Worker::Print() {
	cout << "worker. " << "name: " << aName << "; work Experince " << aWorkExperience << endl;
}
