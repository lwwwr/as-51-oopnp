#pragma once
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

class PersonList;

class Person {
protected:
	string aName;
public:
	Person();
	Person(const Person&);
	Person(string);
	~Person();
	virtual void Print() = 0;
	void Add();
};

class PersonList {
	friend Person;
	class Node {
	public:
		Node();
		Node(Person *pElem);
		Person *apPers;
		Node *apNext;
	};
	static Node *apHead, *apTail;
	static void AddInList(Person*);
public:
	static void Print();
};

PersonList::Node::Node(Person *pElem) : apNext(nullptr), apPers(pElem) {}
PersonList::Node::Node() : apNext(nullptr), apPers(nullptr) {}
PersonList::Node* PersonList::apHead = nullptr;
PersonList::Node* PersonList::apTail = nullptr;

void PersonList::AddInList(Person *pElem) {
	Node *pNewNode = new Node(pElem);
	if (!apHead) apHead = apTail = pNewNode;
	else {
		apTail->apNext = pNewNode;
		apTail = pNewNode;
	}
}
void PersonList::Print() {
	for (Node *pCur = apHead; pCur; pCur = pCur->apNext)
		pCur->apPers->Print();
}

Person::Person() : Person("???") {}
Person::Person(string name) : aName(name) {}
Person::Person(const Person &obj) : Person(obj.aName) {}
Person::~Person() {}

void Person::Add() {
	PersonList::AddInList(this);
}