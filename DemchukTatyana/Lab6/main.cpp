﻿// lab6_2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include "Person.h"
#include "Employe.h"
#include "Ingeneer.h"
#include "Worker.h"

int main() {
	Employe a("Pupcin A.S.", 4);
	Ingeneer b("Demchuck T.V.", 5, 3);
	Worker c("Golovenco H.G.", 4);
	a.Add();
	b.Add();
	c.Add();
	PersonList::Print();
}
