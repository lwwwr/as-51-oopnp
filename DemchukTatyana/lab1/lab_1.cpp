#include <iostream>

using namespace std;

int main() {
	setlocale (LC_ALL,"rus");
	
	float fa = 1000,
		  fb = 0.0001;
	float f = fa - fb,
		  f1 = f * f,
		  f2 = fa*fa,
		  f3 = 2 * fa * fb,
		  f4 = fb * fb,
		  f5 = f1 - (f2 - f3),
		  rezf;
	rezf = f5/f4;
	
	double a = 1000,
		   b = 0.0001;
	double d = a - b,
		   d1 = d * d,
		   d2 = a * a,
		   d3 = 2 * a * b,
		   d4 = b * b,
		   d5 = (d1 - (d2 - d3)),
		   rezd;
	rezd = d5/d4;
	
	cout << "� �������������� ����������� ���� double: " << rezd << endl << endl;
	cout << "� �������������� ����������� ���� float: " << rezf << endl << endl;
	
	int m, n;
	
	cout << "������� m = ";
	cin >> m;
	cout << "������� n = ";
	cin >> n;
	
	int k = ++n * ++m;
	
	cout << endl << "++n * ++m = " << k << endl << endl << "m = "<< m <<", n = " << n << endl << endl;
	cout << "m++ < n = ";
	m++ < n ? cout << "true" : cout << "false";
	cout << endl << endl << "m = " << m << ", n = " << n << endl;
	
	cout << endl << "n++ > m = ";
	n++ > m ? cout << "true" : cout << "false";
	cout << endl << endl << "m = " << m << ", n = " << n << endl;	
}
