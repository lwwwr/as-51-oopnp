﻿#include "pch.h"
#include <iostream>
#include "Array.h"

void task() {
	using std::cout;
	using std::endl;

	Array arr1, arr2;

	cout << "input arr1: ";
	arr1.input();
	cout << endl << "input arr2: ";
	arr2.input();
	cout << endl << "arr1 - 'c': ";
	(arr1 - 'c').print();
	cout << "arr1 * arr2:";
	(arr1*arr2).print();
	cout << "arr1 < arr2: " << (arr1 < arr1);
}
	

int main(){
	task();
}
