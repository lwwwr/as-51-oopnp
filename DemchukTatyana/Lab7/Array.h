#pragma once

#include <iostream>

class Array {
private:
	int aSize;
	static const int MAX_SIZE;
	char *apArr;
public:
	Array();
	Array(const char*, int);
	Array(const Array&);
	~Array();
	void input();
	void print();
	void pushBack(char);
	Array& operator - (char);
	Array& operator * (const Array&);
	bool operator < (const Array&);
};

Array::Array(const Array& object) : Array(object.apArr, object.aSize) {}

Array::Array(const char* pArr, int size) {
	if (size > MAX_SIZE) size = MAX_SIZE;
	this->aSize = size;
	for (int i = 0; i < size; i++)
		this->apArr[i] = pArr[i];
}

Array::Array() : apArr(nullptr), aSize(0) {}

void Array::input() {
	char el;
	while (std::cin >> el && aSize < MAX_SIZE) {
		if (el == '.') return;
		pushBack(el);
	}
}

void Array::print() {
	for (int i = 0; i < aSize; i++)
		std::cout << apArr[i] << " ";
	std::cout << std::endl;
}

void Array::pushBack(char el) {
	char *newArr = new char[aSize + 1];
	bool ok = true;
	for (int i = 0; i < aSize; i++) {
		newArr[i] = apArr[i];
		if (apArr[i] == el) ok = false;
	}
	if (ok)
		newArr[aSize++] = el;
	if (apArr) delete[] apArr;
	apArr = newArr;
}

Array::~Array() { delete[] apArr; }

const int Array::MAX_SIZE = 225;

Array& Array::operator - (char el) {
	Array *newArr = new Array;
	for (int i = 0; i < aSize; i++)
		if (apArr[i] != el)
			newArr->pushBack(apArr[i]);
	return *newArr;
}

bool Array::operator<(const Array& object) { return (aSize < object.aSize); }

Array& Array::operator*(const Array& object) {
	Array *newObject = new Array;
	for (int i = 0; i < aSize; i++)
		for (int j = 0; j < object.aSize; j++)
			if (apArr[i] == object.apArr[j])
				newObject->pushBack(apArr[i]);
	return *newObject;
}
