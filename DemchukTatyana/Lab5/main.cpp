#include <iostream>
#include <QVector>

QVector<int> task1(const QVector<int> &vec){

    QVector<int> rez;
    int el;

    foreach (el, vec) if(el < 0) rez += el;
    foreach (el, vec) if(el == 0) rez += el;
    foreach (el, vec) if(el > 0) rez += el;
    return rez;
}

int task2(const QVector<int> &vec) {
    int first, second, rez = 0, kol = 0;
    if (vec.isEmpty()) {
        std :: cout << ":(";
        return 0;
    }
    foreach (int elem, vec) {
        if (kol > 1) break;
        if (elem < 0) {
            if (kol == 0){
                first = elem;
                kol++;
                continue;
            }
            if (kol == 1) {
                second = elem;
                kol++;
            }
        }
    }

    for (int i = vec.indexOf(first); i <= vec.indexOf(second); i++) rez += vec[i];
    return rez;
}


int main(){
    QVector<int> V = {8,5,0,-4,-6,1,0,0,-3,-4,4};
    foreach(int el, V) std :: cout << el << " ";
    std :: cout << "\ntask 1:\n";
    std :: cout << "\n--> ";
    foreach(int el, task1(V)) std :: cout << el << " ";
    std :: cout << "\n\n task 2:\n";
    std :: cout << task2(V);
}
