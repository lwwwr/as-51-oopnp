#include <iostream>
#include <QVector>
using namespace std;
void task1();
void task2();
int main()
{
	cout << "<==Task 1==>" << endl;
	task1();
	cout << "<==Task 2==>" << endl;
	task2();
	system("pause");
	return 0;
}

void task1(){
	QVector<int> source;
	int a= 5;
	while(cin >> a)
	{
		source.append(a);
		if(cin.get()=='\n')
			break;
	}
	int t = 0;
	QVector<int> answer;
	foreach(a,source){
		if(t % 2)
			answer.append(a);
		t++;
	}
	foreach(a,source){
			if(t % 2)
				answer.append(a);
			t++;
		}
	foreach(a,answer)
		cout << a <<endl;
}
void task2(){
	QVector<int> source;
	int a= 5;
	while(cin >> a)
	{
		source.append(a);
		if(cin.get()=='\n')
			break;
	}
	int t = 0;
	int answer = 0;
	QMutableVectorIterator<int> i(source);
	 while (i.hasNext()){
		if(!t){
			if(i.peekNext() < 0)
				t = 1;
			else
				i.next();
		 }
		if(t)
			answer += abs(i.next());
	 }
	 cout << "Answer: " << answer << endl;
}
