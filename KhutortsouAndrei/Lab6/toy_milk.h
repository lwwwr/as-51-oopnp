﻿#include "good.h"
#ifndef TOY_H
#define TOY_H

class Toy : public Good
{
protected:
	string type;
public:
	Toy(string = "NO_SET", string = "NO_SET", string = "NO_SET", double = 0);
	string Show();
	void Add();
};

Toy::Toy(string _name, string _madeIn, string _type, double _price) : Good(_name,_madeIn, _price)
{
	type = _type;
	currType = eToy;
}
string Toy::Show()
{
	return name + " " + madeIn + " " + type + " " + to_string(price);
}

void Toy::Add()
{
	productList.emplace(currType, this->Show());
}
#endif
#ifndef MILK_H
#define MILK_H
class Milk : public Good
{
protected:
	string type;
public:
	Milk(string = "NO_SET", string = "NO_SET", string = "NO_SET", double = 0);
	string Show();
	void Add();
};
Milk::Milk(string _name, string _madeIn, string _type, double _price) : Good(_name,_madeIn, _price)
{
	type = _type;
	currType = eMilk;
}
string Milk::Show()
{
	return name + " " + madeIn + " " + type + " " + to_string(price);
}

void Milk::Add()
{
	productList.emplace(currType, this->Show());
}

#endif // TOY_H
