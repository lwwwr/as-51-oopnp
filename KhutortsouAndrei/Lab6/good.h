﻿#ifndef GOOD_H
#define GOOD_H
#include "product.h"

class Good : public Product
{
protected:
	double price;
public:
	Good(string = "NO_SET", string = "NO_SET", double = 0);
	virtual void Add();
	virtual string Show();
};

Good::Good(string _name, string _madeIn, double _price) : Product(_name, _madeIn)
{
	price = _price;
	currType = eGood;
}

void Good::Add()
{
	productList.emplace(currType, this->Show());
}

string Good::Show()
{
	return name + " " + madeIn + " " + to_string(price);
}
#endif // GOOD_H


