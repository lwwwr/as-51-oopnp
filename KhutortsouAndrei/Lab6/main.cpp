﻿#include <iostream>
#include "toy_milk.h"

using namespace std;

int main()
{
	Toy myToy("Gun", "USA", "Pistol", 19.55);
	myToy.Add();
	Milk mymilk("Good Milk", "Belarus", "Milk", 1.27);
	mymilk.Add();
	Good myGood("Table", "Russia", 154);
	myGood.Add();
	mymilk.ShowList();

	return 0;
}


/*
 *            product
 *               \
 *             good
 *               \
 *            -------
 *            \      \
 *           toy     milk
 *
 * */
