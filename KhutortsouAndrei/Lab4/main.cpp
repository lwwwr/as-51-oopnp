#include <iostream>
#include <fstream>
#include <istream>
using namespace std;

void str_reverse(char *str_in, char *str_out, int length);
void reverse_write1(ifstream& in, ofstream& out, int buf_size);

int main()
{
	ifstream input("1.txt");
	ofstream output("2.txt", ios::ate);

	reverse_write1(input, output, 512);

	system("pause");
	return 0;
}

void reverse_write1(ifstream& in, ofstream& out, int buf_size)
{
	char *buffer = new char[buf_size];
	char *buffer2 = new char[buf_size];
	if (in && out)
	{
		in.seekg(-buf_size - 1, ios::end);
		while (in.tellg() != 0) {
			in.get(buffer, buf_size);
			str_reverse(buffer, buffer2, buf_size);
			out << buffer2;
			if (in.tellg() <= buf_size * 2 - 2) {
				int b = (int)in.tellg() - buf_size - 2;
				in.seekg(0, ios::beg);
				in.get(buffer, b);
				str_reverse(buffer, buffer2, in.tellg());
				out << buffer2;
				break;
			}
			else
				in.seekg(-(buf_size * 2 - 2), ios::cur);
		}
	}
	else cerr << "Can't open file(s)\n";
	in.close();
	out.close();
}

void str_reverse(char *str_in, char *str_out, int length)
{
	for (int i = 0; i < length - 1; i++)
		str_out[i] = str_in[length - 2 - i];
	str_out[length - 1] = 0;
}