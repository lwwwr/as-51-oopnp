#include <iostream>
#include <math.h>
using namespace std;

int main() {
	double dA = 100, dB = 0.001;
	float fA = 100, fB = 0.001;
	float f[11]; // f[0] -- answer
	double d[11]; // d[0] -- answer

	f[1] = fA - fB;
	f[2] = pow(f[1], 4); // (a-b)^4
	f[3] = pow(fA,4); // a^4
	f[4] = 4 * pow(fA, 3) * fB; // 4 * a^3 * b
	f[5] = 6 * pow(fA,2) * pow(fB, 2); // 6 * a^2 * b^2
	f[6] = f[3] - f[4] + f[5]; // a^4 - 4a^3*b + 6a^2*b^2
	f[7] = f[2] - f[6]; // numerator
	f[8] = pow(fB, 4); // b^4
	f[9] = 4 * fA * pow(fB, 3); // 4 * a * b^3
	f[10] = f[8] - f[9]; // b^4 - 4ab^3 (denominator)
	f[0] = f[7] / f[10]; // answer

	d[1] = dA - dB;
	d[2] = pow(d[1], 4); // (a+b)^4
	d[3] = pow(dA, 4); // a^4
	d[4] = 4 * pow(dA, 3) * dB; // 4 * a^3 * b
	d[5] = 6 * pow(dA, 2) * pow(dB, 2); // 6 * a^2 * b^2
	d[6] = d[3] - d[4] + d[5]; // a^4 - 4a^3*b + 6a^2*b^2
	d[7] = d[2] - d[6]; // numerator
	d[8] = pow(dB, 4); // b^4
	d[9] = 4 * dA * pow(dB, 3); // 4 * a * b^3
	d[10] = d[8] - d[9]; // b^4 - 4ab^3 (denominator)
	d[0] = d[7] / d[10]; // answer

	cout.precision(15);
	cout << f[0] << endl << d[0] << endl;

	int n, m;
	//cin >> n >> m;
	//for example
	n = 20;
	m = 10;

	int a = n---m;
	int b = m--<n;
	int c = n++>m;
	cout << a << endl << b << endl << c << endl;
	system("pause");
	return 0;
}