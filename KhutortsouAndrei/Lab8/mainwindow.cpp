﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	{
		ui->setupUi(this);
		setFixedSize(600,600);
		pTimer = new QTimer();
		connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
		pTimer->setInterval(100);
		pTimer->start();
	}
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}
void MainWindow::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
	QFont font;
	font.setBold(true);
	font.setPixelSize(25);
	p.setFont(font);
	p.setPen( QPen( Qt::blue, 3 ) );
	p.rotate(90);
	p.drawText(QPoint(10,-20),"LAB8 QPainter");
	p.rotate(-90);

	QRect rect(0, 0, 40, 20);
	p.setPen( QPen( Qt::green, 3 ) );
	rx+=5;
	ry-=abs(rx*rx*rx/-1000);
	rect.moveTo(QPoint(rx+200, ry+200));
	p.drawEllipse(rx+200, ry+180, 40, 40);
	p.drawRect(rect);

}
