﻿#include <iostream>
#include <string.h>
#include <cctype>

using namespace std;
void toIdentifier(string input);

int main()
{
	string input = "";
	getline(cin, input);
	toIdentifier(input);
	return 0;
}

void toIdentifier(string input){
	string buf = "";
	string output = "";
	int k = 0;
	for(int i = 0; i<input.length(); i++){
		if(input[i]!=' '){
			if(!k){
				if(isalpha(input[i]) || input[i] == '_')
					buf += input[i];
				k++;
			}
			else{
				if(buf != ""){
					if(!isalpha(input[i]) && !isdigit(input[i]) && input[i]!='_')
						buf = "";
					else{
						buf += input[i];
					}
					k++;
				}
			}
		}
		else{
			output += buf + ' ';
			buf = "";
			k = 0;
		}
	}
	cout << output << endl;
}
