﻿#include <iostream>
#include <string.h>
#include <cctype>
#include <QStringList>
#include <QString>
#include <QTextStream>
QTextStream Qcout(stdout);
QTextStream Qcin(stdin);

using namespace std;
void toIdentifier(QString input);

int main()
{
	QString input = "";
	Qcin.readLineInto(&input, 900);
	toIdentifier(input);
	return 0;
}

void toIdentifier(QString input){
	string buf = "";
	QStringList a = input.split(' ');
	QString output = "";
	for(int i = 0; i<a.count(); i++){
		if(a.at(i)[0].isLetter() || a.at(i)[0] == '_')
			for(int j = 1; j< a.at(i).length(); j++)
				if(!a.at(i)[j].isDigit() && !a.at(i)[j].isLetter())
					a[i].clear();
					output += a.at(i) + ' ';
	}
	Qcout << output << endl;
}