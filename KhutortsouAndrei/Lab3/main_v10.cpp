#include <iostream>
#include <QString>
#include <QStringList>
#include <string>
#include <QTextStream>
QTextStream Qcout(stdout);

void reverseString(char *str);

using namespace std;
int main() {
    char input[256];
    cin.getline(input, 255, '\n');
	reverseString(input);
    system("pause");
    return 0;
}

void reverseString(char* str)
{
	QString line, revWord;
	line = str;
	QStringList words = line.simplified().split(" ");
	for(int i = 0; i< words.count(); i++)
	{
		for(int j = words.size()-1; j>=0; j--)
			if(words[i][j]!='.')
				revWord.append(words[i][j]);
		if(words[i].endsWith('.'))
			revWord.append('.');
		if(!QString::compare(words[i], revWord)){
		   Qcout << words[i].toLatin1()<< endl;
		revWord = "";
		}
	}
}
