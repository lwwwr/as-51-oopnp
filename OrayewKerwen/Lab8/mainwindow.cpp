﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	{
		ui->setupUi(this);
		setFixedSize(600,600);
		pTimer = new QTimer();
		connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
		pTimer->setInterval(100);
		pTimer->start();
	}
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}
void MainWindow::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
	QFont serifFont("Gothic", 20);
	p.setFont(serifFont);
	p.setPen( QPen( Qt::red, 3 ) );
	p.rotate(90);
	p.drawText(QPoint(10,-20),"LAB8 QPainter");
	p.rotate(-90);

	QRect r1(0, 0, 40, 20);
	QRect r2(0, 0, 40, 20);
	p.setPen( QPen( Qt::red, 3 ) );
	rx+=5;
	ry = rx;
	r1.moveTo(QPoint(rx, ry));
	r2.moveTo(QPoint(rx+40, ry-20));
	p.drawRect(r1);
	p.drawRect(r2);

}
