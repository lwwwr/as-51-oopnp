﻿#include <QCoreApplication>
#include <QVector>
#include <QtDebug>


int Smain(int argc, char *argv[]) {
	
		QCoreApplication a(argc, argv);
	
			//Задание 1.
		srand(time(NULL));
		// объявили 2 контейнера
		QVector <double> in, out;
		// заполнили вектор случайными числами(от 4 до 9 шт) значениями 0.0 до 9.9
		for (size_t i = 0; i < rand() % 9 + 4; i++) {
		in << ((rand() % 18 - 9) + ((rand() % 18 - 9) + 1) * 0.1)* ((rand() % 9 + 1) > 5 ? -1 : 1)  * ((rand() % 5 + 1) > 4 ? 0 : 1);
		
	}
	QVector<double>::const_iterator it;
	qDebug() << "Input :" << in;
	for (it = in.constBegin(); it != in.constEnd(); ++it) {
		if (*it >= .0) {
			out.push_front(*it);
			continue;
			
		}
		out.push_back(*it);
		
	}
	for (it = out.constBegin(); it != out.constEnd(); ++it) {
		qDebug() << *it;
		
	}
	
			//Задание 2 
		QList <double> list;
	for (size_t i = 0; i < rand() % 9 + 4; i++) {
		list << (rand() % 9 + (rand() % 9 + 1) * 0.1)* ((rand() % 9 + 1) > 5 ? -1 : 1);
		
	}
	qDebug() << "Input: " << list;
	QMutableListIterator<double>  lit(list);
	double sum;
		// удаляем с начала контейнера не положительные числа
		while (lit.hasNext()) {
		if (lit.next() <= 0) {
			lit.remove();
			continue;
			
		}
		lit.remove();
		break;
		
	}
	lit.toFront();
	++lit.next();
	while (lit.hasNext()) {
		sum += lit.next();
		
	}
	qDebug() << "Summa is " << sum;
	return a.exec();
	
}