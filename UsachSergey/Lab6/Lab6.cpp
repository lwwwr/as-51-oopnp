#include "stdafx.h"
#include<stdio.h>
#include<iostream>
using namespace std;
struct List
 {
	List *next;
	List *prev;
	List() : next(this), prev(this) {}
	List(List *nxt, List *prv) : next(nxt), prev(prv)
		 {
		nxt->prev = prv->next = this;
		}
	
		virtual ~List()
		 {
		next->prev = prev;
		prev->next = next;
		}
	};
struct Node;
class Print
 {
	public:
		Print(const char* NameDocument, const char * gost, const char * whoSigned, int number = 0)
			 {
			strcpy_s(NAMEDocument, NameDocument);
			strcpy_s(Gost, gost);
			strcpy_s(WhoSigned, whoSigned);
			Number = number;
			cout << endl << "Using List constructor" << endl;
			}
		static List list;
		static void print_all();
		virtual void Show() {};
		Print(const Print&) {}
		virtual ~Print();
		char* GetNAMEDocument()
			 {
			return NAMEDocument;
			}
		char* GetGost()
			 {
			return Gost;
			}
		char* GetWhoSigned()
			 {
			return WhoSigned;
			}
		int GetNumber()
			 {
			return Number;
			}
		protected:
			
				char NAMEDocument[250];
			char Gost[50];
			char WhoSigned[250];
			int Number;
			};
List Print::list;
struct Point : public List
 {
	Point() : List() {}
	Point(List *nxt, List *prv, Print *z_)
		 : List(nxt, prv), z(z_) {}
	Print *z;
	};

void Print::print_all()
 {
	List *direction = list.next;
	while (direction != &list)
		 {
		(static_cast<Point*>(direction))->z->print_all();
		direction = direction->next;
		}
	}

Print::~Print() {
	List *point = list.next;
	while (static_cast<Point*>(point)->z != this && point != &list)
		 point = point->next;
	if (point != &list)
		 delete point;
	

}

class
 Receipt : virtual public Print
 {
	public:
		
			Receipt(const char* NameDocument, const char * gost, const char * whoSigned, int number = 0) : Print(NameDocument, gost, whoSigned, number)
			 {
			cout << " Receipt constructor" << endl;
			}
		void Show()
			 {
			cout << endl << "Type: Receipt" << endl;
			cout << "NAMEDocument: " << NAMEDocument << endl;
			cout << "Gost: " << Gost << endl;
			cout << "WhoSigned : " << WhoSigned << endl;
			cout << "Number: " << Number << endl;
			}
		};

class Invoice : public Print
 {
	public:
		Invoice(const char* NameDocument, const char * gost, const char * whoSigned, const char * date, int quantityGoods, int number = 0) : Print(NameDocument, gost, whoSigned, number)
			 {
			strcpy_s(Date, date);
			QuantityGoods = quantityGoods;
			cout << "Invoice constructor" << endl;
			}
		char* GetDate()
			 {
			return Date;
			}
		int GetQuantityGoods()
			 {
			return QuantityGoods;
			}
		void Show()
			 {
			cout << endl << "Type: Invoice" << endl;
			cout << "NAMEDocument: " << NAMEDocument << endl;
			cout << "Gost: " << Gost << endl;
			cout << "WhoSigned : " << WhoSigned << endl;
			cout << "Quantity of goods: " << QuantityGoods << endl;
			cout << "Number: " << Number << endl;
			cout << "Date: " << Date << endl;
			}
		protected:
			int QuantityGoods;
			char Date[250];
			};

class Document : public Print
 {
	public:
		Document(const char* NameDocument, const char * gost, const char * whoSigned, int number = 0, int numberofpages = 0) : Print(NameDocument, gost, whoSigned, number)
			 {
			NP = numberofpages;
			cout << "Document constructor" << endl;
			}
		int GetNP()
			 {
			return NP;
			}
		void Show()
			 {
			cout << endl << "Type: Document" << endl;
			cout << "NAMEDocument: " << NAMEDocument << endl;
			cout << "Gost: " << Gost << endl;
			cout << "WhoSigned : " << WhoSigned << endl;
			cout << "Number: " << Number << endl;
			cout << "Number of pages: " << NP << endl;
			}
		protected:
			int NP;
			};
class Check : public Print
 {
	public:
		Check(const char* NameDocument, const char * gost, const char * whoSigned, const char * forWhom, int number = 0) : Print(NameDocument, gost, whoSigned, number)
			 {
			strcpy_s(ForWhom, forWhom);
			cout << "Check constructor" << endl;
			}
		char* GetForWhom()
			 {
			return ForWhom;
			}
		void Show()
			 {
			cout << endl << "Type: Check" << endl;
			cout << "NAMEDocument: " << NAMEDocument << endl;
			cout << "Gost: " << Gost << endl;
			cout << "WhoSigned : " << WhoSigned << endl;
			cout << "Number: " << Number << endl;
			cout << "For whom: " << ForWhom << endl;
			}
		protected:
			char ForWhom[250];
			};

int main()
 {
	Receipt *a = new Receipt("Payment for water", "P 7.0.97-2016", "Kvouthe S. E.", 76876);
	a->Show();
	Invoice *b = new Invoice("On the flow of concrete", "P 7.1.77-2016", "Rotfuss P. A.", "22.11.2018", 26, 2635);
	b->Show();
	Document *c = new Document("Contract of sale", "P 6.1.7-2018", "Sherlock H. A.", 11234, 127);
	c->Show();
	Check *d = new Check("$ 13, 000 check", "P 7.1.70-1998", "Axelrod B. Y.", "Blad V. S.", 1);
	d->Show();
	return 0;
	}