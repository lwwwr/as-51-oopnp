#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

int main() {
	double dA = 100, dB = 0.001;
	float fA = 100, fB = 0.001;
	float q[11]; // q[0] -- answer
	double w[11]; // w[0] -- answer

	q[1] = fA - fB;
	q[2] = pow(q[1], 4); // (a-b)^4
	q[3] = pow(fA, 4); // a^4
	q[4] = 4 * pow(fA, 3) * fB; // 4 * a^3 * b
	q[5] = 6 * pow(fA, 2) * pow(fB, 2); // 6 * a^2 * b^2
	q[6] = q[3] - q[4] + q[5]; // a^4 - 4a^3*b + 6a^2*b^2
	q[7] = q[2] - q[6]; // numerator
	q[8] = pow(fB, 4); // b^4
	q[9] = 4 * fA * pow(fB, 3); // 4 * a * b^3
	q[10] = q[8] - q[9]; // b^4 - 4ab^3 
	q[0] = q[7] / q[10]; // answer

	w[1] = dA - dB;
	w[2] = pow(w[1], 4); // (a+b)^4
	w[3] = pow(dA, 4); // a^4
	w[4] = 4 * pow(dA, 3) * dB; // 4 * a^3 * b
	w[5] = 6 * pow(dA, 2) * pow(dB, 2); // 6 * a^2 * b^2
	w[6] = w[3] - w[4] + w[5]; // a^4 - 4a^3*b + 6a^2*b^2
	w[7] = w[2] - w[6]; // numerator
	w[8] = pow(dB, 4); // b^4
	w[9] = 4 * dA * pow(dB, 3); // 4 * a * b^3
	w[10] = w[8] - w[9]; // b^4 - 4ab^3
	w[0] = w[7] / w[10]; // answer

	cout.precision(15);
	cout << q[0] << endl << w[0] << endl;

	int n, m;

	n = 20;
	m = 10;

	int a = n-- - m;
	int b = m--<n;
	int c = n++>m;
	cout << a << endl << b << endl << c << endl;
	system("pause");
	return 0;
}


