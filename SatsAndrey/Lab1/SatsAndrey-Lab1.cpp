#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "rus");

	cout << "������� �1" << endl;

	int const a = 100;
	double const b = 0.001;
	float f1, f2, f3, f4, f5, f6, fr;
	double d1, d2, d3, d4, d5, d6, dr;

	//������� ���������� ��� float

	cout << "\n��������� ��� ������������� float: ";
	f1 = (a + b) * (a + b) * (a + b) * (a + b);
	f2 = a * a * a * a;
	f3 = 4 * a * a * a * b;
	f4 = 6 * a * a * b * b;
	f5 = 4 * a * b * b * b;
	f6 = b * b * b * b;
	fr = (f1 - (f2 + f3 + f4)) / (f5 + f6);
	cout << fr << endl;

	//������ ��� double

	cout << "\n��������� ��� ������������� double: ";
	d1 = (a + b) * (a + b) * (a + b) * (a + b);
	d2 = a * a * a * a;
	d3 = 4 * a * a * a * b;
	d4 = 6 * a * a * b * b;
	d5 = 4 * a * b * b * b;
	d6 = b * b * b * b;
	dr = (d1 - (d2 + d3 + d4)) / (d5 + d6);
	cout << dr << endl;

	cout << "\n\n������� �2" << endl;

	cout << "\n������� ������������� ���������� \"m\": ";
	int m;
	cin >> m;
	cout << "������� ������������� ���������� \"n\": ";
	int n;
	cin >> n;

	cout << "\n��������� � ��������� � ������� ���������:" << endl;

	cout << "1) n++ - m = " << n++ - m << endl;   //������ ��������.

	if (m-- > n) {   //������ ��������.
		cout << "2) m-- > n = true!" << endl;
	}
	else {
		cout << "2) m-- > n = false!" << endl;
	}

	if (n-- > m) {   //������ ��������.
		cout << "3) n-- > m = true!\n" << endl;
	}
	else {
		cout << "3) n-- > m = false!\n" << endl;
	}

	system("pause");
	return 0;
}
