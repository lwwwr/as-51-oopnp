﻿// Lab6_8.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include "Place.h"
#include "Region.h"
#include "City.h"
#include "Megapolis.h"

int main() {
	Region a(23, 54, 356);
	City b(12, 34, 567, "gomel");
	Megapolis c(14, 36, 12000, "minsk", "President");
	a.Add();
	b.Add();
	c.Add();
	List::PrintAll();
}
