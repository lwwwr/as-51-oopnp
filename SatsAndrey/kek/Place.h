#pragma once
class Place {
protected:
	int aX, aY;
public:
	Place(const Place&);
	Place(int, int);
	Place();
	~Place();
	virtual void Print() = 0;
	void Add();
};

Place::Place(int x, int y) : aX(x), aY(y) {}
Place::Place(const Place &obj) : Place(obj.aX, obj.aY) {}
Place::Place() : Place(0, 0) {}
Place::~Place(){}

struct Node {
	Node(Place*);
	Place *apPl;
	Node *apNext;
};

Node::Node(Place *el) : apNext(nullptr), apPl(el) {}

class List {
private:
	static Node *apHead;
public:
	static void AddInList(Place*);
	static void PrintAll();
};

void List::AddInList(Place *el) {
	Node *pNewNode = new Node(el);
	if (!apHead) apHead = pNewNode;
	else {
		pNewNode->apNext = apHead;
		apHead = pNewNode;
	}
}

void List::PrintAll() {
	for (Node *pCur = apHead; pCur; pCur = pCur->apNext)
		pCur->apPl->Print();
}

Node* List::apHead = nullptr;

void Place::Add() {
	List::AddInList(this);
}
