#pragma once
#include "Region.h"
class City :
	public Region {
protected:
	string aName;
public:
	City(const City&);
	City(int, int, int, string);
	City();
	~City();
	void Print();
};

City::City(int x, int y, int squard, string name) : Region(x, y, squard), aName(name) {}
City::City(const City &obj) : City(obj.aX, obj.aY, obj.aSquard, obj.aName) {}
City::City() : City(0, 0, 0, "????") {}
City::~City() {}
void City::Print() {
	cout << "City: " << "coordinats (" << aX << ", " << aY << ")" 
		<< ", squard: " << aSquard << "; Name: " << aName <<  endl;
}

