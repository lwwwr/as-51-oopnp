#pragma once
#include "City.h"
class Metropolis :
	public City {
protected:
	string aForm;
public:
	Metropolis(const Contry&);
	Metropolis(int, int, int, string, string);
	Metropolis();
	~Metropolis();
	void Print();
};

Metropolis::Metropolis(int x, int y, int squard, string name, string form) : City(x, y, squard, name), aForm(form) {}
Metropolis::Metropolis(const Metropolis &obj) : Metropolis(obj.aX, obj.aY, obj.aSquard, obj.aName, obj.aForm) {}
Metropolis::Metropolis() : Metropolis(0, 0, 0, "????", "????") {}
Metropolis::~Metropolis() {}
void Contry::Print() {
	cout << "Metropolis: " << "coordinats (" << aX << ", " << aY << ")"
		<< ", squard: " << aSquard << "; Name: " << aName <<
		"form of government: " << aForm << endl;
}
