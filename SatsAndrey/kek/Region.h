#pragma once
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

#include "Place.h"
class Region :
	public Place {
protected:
	int aSquard;
public:
	Region(const Region&);
	Region(int, int, int);
	Region();
	~Region();
	void Print();
};

Region::Region(int x, int y, int squard) : Place(x, y), aSquard(squard) {}
Region::Region(const Region &obj) : Region(obj.aX, obj.aY, obj.aSquard) {}
Region::Region() : Region(0, 0, 0) {}
Region::~Region() {}
void Region::Print() {
	cout << "Region: " << "coordinats (" << aX << ", " << aY << ")" << ", squard: " << aSquard << endl;
}

