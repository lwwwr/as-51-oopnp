﻿// lab7_8.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include "list.h"
#include <iostream>

int main() {
	using std::cout;

	List list1, list2;
	cout << "list1: ";
	list1.input();
	cout << " -> ";
	list1.print();
	cout << "\n\nlist2: ";
	list2.input();
	cout << " -> ";
	list2.print();
	cout << "\n\nlist1 + 'g': ";
	(list1 + 'g').print();
	cout << "--list1: ";
	--list1;
	list1.print();
	cout << "list1 == list2? " << (list1 == list2);
}