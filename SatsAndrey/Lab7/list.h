#pragma once

#include <iostream>

class List
{
private:
	static int MAX_SIZE;
	int size;
	class Node {
		Node();
		Node(char);
		Node* pNext;
		char elem;
		friend List;
	};
	Node *pHead;
	void pushBeg(char);
	void pushEnd(char);
public:
	List& operator + (char);
	void operator -- ();
	bool operator == (const List&);
	void input();
	void print();
	List();
	List(const List&);
	~List();
};

void List::input() {
	char ch;
	while (size < MAX_SIZE) {
		std::cin >> ch;
		if (ch == '.') break;
		this->pushBeg(ch);
	}
}

void List::pushBeg(char elem) {
	Node *pNode = new Node(elem);
	if (!pHead) pHead = pNode;
	else {
		pNode->pNext = pHead;
		pHead = pNode;
	}
	size++;
}

List& List::operator+(char elem) {
	List *pList = new List(*this);
	pList->pushBeg(elem);
	return *pList;
}

void List::print() {
	for (Node *pCur = pHead; pCur; pCur = pCur->pNext)
		std::cout << pCur->elem << " ";
	std::cout << std::endl;
}


void List::operator--() {
	Node *pNode = pHead;
	pHead = pHead->pNext;
	size--;
	delete pNode;
}

bool List::operator==(const List& obj) {
	if (size != obj.size) return false;
	bool oke = false;
	for (Node *pCur1 = pHead; pCur1; pCur1 = pCur1->pNext) {
		for (Node *pCur2 = obj.pHead; pCur2; pCur2 = pCur2->pNext) {
			if (pCur1->elem == pCur2->elem) {
				oke = true;
				break;
			}
		}
		if (!oke) return false;
		else oke = false;
	}
	return true;
}

List::List(const List& obj) {
	for (Node *pCur = obj.pHead; pCur; pCur = pCur->pNext)
		pushEnd(pCur->elem);
}

List::List() : pHead(nullptr), size(0) {}

int List::MAX_SIZE = 225;

List::Node::Node() : pNext(nullptr) {}

List::Node::Node(char el) : pNext(nullptr), elem(el) {}

List::~List() {
	while (pHead){
		Node *pNode = pHead->pNext;
		delete pHead;
		pHead = pNode;
		size--;
	}		
}

void List::pushEnd(char elem) {
	Node *pNode = new Node(elem);
	if (!pHead) pHead = pNode;
	else {
		Node *pCur = pHead;
		for (; pCur->pNext; pCur = pCur->pNext);
		pCur->pNext = pNode;
	}
}