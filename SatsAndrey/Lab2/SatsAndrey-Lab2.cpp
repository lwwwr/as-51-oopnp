#include <iostream>
#include <cstring>

using namespace std;

const int MAX = 255;

void sort(char *num, int size) {
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			if (num[j] < num[i]) {
				int temp = num[i];
				num[i] = num[j];
				num[j] = temp;
			}
}

int main() {


	char str[MAX];

	cin.getline(str, MAX);

	int size = 0;// ������� ���������� ����

	for (int i = 0; str[i] != '\0'; i++) {
		if (str[i] >= 48 && str[i] <= 57) {//��� ������� � ������� ����, �.�. ������� ������� ����������� ���� ��� (0-9) -> (48-57)
			size++;
		}
	}
	char *numbers = new char[size];// �������� ������, ��� �������

	int k = 0;
	for (int i = 0; str[i] != '\0'; i++) {//���������� ��� ���� �����
		if (str[i] >= 48 && str[i] <= 57) {
			numbers[k] = str[i];
			k++;
		}
	}

	sort(numbers, size);//� ���� ������� �� ���������

	k = 0;

	for (int i = 0; str[i] != '\0'; i++) {//� ��� ��������� ���������������
		if (str[i] >= 48 && str[i] <= 57) {
			str[i] = numbers[k];
			k++;
		}
	}

	cout << str << endl;

	delete[] numbers;

	system("pause");
	return 0;
}