﻿#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <vector>


int main()
{
	char ch = '0';
	int  i = 1;

	std::string STR;
	std::cout << "Enter file names separated by a space: ";
	std::getline(std::cin, STR);
	system("cls");

	std::istringstream iss(STR);
	std::vector<std::string> names;
	std::string name;


	while (iss >> name)
	{
		names.push_back(name);
	}


	for (auto name : names)
	{
		std::cout << "File name is '" << name <<"\'"<< std::endl;
		std::cout << "Page number: " << i << std::endl;
		std::cout << "The contents of the file: " << std::endl;
		i++;
		std::ifstream file(name);


		while (!file.eof())
		{
			ch = file.get();
			std::cout << ch;
		}


		file.close();
		std::cout << "\n\npress Enter to continue";
		std::cin.get();
		system("cls");
	}
	return 0;
}