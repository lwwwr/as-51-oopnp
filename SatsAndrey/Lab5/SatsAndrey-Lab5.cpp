#include <iostream>
#include <QVector>
#include "math.h"//for abs()

void task1(){
    std::cout << "task 1\n\n";
    QVector <int> v = {1,2,6,1,4,2,8,9};
    QVector <int> rez;
    int a = 1;
    int b = 4;

    std::cout << "[a;b]   a = " << a << "  b = " << b << "\n";
    foreach(int elem, v)
        if(abs(elem) >= a && abs(elem) <= b)
            rez += elem;
    foreach(int elem, v) std::cout << elem << " ";
    std::cout << "\n--->";
    foreach(int elem, rez) std::cout << elem << " ";
}

void task2(){
    std::cout << "\n\ntask 2\n\n";
    QVector <int> v = {1,2,0,6,1,4,2,0,8,9};
    QVectorIterator <int> i(v);
    bool find = false;
    int rezult = 0;

    while(i.hasNext() && !find)
        if(i.next() == 0){
            while(i.next() != 0)
                rezult += i.peekPrevious();
            find = true;
        }
    foreach(int elem, v) std::cout << elem << " ";
    std::cout << "\nanswer: " << rezult;
}

int main() {
    task1();
    task2();
}
