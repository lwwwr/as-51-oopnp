#include <QMainWindow>
#include "QPainter"
#include "QPen"
#include "QTimer"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *pParent = nullptr);
    ~MainWindow();
    void    paintEvent(QPaintEvent *);

private:
    Ui::MainWindow  *Ui;
    QTimer          *pTimer;
    int X =-60;
};
