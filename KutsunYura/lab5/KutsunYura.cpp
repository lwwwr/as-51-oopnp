#include <iostream>
#include <math.h>
#include <QVector>

void task1();
void task2();

int main()
{
    task1();
    task2();
    return 0;
}

void task1()
{
    using namespace std;
    int buff=0;
    QVector<int> newVector, originalVector;
    while(cin>>buff){
        originalVector.append(buff);
        if(cin.get()=='\n'){
            break;
        }
    }
    int i=0,j=0;
    foreach(i,originalVector){
        if(1>=abs(i)){
           newVector<<i;
        }
    }
    cout<<"task 1: ";
    foreach(j,newVector){
        cout<<j<<" ";
    }
    cout<<endl;
}

void task2()
{
    using namespace std;
    int buff=0;
    QList<int> sequence;
    while(cin>>buff){
        sequence.append(buff);
        if(cin.get()=='\n'){
            break;
        }
    }
    QListIterator<int> i(sequence);
    int summ1=0,summ2=0;
    i.toFront();
    while(0<=i.peekNext()){
        i.next();
    }
    while(i.hasNext()){
        if(i.peekNext()>=0){
            summ1+=i.next();
        }
        else{
           summ2=summ1;
           i.next();
        }
    }
    cout<<"task 2: "<<summ2;
}
