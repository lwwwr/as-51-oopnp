#include <iostream>
#include <Qstring>
#include <QStringList>
#include <QTextStream>
QTextStream Qcin(stdin);
QTextStream Qcout(stdout);

using namespace std;

void searchFirstWord()
{
    int count=0;
    QString str, firstWord;
    cout<<"Enter the string:"<<endl;
    str = Qcin.readLine();
    QStringList words = str.split(" ");
    firstWord=words[0];
    for(int i=1;i<words.count();i++)
        {
        if(firstWord==words[i]) count++;
        }
    cout<<"The number of words which identical the first word:"<<count;
}

int main()
{
    searchFirstWord();
    return 0;
}
