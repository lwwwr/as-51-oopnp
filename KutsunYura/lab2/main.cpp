#include <iostream>
#include <cstring>

using namespace std;

void searchFirstWord()
  {
    cout<<"Enter the string:"<<endl;
    string str,firstWord;
    bool first=1;
    int count=0;
    while(cin>>str)
      {
        if(first) {firstWord=str; first=0;}
        if(firstWord==str) count++;
        if(cin.get()=='\n') break;
      }
    cout<<"The number of words which identical the first word:"<<count-1;
  }

int main()
{
  searchFirstWord();
  return 0;
}
