#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

double qBegin = 14;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    pUi(new Ui::MainWindow)
{
    pUi->setupUi(this);
    setFixedSize(740,740);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
    pTimer->start(80);
}

void MainWindow::paintEvent(QPaintEvent *) {
    QString     labName = "Graphic primitives in the QT library";
    QFont       font("Courier", 12, QFont::Light, false);
    QPainter    label(this);
    font.setCapitalization(QFont::SmallCaps);
    font.setStyleHint(QFont:: Serif);
    label.setFont(font);
    label.translate(350, 10);
    for (int i = 1; i < labName.size(); i+=2) {
        labName.insert(i, '\n');
    }
    label.drawText(rect(),labName);
    QMatrix matrix;
    matrix.scale(6, 6);
    QPainter painter(this);
    double X = -qBegin*7;
    double Y = 22*sin(X+1);
    qBegin = qBegin - 1;
    painter.setWindow(-500, -500, 1000, 1000);
    painter.setMatrix(matrix);
    painter.setPen(Qt::green);
    painter.setBrush(Qt::black);
    QRect rectangle(X, Y, 30, 60);
    QRect circle(X, Y-30, 30, 30);
    painter.drawRect(rectangle);
    painter.drawEllipse(circle);
    if (-11>qBegin) {
       qBegin = 14;
    }
}

MainWindow::~MainWindow()
{
    delete pUi;
}
