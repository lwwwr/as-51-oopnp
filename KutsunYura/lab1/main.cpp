#include <iostream>
#include <math.h>

using namespace std;

void task1_1(double a,double b)
{
double f1,f2,f3,f4,f5;
f1=pow((a+b),2);
f2=pow(a,2);
f3=2*a*b;
f4=pow(b,2);
f5=(f1-(f2+f3))/f4;
cout<<"double:"<<f5<<endl;

}

void task1_2(float a,float b)
{
float f1,f2,f3,f4,f5;
f1=pow((a+b),2);
f2=pow(a,2);
f3=2*a*b;
f4=pow(b,2);
f5=(f1-(f2+f3))/f4;
cout<<"float:"<<f5<<endl;
}

void task2(int m,int n)
{
int f1,f2,f3;
f1=--m-++n;
cout<<"--m-++n="<<f1<<endl;
f2=m*n<n++;
cout<<"m*n<n++:"<<f2<<endl;
f3=n-->m++;
cout<<"n-->m++:"<<f3;
}

int main() {
double a1=1000,b1=0.0001;
float a2=1000,b2=0.0001;
int m=5,n=4;
task1_1(a1,b1);
task1_2 (a2,b2);
task2(m,n);
return 0;
}
