#ifndef Class_H
#define Class_H
#include <set>
#include <iostream>
#include <stdarg.h>
using namespace std;

class Class
{
private:
	set<char> Set;
public:
	Class();
	Class(Class &obj);
	~Class();
	void Input();
	void Print();
	void operator + (Class &obj);
	bool operator <= (Class &obj);
	void operator () (int n, ...);
};

Class::Class()
{

}

Class::Class(Class &obj)
{
	Set = obj.Set;
}

Class::~Class()
{
	Set.clear();
}

void Class::Input()
{
	cout << "Enter new element: ";
	char a = ' ';
	cin >> a;
	Set.insert(a);
}

void Class::Print()
{
	for(auto i : Set){
		cout << i << " ";
	}
	cout << endl;
}

void Class::operator +(Class &obj)
{
	for(auto i : obj.Set){
		Set.insert(i);
	}
}

bool Class::operator <=(Class &obj)
{
	if(Set <= obj.Set){
		return true;
	}else{
	 	 return false;
	 }
}

void Class::operator () (int n, ...)
{
  va_list vl;
  va_start(vl, n);
  for(int i = 0; i<n; i++){
	  Set.insert((char)va_arg(vl, int));
	}
  va_end(vl);
}


#endif // Class_H
