﻿#include <iostream>
#include "Class.h"

void Test();

int main(){
	Test();
	return 0;
}

void Test(){
	using namespace std;
	Class first;
	Class second;
	second(1,'a');
	first.Input();
	cout << "'()' test.\n";
	try{
		cout << "Second: ";
		second.Print();
		cout << "Using '()' operator''.\n";
		cout<<"(second(3,'b','c','d')).\n";
		second(3,'b','c','d');
		cout << "Second: ";
		second.Print();
	}catch(...){
		cout << "'()' error occure.\n";
	}
	cout << "Operator '()' is working.\n---------------------------\n";
	cout << "'+' test.\n";
	try{
		second(3,'a','b','c');
		cout << "First: ";
		first.Print();
		cout << "Second: ";
		second.Print();
		cout << "Using '+' operator.\n";
		cout<<"(first + second).\n";
		first + second;
		cout << "First: ";
		first.Print();
	}catch(...){
		cout << "'+' error occure.\n";
	}
	cout << "Operator '+' is working.\n---------------------------\n";
	cout << "'<=' test.\n";
	try{
		cout << "First: ";
		first.Print();
		cout << "Second: ";
		second.Print();
		cout << "'First' <= 'Second'?\n";
		if(first<=second)
			cout << "Yep.\n";
		else
			cout << "No.\n";
	}catch(...){
		cout << "'<=' error occure.\n";
	}
	cout << "Operator '<=' is working.\n---------------------------";
}
