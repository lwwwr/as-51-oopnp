#include <iostream>
#include "Factory.h"
#include "InsuranceCo.h"
#include "ShipbuildingCo.h"
#include "OrganizationList.h"

void Task();

int main() {
	Task();
	return 0;
}

void Task() {
	std::cout << "Organizations:" << std::endl << std::endl;
	InsuranceCo strah;
	strah.Add();
	InsuranceCo strah2("OAO", "gosStrah", 3);
	strah2.Add();
	Factory mebel;
	mebel.Add();
	Factory mebel2("OAO", "BelMebel",  100);
	mebel2.Add();
	ShipbuildingCo ship;
	ship.Add();
	ShipbuildingCo ship2("OAO", "SudStroi", 100,  "Ship");
	ship2.Add();
	OrganizationList::View();
}
