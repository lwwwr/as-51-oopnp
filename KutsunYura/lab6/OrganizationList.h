﻿#pragma once
#include <iostream>

struct list {
	Organization	 *pData;
	list	         *pNext;
};

class OrganizationList {
	private:
		list   *apHead;
		list   *apTail;
	public:
		static list            *apFirst;
		static OrganizationList gsList;

		OrganizationList();
		OrganizationList(const OrganizationList &);
	    ~OrganizationList();
		list	       *GetHead() const;
		void		    	SetHead(list *);
		list	       *GetTail() const;
		void		    	SetTail(list *);
		static void		View();
};

OrganizationList  OrganizationList::gsList	= { };
list		*OrganizationList::apFirst	= 0;
OrganizationList::OrganizationList() {
	apHead = NULL;
	apTail = NULL;
}
OrganizationList::OrganizationList(const OrganizationList &Organization) { }
OrganizationList::~OrganizationList() {
	while (apHead != NULL) {
		list *pTemp = apHead->pNext;
		delete apHead;
		apHead = pTemp;
	}
}
list   *OrganizationList::GetHead()	const		{ return apHead; }
void	OrganizationList::SetHead(list* pFirst)	{ this->apHead = pFirst; }
list   *OrganizationList::GetTail()	const		{ return apTail; }
void	OrganizationList::SetTail(list* pFirst)	{ this->apTail = pFirst; }
void	OrganizationList::View() {
	if (NULL == OrganizationList::apFirst) {
		std::cout << "No items in list!" << std::endl;
	}
	else {
		list *pTemp = new list;
		pTemp = OrganizationList::apFirst;
		while (pTemp != NULL) {
			pTemp->pData->Show();
			pTemp = pTemp->pNext;
		}
    }
}
