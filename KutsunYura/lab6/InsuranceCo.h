#pragma once
#include <iostream>
#include <string>
#include "Organization.h"
#include "OrganizationList.h"

class InsuranceCo : public Organization {
	protected:
		int aDepartmNum;
	public:
		InsuranceCo();
		InsuranceCo (
			std::string,
			std::string,
			int
		);
		InsuranceCo(const InsuranceCo &);
	   ~InsuranceCo();
		void Show();
		void Add();
};
InsuranceCo::InsuranceCo() {
	aDepartmNum = 0;
}
InsuranceCo::InsuranceCo(
		std::string		typeOfOrganization,
		std::string		nameOfOrganization,
		int             departmNum
) : Organization (
		typeOfOrganization,
		nameOfOrganization
) {
	aDepartmNum = departmNum;
}
InsuranceCo::InsuranceCo(const InsuranceCo &rInsuranceCo) { }
InsuranceCo::~InsuranceCo() { }
void InsuranceCo::Show() {
	std::cout << " >Insurance companies:"	<< std::endl<<std::endl<<" "<< Organization::aTypeOfOrganization
	<< "  *  "<< Organization::aNameOfOrganization<< "  *  department number:"<< this->aDepartmNum<< std::endl<<std::endl;
}
void InsuranceCo::Add() {
	list *pTemp = new list;
	pTemp->pData = new InsuranceCo(
		aTypeOfOrganization,
		aNameOfOrganization,
		aDepartmNum
	);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) {
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	} else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}
