#pragma once
#include <iostream>
#include <string>
#include "Factory.h"
#include "OrganizationList.h"

class ShipbuildingCo : public Factory {
	protected:
		std::string aSpecialization;
	public:
		ShipbuildingCo();
		ShipbuildingCo (
			std::string,
			std::string,
			int,
			std::string
		);
		ShipbuildingCo(const ShipbuildingCo &);
       ~ShipbuildingCo();
		void Show();
		void Add();
};
ShipbuildingCo::ShipbuildingCo() { aSpecialization = "ConstructorShipbuildingCo"; }
ShipbuildingCo::ShipbuildingCo(
		std::string		typeOfOrganization,
		std::string		nameOfOrganization,
		int				AmountOfWorkers,
		std::string		specialization
) : Factory (
		typeOfOrganization,
		nameOfOrganization,
		AmountOfWorkers
) {
	aSpecialization = specialization;
}
ShipbuildingCo::ShipbuildingCo(const ShipbuildingCo &rShipbuildingCo) { }
ShipbuildingCo::~ShipbuildingCo() { }
void ShipbuildingCo::Show() {
	std::cout << " >Shipbuilding: "		<< std::endl<<std::endl<<" "
		<< Organization::aTypeOfOrganization<<"  *  "<< Organization::aNameOfOrganization
		<<"  *  number of workers:"<< aAmountOfWorkers<<"  *  "
		<<"specialization: "<< this->aSpecialization<< std::endl<<std::endl;
}
void ShipbuildingCo::Add() {
	list *pTemp = new list;
	pTemp->pData = new ShipbuildingCo(
		aTypeOfOrganization,
		aNameOfOrganization,
		aAmountOfWorkers,
		aSpecialization
	);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) {
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	}
	else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}
