#pragma once
#include <iostream>
#include <string>

class Organization {
	protected:
		std::string		aTypeOfOrganization;
		std::string		aNameOfOrganization;
	public:
		Organization();
		Organization (
			std::string,
			std::string
		);
		Organization(const Organization &);
	  ~Organization();
		virtual void Show() = 0;
		virtual void Add()	= 0;
};
Organization::Organization() {
  aTypeOfOrganization		= "ConstructorTypeOfOrganization";
	aNameOfOrganization		= "ConstructorNameOfOrganization";
}
Organization::Organization(std::string typeOfOrganization, std::string nameOfOrganization) {
	aTypeOfOrganization		= typeOfOrganization;
	aNameOfOrganization		= nameOfOrganization;
}
Organization::Organization(const Organization &organization) { }
Organization::~Organization() { }
