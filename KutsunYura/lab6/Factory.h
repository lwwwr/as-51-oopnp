#pragma once
#include <iostream>
#include <string>
#include "Organization.h"
#include "OrganizationList.h"

class Factory : public Organization {
	protected:
		int aAmountOfWorkers;
	public:
		Factory();
		Factory (
			std::string,
			std::string,
			int
		);
		Factory(const Factory &);
	   ~Factory();
		void Show();
		void Add();
};

Factory::Factory() { aAmountOfWorkers = 0; }
Factory::Factory (
		std::string		typeOfOrganization,
		std::string		nameOfOrganization,
		int       		AmountOfWorkers
) : Organization (
		typeOfOrganization,
		nameOfOrganization
) {
	aAmountOfWorkers = AmountOfWorkers;
}
Factory::Factory(const Factory &rFactory) { }
Factory::~Factory() { }
void Factory::Show() {
	std::cout << " >Factories: "	<< std::endl<<std::endl<<" "<< Organization::aTypeOfOrganization
		<< "  *  "<< Organization::aNameOfOrganization<<"  *  number of workers:"<< this->aAmountOfWorkers<< std::endl<<std::endl;
}
void Factory::Add() {
	list *pTemp = new list;
	pTemp->pData = new Factory(
		aTypeOfOrganization,
		aNameOfOrganization,
		aAmountOfWorkers
	);
	pTemp->pNext = NULL;
	if (OrganizationList::gsList.GetHead() != NULL) {
		OrganizationList::gsList.GetTail()->pNext = pTemp;
		OrganizationList::gsList.SetTail(pTemp);
	}
	else {
		OrganizationList::apFirst = pTemp;
		OrganizationList::gsList.SetHead(pTemp);
		OrganizationList::gsList.SetTail(pTemp);
	}
}
