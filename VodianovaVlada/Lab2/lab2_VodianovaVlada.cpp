#include <iostream>
#include <string>

using namespace std;

void check(string str) {
	string::size_type found = str.find_first_of(",.?!:;");
	while (found != string::npos)
	{
		str[found] = ' ';
		found = str.find_first_of(",.?!:;", found + 1);
	}
}

int max(string str, int r) {
	int word = 0;
	int maxlen = 0;
	int index = 0;
	int pos = 0;
	
	for (int i = 0; i < r; i++) {
		if (str[i] == ' ' || str[i] == '.') {
			if (word > maxlen) {
				maxlen = word;
				index = i;
				pos = index - maxlen;
			}
			
			word = 0;
		}
		else word++;
		}

	cout << str.substr(pos, index-pos);
	return maxlen;
}

int min (string str, int r) {
	int word = 0;
	int minlen = r;
	int index = 0;
	int pos = 0;

	for (int i = 0; i < r; i++) {
		if (str[i] == ' ' || str[i] == '.') {
			if (word < minlen) {
				minlen = word;
				index = i;
				pos = index - minlen;
			}
			word = 0;
		}
		else word++;
	}

	cout << str.substr(pos, index-pos);
	return minlen;
}

int main() {
	string str;
	getline (cin, str);
	check (str);
	
	int r = str.length();
	cout << "The longest word: ";
	max(str, r);
	cout << endl << "The shortest word: ";
	min(str, r);
	
	cin.get();
	cin.get();
	return 0;
}