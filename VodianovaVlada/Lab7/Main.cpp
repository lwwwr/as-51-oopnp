#include <iostream>
#include "CharArray.h"

int main() {

	setlocale(LC_ALL, "Russian");

	char str[] = { 's', 't', 'r', 'i', 'n', 'g' };

	CharArray firstObj; //конструктор без параметров
	CharArray secondObj(str, 6); //конструктор с параметрами
	CharArray thirdObj(secondObj); //конструктор копирования

	cin >> firstObj;
	cout << firstObj << endl;

	thirdObj = secondObj - 'r';
	cout << secondObj << " -> " << thirdObj << endl;

	if (secondObj < thirdObj) cout << "true" << endl;
	else cout << "false" << endl;

	CharArray fourthObj;

	fourthObj = secondObj * firstObj;
	cout << fourthObj << endl;

	cin.get();
	cin.get();
	return 0;
}
