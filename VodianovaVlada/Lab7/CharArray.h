#include <iostream>

using namespace std;

class CharArray{
public:
	int length;
	int count;
	char *data;
	CharArray(int size = 0) {
		if (size <= 0) {
			length = 0;
			count = 32;
			data = NULL;
		}

		else {
			count = 32;
			while (size > count) {
				count *= 2;
			}

			length = size;
			data = new char[count];
		}
	}

	CharArray(char A[], int size) {
		length = size;
		count = 32;
		while (size > count) {
			count *= 2;
		}

		data = new char[count];

		for (int i = 0; i < length; i++) {
			data[i] = A[i];
		}
	}

	CharArray(const CharArray &obj) {
		if (obj.length > 0) {
			count = 32;
			length = obj.length;
			data = new char[length];
			for (int i = 0; i < length; i++) {
				data[i] = obj.data[i];
			}
		}

		else {
			length = 0;
			count = 32;
			data = NULL;
		}
	}

	~CharArray() {
		cout << "Object destroyed\n";
	}

	friend ostream &operator <<(ostream &of, CharArray &obj) {
		if (obj.data != NULL) {
			for (int i = 0; i < obj.length; i++) {
				of << obj.data[i] << " ";
			}
		}

		else {
			of << "It's empty" << endl;
		}
		return of;
	}

	friend istream &operator >> (istream &in, CharArray &obj) {
		int tmp;
		cout << "Enter size: " << endl;
		cin >> tmp;
		cout << "Enter the set: " << endl;
		while (tmp > obj.count) {
			obj.count *= 2;
		}

		if (tmp > 0) {
			delete[]obj.data;
			obj.length = tmp;
			obj.data = new char[obj.count];
			for (int i = 0; i < obj.length; i++) {
				in >> obj.data[i];
			}
		}

		else {
			cout << "error!" << endl;
		}
		return in;
	}

	CharArray &operator -(char symbol) {
		CharArray *result;
		result = new CharArray(length - 1);
		int index = 0;
		for (int i = 0; i < length; i++) {
			if (data[i] != symbol) {
				result->data[index] = data[i];
				index++;
			}
		}
		return *result;
	}

	CharArray &operator *(CharArray &obj) {
		int sizeOfResultObj = 0;
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < obj.length; j++) {
				if (data[i] == obj.data[j]) sizeOfResultObj++;
			}
		}

		CharArray *result;
		result = new CharArray(sizeOfResultObj);
		int index = 0;
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < obj.length; j++) {
				if (data[i] == obj.data[j]) {
					result->data[index] = data[i];
					index++;
				}
			}
		}
		return *result;
	}

	bool operator <(CharArray &obj) {
		if (length < obj.length) {
			return true;
		}

		else if (length > obj.length) {
			return false;
		}

		else {
			for (int i = 0; i < length; i++) {
				if (data[i] < obj.data[i]) {
					return true;
				}

				else if (data[i] > obj.data[i]) {
					return false;
				}
			}
			return false;
		}
	}
};

