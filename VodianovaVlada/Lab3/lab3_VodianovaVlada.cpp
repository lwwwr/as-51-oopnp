#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QTextStream>
using namespace std;

void check(QString str, int r) {
     for (int i = 0; i < r; i++){
        if(str[i].isPunct()){
            str[i] = ' ';
        }
    }
  }

int max(QString str, int r) {
    int word = 0;
    int maxlen = 0;
    int index = 0;
    int pos = 0;

    for (int i = 0; i < r; i++) {
        if (str[i] == ' ' || str[i] == '.') {
            if (word > maxlen) {
                maxlen = word;
                index = i;
                pos = index - maxlen;
            }

            word = 0;
        }
        else word++;
        }

    QString x = str.mid(pos, index-pos);
    cout << x.toStdString();
    return maxlen;
}

int min (QString str, int r) {
    int word = 0;
    int minlen = r;
    int index = 0;
    int pos = 0;

    for (int i = 0; i < r; i++) {
        if (str[i] == ' ' || str[i] == '.') {
            if (word < minlen) {
                minlen = word;
                index = i;
                pos = index - minlen;
            }
            word = 0;
        }
        else word++;
    }

    QString x = str.mid(pos, index-pos);
    cout << x.toStdString();
    return minlen;
}

int main() {
    QString str;
    QTextStream inp(stdin);
    str = inp.readLine();

    int r = str.length();
    check (str, r);
    cout << "The longest word: ";
    max(str, r);
    cout << endl << "The shortest word: ";
    min(str, r);

    cin.get();
    cin.get();
    return 0;
}
