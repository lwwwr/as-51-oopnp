#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

public slots:
    void MyTimerSlot();
protected:
    void paintEvent(QPaintEvent* pe) override;

private:
    Ui::MainWindow *ui;

    QRectF* rect;
    qreal x=-5;
    qreal y=-5;

    QRectF* rect2;
    qreal x2=-4.5;
    qreal y2=-5;

    QRectF* rect3;
    qreal x3=-5;
    qreal y3=-5;
};

#endif // MAINWINDOW_H
