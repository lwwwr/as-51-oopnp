#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>
#include <QTimer>
#include <QtMath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    rect = new QRectF(5, 125, 25, 25);
    rect2 = new QRectF(5, 150, 25, 25);
    rect3 = new QRectF(5, 175, 25, 25);

    
    auto timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()),
          this, SLOT(MyTimerSlot()));

    timer->start(1000/30);
}

void MainWindow::paintEvent(QPaintEvent* pe)
{
    Q_UNUSED(pe)

    QPainter painter(this);

    painter.setFont(QFont("Sans Serif", 24));
    painter.drawText(75, 75, "Графические примитивы в библиотеке QT");

    painter.setWindow(QRect(-200, -200, 400, 400));

    painter.setBrush(Qt::red);
    painter.drawEllipse(*rect);
    painter.drawEllipse(*rect2);
    painter.drawEllipse(*rect3);
}

void MainWindow::MyTimerSlot()
{
    y = qPow(x,3);
    rect->moveTo(-x, -y);
    x = x + 0.1;

    y2 = y+25;
    rect2->moveTo(-x2, -y2);
    x2 = x;

    y3 = y2+25;
    rect3->moveTo(-x3, -y3);
    x3 = x;

    if (y > 400)
    {
        y = -5; x = -5;
        y2 = y+25; x2 = x;
        y3 = y2+25; x3 = x;
    }

    update();
}

MainWindow::~MainWindow()
{
    delete ui;
}
