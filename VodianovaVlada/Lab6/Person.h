#ifndef PERSON_H
#define PERSON_H
#include <iostream>
#include <string>

using namespace std;

class person {
public:
	static person* Begin;
	static person* End;
	person* Next;
	static void print() {
		person* tempPtr = Begin;
		while (tempPtr != End) {
			tempPtr->Show();
			tempPtr = tempPtr->Next;

		}
		End->Show();
	}

	person(const char* _name) :name(_name) {
		if (Begin == 0) {
			Begin = this;
			End = this;
		}
		else {
			End->Next = this;
			End = this;
		}
	}
	virtual void Show() = 0;
	~person() {}
protected:
	char* name;
};
person* person::Begin = 0;
person* person::End = 0;
#endif

