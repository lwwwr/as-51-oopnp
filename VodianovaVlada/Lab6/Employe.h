#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <iostream>
#include <string>
#include "person.h"

using namespace std;

class employee : public person {
public:
	employee(const char* _name, int _TimeOfWork) : person(_name), TimeOfWork(_TimeOfWork) {
	}
	void Show() {
		cout << "employee:" << endl;
		cout << "name:" << _name << " ";
		cout << "TimeOfWork:" << TimeOfWork << endl;
	}
	~employee() {}

protected:
	int TimeOfWork;
};
#endif

