#ifndef WORKER_H
#define WORKER_H
#include <iostream>
#include <string>
#include "engineer.h"
#include "person.h"

using namespace std;

class worker : public engineer
{
public:
	worker(const char* _name, int _salary, char* _profession) : engineer(_name, _salary), profession(_profession) {
	}
	void Show() {
		cout << "worker:" << endl;
		cout << "name:" << _name << " ";
		cout << "salary:" << salary << " ";
		cout << "profession:" << profession << endl;
	}
	~worker() {}

protected:
	char* profession;
};
#endif
