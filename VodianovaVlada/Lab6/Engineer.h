#ifndef ENGINEER_H
#define ENGINEER_H
#include <iostream>
#include <string>
#include "person.h"

using namespace std;

class engineer : public person {
public:
	engineer(const char* _name, int _salary) : person(_name), salary(_salary) {
	}
	void Show() {
		cout << "engineer:" << endl;
		cout << "name:" << _name << " ";
		cout << "salary:" << salary << endl;
	}
	~engineer() {}
protected:
	int salary;
};
#endif

