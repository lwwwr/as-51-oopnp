#include <fstream>
#include <iostream>
#include <string>
#include <stdbool.h>
using namespace std;
int main()
{
	ifstream inputStream;
	ofstream outputStream;
	char buffer[255];
	bool isCommented = false;
	inputStream.open("D:\\input.txt", ios::in);
	outputStream.open("D:\\output.txt", ios::out);
	while (!inputStream.eof())
	{
		inputStream.getline(buffer, sizeof(buffer));
		char *out;
		if (isCommented)
		{
			out = strtok(buffer, "*/");
			if (out == NULL)
				continue;
			else
			{
				char *buffer2 = &buffer[2];
				outputStream.write(buffer2, sizeof(char)*strlen(buffer2));
				isCommented = false;
			}
		}
		out = strtok(buffer, "//");
		if (out != NULL)
		{
			outputStream.write(out, sizeof(char)*strlen(out));
			continue;
		}
		out = strtok(buffer, "/*");
		if (out != NULL)
		{
			isCommented = true;
			outputStream.write(out, sizeof(char)*strlen(out));
			continue;
		}
		outputStream.write(buffer, sizeof(buffer));
	}
	inputStream.close();
	outputStream.close();
	system("pause");
	return 0;
}
void clean(char *buffer)
{
	for ( int i = 0; i < 255; i++)
	{
		 buffer[i] = '\0';
	}
}
