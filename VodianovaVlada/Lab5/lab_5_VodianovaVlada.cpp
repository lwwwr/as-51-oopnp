#include <iostream>
#include <QVector>
#include <QVectorIterator>
#include <math.h>

using namespace std;

int main() {
    QVector<int> vector;
    QVector<int> vector1;
    cout << "Enter elements (\"*\" - to exit): ";
    int value;
    char* str = new char[255];
    while (true) {
        cin >> str;

        if (strcmp(str, "*") == 0)
            break;

        value = (int) atof(str);
        vector << value;
    }
    cout << "TEST: ";
    for (int i = 0; i < vector.size(); i++)
        cout << vector[i] << " ";

    QVectorIterator<int> iter(vector);
    int firstIndex = -1;
    int secondIndex = -1;
    int i = 0;
    while (iter.hasNext()) {
        if (iter.peekNext() < 0) {
            secondIndex = (secondIndex == -1 && firstIndex != -1) ? i : secondIndex;
            firstIndex = (firstIndex == -1) ? i : firstIndex;
        }
        i++;
        iter.next();
    }

    int sum = 0;
    i = 0;
    for (iter.toFront(); i < secondIndex; iter.next(), i++) {
        if (i > firstIndex) {
            sum += iter.peekNext();
        }
    }

    cout << "\nSUM: " << sum;

    for (int i = 0; i < vector.size(); i++) {
        if (vector[i]<0){
            vector1.append(vector[i]);
        }
    }

    for (int i = 0; i < vector.size(); i++) {
        if (vector[i]=0){
            vector1.append(vector[i]);
        }
    }

    for (int i = 0; i < vector.size(); i++) {
        if (vector[i]>0){
            vector1.append(vector[i]);
        }
    }

    cout << "\nTEST SORT: ";
    for (int i = 0; i < vector1.size(); i++)
        cout << vector1[i] << " ";
}
