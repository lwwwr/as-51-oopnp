#include <QString>
#include <QTextStream>
#include <iostream>

QTextStream in(stdin);

using std:: cout;

QString task(QString);

int main()
{
    QString str;
    cout << "input sentens: \n";
    str = in.readLine();
    cout << "vyvod: " << task(str).toStdString();
}

QString task(QString str){

    QString num, alpha;
    str.remove(QRegExp("\\W+"));
    QStringList elements = str.split("");

    for(int i = 0; i < elements.count(); i++){
        if(elements[i].toInt() || elements[i] == "0")
            num += elements[i];
        else
            alpha += elements[i];
    }

    return alpha + num;
}
