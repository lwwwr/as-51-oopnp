#include <iostream>

using namespace std;

double dfoo(double da, double db){
	double	
		d1 = da - db,
		d2 = d1 * d1 * d1,
		d3 = da * da * da,
		d4 = 3 * da * db * db,
		d5 = db *db *db,
		d6 = 3 * da * da * db,
		d7 = d2 - (d3 - d4),
		d8 = d5 - d6,
		rezd = d7/d8;
	
	return rezd;
}

float ffoo(float da, float db){
	float	
		d1 = da - db,
		d2 = d1 * d1 * d1,
		d3 = da * da * da,
		d4 = 3 * da * db * db,
		d5 = db *db *db,
		d6 = 3 * da * da * db,
		d7 = d2 - (d3 - d4),
		d8 = d5 - d6,
		rezd = d7/d8;
	
	return rezd;
}



int main(){
	setlocale(LC_ALL, "rus");
	
	double da = 1000,
	db = 0.0001;
	
	float fa = 1000,
	fb = 0.0001;
	
	cout<<"float: "<<ffoo(fa, fb)<<endl;
	cout<<"double: " << dfoo(da, db) << endl;
	
	int n, m;
	
	cout<<endl<<"n = ";
	cin>>n;
	cout<<"m = ";
	cin>>m;
	cout << endl;
	cout << "m- ++n = " << (m - ++n) << endl;
	cout << "(1 - true, 0 - false)" << endl;
	cout << "m++ < ++n = " << (m++ < ++n) << endl;
	cout << "n-- < --m = " << (n-- < --m) <<endl;
}
