#include "pch.h"
#include "List.h"
#include "PrintEdition.h"
#include "Book.h"
#include "Magazine.h"
#include "TextBook.h"

int main() {
	Book a("solnyshko", 2000, "Dosoevsky");
	Magazine b("oblachko", 2018, "Auto");
	TextBook c("solnyshko", 2015, "malyhina C.A.", "English language");
	Book d(a); // пример конструктора копирования
	List::ShowList();
}