#pragma once

#include "PrintEdition.h"

class PrintEdition;

class List {
private:
	friend PrintEdition;
	class Node {
	public:
		Node(PrintEdition*);
		Node();
		PrintEdition *aElem;
		Node *apNext;
	};
	static Node *apHead;
	static void AddInList(PrintEdition*);
public:
	static void ShowList();
	List();
	~List();
};

List::Node::Node(PrintEdition *elem) :
	aElem(elem),
	apNext(nullptr)
{}
List::Node::Node() :
	apNext(nullptr),
	aElem(nullptr)
{}

List::Node* List::apHead = nullptr;
void List::AddInList(PrintEdition *pElem) {
	Node *pNode = new Node(pElem);
	if (!apHead) apHead = pNode;
	else {
		pNode->apNext = apHead;
		apHead = pNode;
	}
}
void List::ShowList() {
	for (Node *pCur = apHead; pCur; pCur = pCur->apNext)
		pCur->aElem->ShowElem();
}
List::List() {}
List::~List() {}