#pragma once
#include "Book.h"
class TextBook :
	public Book
{
protected:
	string aSpecific;
public:
	TextBook(string, int, string, string);
	TextBook(const TextBook&);
	TextBook();
	void ShowElem() override;
	~TextBook();
};

TextBook::TextBook(string publHouse, int year, string autor, string specific) :
	Book(publHouse, year, autor),
	aSpecific(specific)
{}
TextBook::TextBook(const TextBook &obj) :
	TextBook(obj.aPublHouse, obj.aYear, obj.aAutor, obj.aSpecific)
{}
TextBook::TextBook() : TextBook("???", 0, "???", "???") {}

void TextBook::ShowElem() {
	cout << "(TextBook) publ. house: " << aPublHouse
		<< "year: " << aYear << " autor: " << aAutor
		<< "specification: " << aSpecific << endl;
}
TextBook::~TextBook() { }