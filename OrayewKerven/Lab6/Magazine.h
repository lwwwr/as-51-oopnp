#pragma once
#include "PrintEdition.h"
class Magazine :
	public PrintEdition {
protected:
	string aAbout;
public:
	Magazine(const Magazine&);
	Magazine(string, int, string);
	Magazine();
	~Magazine();
	void ShowElem() override;
};

Magazine::Magazine(string publHouse, int year, string about) :
	PrintEdition(publHouse, year),
	aAbout(about)
{}

Magazine::Magazine(const Magazine &obj) :
	Magazine(obj.aPublHouse, obj.aYear, obj.aAbout)
{}

Magazine::Magazine() : Magazine("???", 0, "???") {}

Magazine::~Magazine() {}

void Magazine::ShowElem() {
	cout << "(book) publ. House: " << aPublHouse << " year: " << aYear << " About: " << aAbout << endl;
}