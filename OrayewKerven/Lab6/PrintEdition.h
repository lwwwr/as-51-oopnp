#pragma once

#include "List.h"
#include <string>
#include <iostream>


using std::string;
using std::cout;
using std::endl;

class List;

class PrintEdition {
protected:
	string aPublHouse;
	int aYear;
	void Add();
public:
	PrintEdition();
	PrintEdition(const PrintEdition&);
	PrintEdition(string, int);
	~PrintEdition();
	virtual void ShowElem() = 0;
};

PrintEdition::PrintEdition(string publHouse, int year) :
	aPublHouse(publHouse),
	aYear(year) {
	Add();
}

PrintEdition::PrintEdition(const PrintEdition &obj) :
	PrintEdition(obj.aPublHouse, obj.aYear)
{}

PrintEdition::PrintEdition() : PrintEdition("???", 0) {}

PrintEdition::~PrintEdition() {}

void PrintEdition::Add() {
	List::AddInList(this);
}
