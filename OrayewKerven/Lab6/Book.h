#pragma once

#include "PrintEdition.h"

class Book :
	public PrintEdition {
protected:
	string aAutor;
public:
	Book(string, int, string);
	Book();
	Book(const Book&);
	~Book();
	void ShowElem() override;
};

Book::Book(string publHouse, int year, string autor) :
	PrintEdition(publHouse, year),
	aAutor(autor)
{}
Book::Book(const Book &obj) :
	Book(obj.aPublHouse, obj.aYear, obj.aAutor)
{}
Book::Book() : Book("???", 0, "???") {}

void Book::ShowElem() {
	cout << "(book) publ. House: " << aPublHouse << " year: " << aYear << " Autor: " << aAutor << endl;
}

Book::~Book() {}