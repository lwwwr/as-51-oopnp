﻿#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm> // for sort()
#include <fstream>

using namespace std;

struct elem {
	string str;
	int count;
	elem(const string &s, int c) : str(s), count(c) {}
	bool operator < (const elem &el) {
		return el.count < count;
	}
};

void push(const string &word, vector<elem> &listElem) {
	bool isKept = false;
	for (int i = 0; i < listElem.size(); i++)
		if (listElem[i].str == word) {
			(listElem[i].count)++;
			isKept = true;
			return;
		}
	if (!isKept) {
		elem el(word, 1);
		listElem.push_back(el);
	}
	return;
}

void task() {
	setlocale(LC_ALL, "rus");
	ifstream fin("InPut.txt");
	if (!fin.is_open()) {
		cout << "file don't open";
		return;
	}
	fin.seekg(0, ios_base::end);
	int size = fin.tellg();
	fin.seekg(0, ios_base::beg);
	char *file = new char[size + 1];
	fin.read(file, size);
	fin.close();
	vector <elem> listElem;
	for (int i = 0; i < size; i++) {
		if (file[i] != ' ' && (file[i - 1] == ' ' || i == 0)) {
			string word;
			while (file[i] != ' ' && i < size) {
				word += file[i];
				i++;
			}
			push(word, listElem);
		}
	}
	sort(listElem.begin(), listElem.end());
	for (int i = 0; i < listElem.size(); i++)
		cout << listElem[i].count << " " << listElem[i].str << endl;
	delete[] file;
}

int main() {
	task();
	return 0;
}