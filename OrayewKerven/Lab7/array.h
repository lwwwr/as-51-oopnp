#pragma once
#include <cstring>
#include <iostream>

const int MAX_SIZE = 225;

class Array {
private:
	char *apArr;
	static const int MAX;
	int aSize;
	void Copy(const char*, int);
	void PushBack(char);
public:
	Array();
	Array(const Array&);
	Array(const char*, int);
	~Array();
	void InPut();
	void Print();
	bool operator > (char ch); //�������� �� ��������������(char in set �������)
	Array& operator * (const Array&);//����������� ���������
	bool operator < (const Array&); //�������� �� �����������
};

bool Array::operator<(const Array&obj) {
	if (aSize < obj.aSize) return false;
	bool ok = false;
	for (int i = 0; i < obj.aSize; i++) {
		for (int j = 0; j < aSize; j++) {
			if (obj.apArr[i] == apArr[j]) {
				ok = true;
				break;
			}
		}
		if (ok) ok = false;
		else return false;
	}
	return true;
}

Array& Array::operator*(const Array& obj) {
	Array *newArr = new Array;
	int k = 0;
	for (int i = 0; i < obj.aSize; i++)
		for (int j = 0; j < aSize; j++) {
			if (obj.apArr[i] == apArr[j]) {
				newArr->PushBack(apArr[j]);
				break;
			}
		}
	return *newArr;
}

bool Array::operator>(char ch) {
	for (int i = 0; i < aSize; i++)
		if (ch == apArr[i]) return true;
	return false;
}

Array::Array(const char *pArr, int size) : apArr(nullptr), aSize(size) {
	Copy(pArr, size);
};

void Array::Copy(const char *pArr, int size) {
	if (size > MAX) return;
	if (apArr) delete apArr;
	aSize = size;
	apArr = new char[size];
	for (int i = 0; i < size; i++)
		apArr[i] = pArr[i];
}

const int Array::MAX = MAX_SIZE;

Array::Array() : apArr(nullptr), aSize(0) {};

Array::Array(const Array &obj) {
	Copy(obj.apArr, obj.aSize);
}

Array::~Array() {
	delete[] apArr;
}

void Array::InPut() {
	char ch;
	while (aSize < MAX) {
		std::cin >> ch;
			if (ch == '.') break;
			else PushBack(ch);
	}
}

void Array::PushBack(const char ch) {
	if ((*this) > ch) return;
	char *pNewArr = new char[aSize + 1];
	for (int i = 0; i < aSize; i++)
		pNewArr[i] = apArr[i];
	pNewArr[aSize++] = ch;
	delete[] apArr;
	apArr = pNewArr;
}

void Array::Print() {
	for (int i = 0; i < aSize; i++)
		std::cout << apArr[i] << " ";
	std::cout << std::endl;
}