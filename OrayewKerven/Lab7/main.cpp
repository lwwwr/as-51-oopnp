﻿// lab7_6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "array.h"

int main() {
	Array arr1;
	std::cout << "input arr1: ";
	arr1.InPut();
	Array arr2;
	std::cout << "input arr2: ";
	arr2.InPut();
	std::cout << "\n\narr1: ";
	arr1.Print();
	std::cout << "\narr2: ";
	arr2.Print();
	std::cout << "\n\naffiliation check 'a' int arr1: ";
	arr1 > 'a' ? std::cout << "true" : std::cout << "false";
	std::cout << "\narr2 is subset arr1: ";
	arr1 < arr2 ? std::cout << "true" : std::cout << "false";
	std::cout << "\nintersection of arr2 and arr1: ";
	(arr1 * arr2).Print();
}