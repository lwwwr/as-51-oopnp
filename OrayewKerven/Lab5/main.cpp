#include <iostream>
#include <QVector>
#include <math.h>

void task1() {
    using std :: cout;

    QVector<double> v = {0.1, 0.5, 6,7,-5,-0.2, 3, -0.8};
    QVector<double> rez, rez2;

    foreach(double el, v)
        if (fabs(el) < 1) rez += el;
        else rez2 += el;
    rez += rez2;

    cout << "task1:\n";
    foreach (double el, v) cout << el << " ";
    cout << "\n----------------\n";
    foreach (double el, rez) cout << el << " ";
}

void task2() {
    using std :: cout;

    cout << "\n\ntask 2:\n";

    int rez = 0;
    QVector<int> v = {-3,4,6,2,-8,9,-6,-7};
    int pos;
    bool ok = false;

    if (v.isEmpty()) return;
    for (int i = v.indexOf(v.last() - 1); i >= 0; i--)
        if(v[i] > 0) {
            pos = v[i];
            ok = true;
            break;
        }
    if (!ok) return;
    for (int i = 0; i <= v.indexOf(pos); i++) rez += v[i];
    foreach (int el, v) cout << el << " ";
    cout <<"\nrez: " << rez;
}

int main(){
    task1();
    task2();
}
