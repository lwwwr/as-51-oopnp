#pragma once
#include "personnel.h"
class worker :
	public personnel {
protected:
	int aWorkExperience;
public:
	worker(const worker&);
	worker();
	worker(string, int);
	~worker();
	void Print() override;
};

worker::worker(const worker &el) : worker(el.aName, el.aWorkExperience) {}
worker::worker(string name, int experience) : personnel(name), aWorkExperience(experience) {}
worker::worker() : worker("???", 0) {}
worker::~worker() {}
void worker::Print() {
	cout << "worker. " << "name: " << aName << "; work Experince " << aWorkExperience << endl;
}