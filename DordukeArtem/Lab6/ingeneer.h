#pragma once
#include "ingeneer.h"
class ingeneer :
	public worker {
protected:
	int aCategory;
public:
	ingeneer(const ingeneer&);
	ingeneer();
	ingeneer(string, int, int);
	~ingeneer();
	void Print() override;
};

ingeneer::ingeneer(const ingeneer &el) : ingeneer(el.aName, el.aWorkExperience, el.aCategory) {}
ingeneer::ingeneer(string name, int experience, int category) : worker(name, experience), aCategory(category)  {}
ingeneer::ingeneer() : ingeneer("???", 0, 0) {}
ingeneer::~ingeneer() {}
void ingeneer::Print() {
	cout << "ingeneer. " << "name: " << aName << "; work Experince: " << aWorkExperience <<
		"; cftegory: " << aCategory << endl;
}

