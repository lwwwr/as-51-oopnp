#pragma once
#include "personnel.h"
class administration :
	public personnel {
protected:
	string aOfWhat;
public:
	administration(const administration&);
	administration();
	administration(string, string);
	~administration();
	void Print() override;
};

administration::administration(const administration &el) : administration(el.aName, el.aOfWhat) {}
administration::administration(string name, string what) : personnel(name), aOfWhat(what) {}
administration::administration() : administration("???", "???") {}
administration::~administration() {}
void administration::Print() {
	cout << "Administration. " << "name: " << aName << "; of " << aOfWhat << endl;
}