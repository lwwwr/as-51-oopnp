#pragma once

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

class list;

class personnel {
protected:
	string aName;
public:
	personnel(const personnel&);
	personnel();
	personnel(string name);
	~personnel();
	void Add();
	virtual void Print() = 0;
};

class node{
public:
	node();
	node(personnel*);
	friend list;
private:
	personnel *apPers;
	node *apNext;
};

node::node() : apPers(nullptr), apNext(nullptr) {}
node::node(personnel *el) : apPers(el), apNext(nullptr) {}

class list {
private:
	static node *apBeg;
public:
	static void AddInList(personnel*);
	static void Print();
};

personnel::personnel(string name) : aName(name) {}
personnel::personnel(const personnel &el) : personnel(el.aName) {}
personnel::personnel() : personnel("???") {}
personnel::~personnel() {}
void personnel::Add() {
	list::AddInList(this);
}

node* list::apBeg = nullptr;
void list::AddInList(personnel *el) {
	node *pTemp = new node(el);
	if (apBeg) 
		pTemp->apNext = apBeg;
	apBeg = pTemp;
}
void list::Print() {
	for (node *pCur = apBeg; pCur; pCur = pCur->apNext)
		pCur->apPers->Print();
}