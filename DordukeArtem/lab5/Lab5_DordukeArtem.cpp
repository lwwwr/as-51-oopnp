#include <iostream>
#include <QVector>

void Task1(const QVector<int> &vector) {
    QVector<int> vect;
    foreach(int elem, vector)
        if(elem >= 0) vect += elem;
    std::cout << "result of TASK1: ";
    foreach(int elem, vect) std::cout << elem << " ";
    std::cout << std::endl;
}

void Task2 (const QVector<int> &vector) {
    int sum = 0;
    QVector<int>::const_iterator i;
    QVector<int>::const_iterator j;
    for (i = vector.begin(); i != vector.end(); ++i)
        if (*i == 0) break;
    for (j = vector.end() - 1; j != vector.begin(); --j)
        if(*j == 0) break;
    for (i; i <= j; i++)
        sum += *i;
    std::cout << "rezult of TASK2: " << sum;
}

int main() {
    QVector <int> vector = {2, -5, 0, 6, -7, 2, 0, 5, -9, 0, 8, -2};
    std::cout << "initial vector: ";
    foreach(int elem, vector) std::cout << elem << " ";
    std::cout << "\n\n";
    Task1(vector);
    Task2(vector);
}
