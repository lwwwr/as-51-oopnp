﻿#include "pch.h"
#include "Array.h"


using namespace std;

int main() {
	Arr arr1, arr2;
	arr1.Input();
	arr2.Input();
	cout << "arr1: ";
	arr1.Print();
	cout << "arr2: ";
	arr2.Print();
	cout << "arr1 - 'd': ";
	(arr1 - 'd').Print();
	cout << "arr1 > arr2: " << (arr1 > arr2);
	cout << "\narr1 != arr2: " << (arr1 != arr2);
}