#pragma once
#include <iostream>

using std::cout;
using std::endl;
using std::cin;

class Arr {
private:
	char *apMas;
	static int MAX_SIZE;
	int aSize;
public:
	Arr(const Arr&);
	Arr();
	void Add(char);
	~Arr();
	bool operator > (const Arr&);
	bool operator != (const Arr&);
	Arr& operator -(char);
	void Input();
	void Print();
};

void Arr::Print() {
	for (int i = 0; i < aSize; i++)
		cout << apMas[i] << " ";
	cout << endl;
}

void Arr::Input() {
	while (true) {
		char ch;
		cin >> ch;
		if (ch == '.') return;
		else Add(ch);
	}
}

Arr::Arr() : aSize(0), apMas(nullptr) {}
Arr::Arr(const Arr &mas) {
	for (int i = 0; i < mas.aSize; i++)
		Add(mas.apMas[i]);
}

void Arr::Add(char ch) {
	char *pNewMas = new char[aSize + 1];
	bool ok = true;
	for (int i = 0; i < aSize; i++) {
		if (ch == apMas[i])
			ok = false;
		pNewMas[i] = apMas[i];
	}
	if (ok) pNewMas[aSize++] = ch;
	delete[] apMas;
	apMas = pNewMas;
}

Arr& Arr::operator-(char ch) {
	Arr *pNewArr = new Arr;
	int j = 0;
	for (int i = 0; i < aSize; i++) {
		if (apMas[i] == ch) continue;
		pNewArr->Add(apMas[i]);
		j++;
	}
	pNewArr->aSize = j;
	return *pNewArr;
}

bool Arr::operator!=(const Arr &Mas) {
	return (aSize != Mas.aSize);
}

bool Arr::operator>(const Arr &Mas) {
	if (aSize < Mas.aSize) return false;
	bool ok = true;
	for (int i = 0; i < aSize; i++) {
		for (int j = 0; j < Mas.aSize; j++) {
			if (apMas[i] == Mas.apMas[j]) {
				ok = true;
				break;
			}
		}
		if (!ok) return false;
		else ok = false;
	}
	return true;
}

Arr::~Arr() {
	delete[] apMas;
}