#include "stdafx.h"
#include <iostream>
#include <string>
#include <regex>

using namespace std;

int main()
{
	string str;
	while (cin >> str)
	{
		cmatch result;
		regex regular("(http|https)"
			"(:)"
			"(//)"
			"(www.){0,1}"
			"([\\w\\W\./]+)");
		if (regex_search(str.c_str(), result, regular)) {
			for (int i = 1; i < result.size(); i++)
				cout << result[i];
			cout << endl;
		}
	}
	return 0;
}