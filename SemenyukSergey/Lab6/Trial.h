#pragma once
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

class Trial {
protected:
	string aName;
public:
	Trial(string);
	Trial(const Trial&);
	Trial();
	~Trial();
	virtual void PrintInfo() = 0;
	void Add();
};

struct Node {
	Node(Trial*);
	Node();
	Node *apNext;
	Trial *apTrial;
};

Node::Node(Trial *pObj) : apNext(nullptr), apTrial(pObj) {}
Node::Node() : apNext(nullptr), apTrial(nullptr) {}

class TrialList {
	friend Trial;
private:
	static Node *apBeg;
	static void AddInList(Trial*);
public:
	static void View();
};

Node* TrialList::apBeg = nullptr;
void TrialList::View() {
	Node *pTemp = apBeg;
	while (pTemp) {
		pTemp->apTrial->PrintInfo();
		pTemp = pTemp->apNext;
	}
}

Trial::Trial(string name) : aName(name) {}
Trial::Trial() : Trial("???") {}
Trial::Trial(const Trial &obj) : Trial(obj.aName) {}
void Trial::Add() {
	TrialList::AddInList(this);
}
Trial::~Trial() {}

void TrialList::AddInList(Trial *el) {
	Node *pTemp = new Node(el);
	if (!apBeg) apBeg = pTemp;
	else {
		pTemp->apNext = apBeg;
		apBeg = pTemp;
	}
}