#pragma once
#include "Exam.h"
class FinalExam :
	public Exam {
protected:
	int aPassingScore;
public:
	FinalExam(const FinalExam&);
	FinalExam(string, int, int);
	FinalExam();
	~FinalExam();
	void PrintInfo() override;
};

FinalExam::FinalExam(const FinalExam &obj) : FinalExam(obj.aName, obj.aMarkForQuestion, obj.aPassingScore) {}
FinalExam::FinalExam(string name, int qant, int passingScore) : Exam(name, qant), aPassingScore(passingScore) {}
FinalExam::FinalExam() : FinalExam("???", 0, 0) {}
FinalExam::~FinalExam() {}
void FinalExam::PrintInfo() {
	cout << "Final exam " << aName << ", mark for question: " << aMarkForQuestion << "score; " << "Passing score: " << aPassingScore << " score" << endl;
}