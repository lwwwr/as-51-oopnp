#pragma once
#include "Trial.h"
class Test :
	public Trial {
private:
	int aQuantOfTests;
public:
	Test(const Test&);
	Test(string, int);
	Test();
	~Test();
	void PrintInfo() override;
};

Test::Test() : Test("???", 0) {}
Test::Test(string name, int quantity) : Trial(name), aQuantOfTests(quantity) {}
Test::Test(const Test &obj) : Test(obj.aName, obj.aQuantOfTests) {}
void Test::PrintInfo() {
	cout << "Test " << aName << ", qantity: " << aQuantOfTests << endl;
}
Test::~Test() {}

