#include "stdafx.h"
#include "Test.h"
#include "Trial.h"
#include "Exam.h"
#include "FinalExam.h"

int main() {
	Test test1("name of test", 10);
	Test test2;
	Exam exam1("name of exam", 1);
	FinalExam f("name of finalExam", 2, 200);
	test1.Add();
	test2.Add();
	exam1.Add();
	f.Add();
	TrialList::View();
}
