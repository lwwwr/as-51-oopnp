#pragma once
#include "Trial.h"
class Exam :
	public Trial {
protected:
	int aMarkForQuestion;
public:
	Exam(string, int);
	Exam(const Exam&);
	Exam();
	~Exam();
	void PrintInfo() override;
};

Exam::Exam(string name, int mark) : Trial(name), aMarkForQuestion(mark) {}
Exam::Exam(const Exam &obj) : Exam(obj.aName, obj.aMarkForQuestion) {}
Exam::Exam() : Exam("???", 0) {}
Exam::~Exam() {}
void Exam::PrintInfo() {
	cout << "Exam. " << aName << ", mark for question: " << aMarkForQuestion << " score" << endl;
}