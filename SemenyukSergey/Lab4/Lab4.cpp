#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>

int Difference(const std:: string &FileStr1, const std:: string &FileStr2) {
	int i = 0, j = 0;
	while (i < FileStr1.length() && j < FileStr2.length()) {
		if (FileStr1[i] != FileStr2[j]) {
			return (i + 1);
		}
		i++;
		j++;
	}
	return 0;
}

int main() {
	std:: ifstream file1("Name1.txt");
	std:: ifstream file2("Name2.txt");
	std:: string FileStr1, FileStr2;
	int NumString = 0, ItemNumber = 0, difference = 0;

	if (!file1.is_open() || !file2.is_open()) {
		std:: cout << "File not found" << std:: endl;
		return 0;
	}

	while (file1 && file2) {
		NumString++;
		getline(file1, FileStr1);
		getline(file2, FileStr2);

		ItemNumber = Difference(FileStr1, FileStr2);
		if (ItemNumber) {
			difference++;
			break;
		}
	}
	file1.close();
	file2.close();

	if (difference) {
		std:: cout << "String: " << NumString << " Symbol: " << ItemNumber << std:: endl;
}
	else {
		std:: cout << "files are identical" << std:: endl;
	}

	return 0;
}
