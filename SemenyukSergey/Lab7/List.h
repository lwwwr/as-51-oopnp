#pragma once

#include <iostream>

struct Node {
	Node();
	Node(char);
	Node(const Node*);
	Node *apNext;
	char aElem;
};

Node::Node() : apNext(nullptr) {}
Node::Node(char elem) : apNext(nullptr), aElem(elem) {}
Node::Node(const Node *obj) : apNext(nullptr), aElem(obj->aElem) {}

class List {
private:
	int size;
	Node *apBeg;
public:
	void PushEnd(char);
	char PopBeg();
	List& operator +(const List&);
	List& operator --();
	bool operator ==(const List&);
	void Input();
	void Print();
	List();
	List(char);
	List(const List&);
	void Add(char);
	~List();
};

List::List(char elem) : List() {
	Add(elem);
}

List::List(const List &obj) : List() {
	Node *pTmp = obj.apBeg;
	while (pTmp) {
		Add(pTmp->aElem);
		pTmp = pTmp->apNext;
	}
}

List::List() : size(0), apBeg(nullptr) {}

List& List::operator --() {
	PopBeg();
	return *this;
}

List& List::operator+(const List &obj) {
	List *pNewList = new List(*this);
	Node *pCur = obj.apBeg;
	while (pCur) {
		pNewList->Add(pCur->aElem);
		pCur = pCur->apNext;
	}
	return *pNewList;
}

bool List::operator ==(const List &obj) {
	return (size == obj.size);
}

void List::Input() {
	char ch;
	while (std::cin >> ch) {
		if (ch == '.') return;
		Add(ch);
	}
}

void List::Print() {
	Node *pCur = apBeg;
	while (pCur) {
		std::cout << pCur->aElem << " ";
		pCur = pCur->apNext;
	}
	std::cout << std::endl;
}

void List::Add(char elem) {
	if (!apBeg) {
		Node *pNewNode = new Node(elem);
		apBeg = pNewNode;
	}
	else PushEnd(elem);
}

void List::PushEnd(char elem) {
	Node *pNewNode = new Node(elem);
	Node *pCur = apBeg;
	while (pCur->apNext) {
		if (pCur->aElem == elem) {
			delete pNewNode;
			return;
		}
		pCur = pCur->apNext;
	}
	pCur->apNext = pNewNode;
	size++;
}

char List::PopBeg() {
	char elem = apBeg->aElem;
	Node *pTemp = apBeg;
	apBeg = apBeg->apNext;
	delete pTemp;
	size--;
	return elem;
}

List::~List() {
	while (apBeg) PopBeg();
}