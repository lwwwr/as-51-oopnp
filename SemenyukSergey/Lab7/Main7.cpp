#include "stdafx.h"
#include "List.h"
#include <iostream>

using std::endl;
using std::cout;

int main() {
	List list1;
	List list2;
	cout << "INPUT LIST1: ";
	list1.Input();
	list1.Print();
	cout << "INPUT LIST2: ";
	list2.Input();
	list2.Print();
	cout << "list1 + list2: ";
	(list1 + list2).Print();
	cout << "--list1: ";
	--list1;
	list1.Print();
	cout << "list1 == list2: " << (list1 == list2);

}

