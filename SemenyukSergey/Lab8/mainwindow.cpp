﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	{
		ui->setupUi(this);
		setFixedSize(600,600);
		pTimer = new QTimer();
		connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
		pTimer->setInterval(500);
		pTimer->start();
	}
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}
void MainWindow::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
	int id = QFontDatabase::addApplicationFont("matrix_font.ttf"); //adding matrix-font to project
		QString family = QFontDatabase::applicationFontFamilies(id).at(0); //set matrix-font as family[0]
		QFont f(family);  // creating font with matrix-font's family

	p.rotate(90);
	p.drawText(QPoint(10,-20),"LAB8 QPainter");
	p.rotate(-90);
	ry = 50 * sin(rx*rx) + 100;
	rx+=5;
	p.setPen( QPen( Qt::green, 3 ) );
	p.drawEllipse(rx*5, ry, 40, 40);
	p.drawEllipse(rx*5-10, ry+40, 60, 60);
	p.drawEllipse(rx*5-20, ry+100, 80, 80);

}
