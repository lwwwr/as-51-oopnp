#include <QVector>
#include <iostream>
void Task1(){
    std::cout << "-------task1-------\n";
    QVector <int> vector = { 2, 0 ,6, 5, 7, 0, 8, 9, 0 }, rez, num;
    std::cout << "initial vector: ";
    foreach(int elem, vector)
            std::cout << elem << " ";
    std::cout << std::endl;
    foreach(int elem, vector)
        if(elem == 0) rez += elem;
        else num += elem;
    rez += num;
    std::cout << "rezult: ";
    foreach(int elem, rez)
        std::cout << elem << " ";
    std::cout << std::endl;
}

void Task2(){
    std::cout << "-------task2-------\n";
    QVector <int> vector = { 2, 0 ,6, 5, 7, 0, 8, 9, 0 };
    QVector <int> :: iterator i1 = vector.begin(),
            i2 = vector.end();
    int rez = 0;
    for (i1; i1 != vector.end(); ++i1)
        if((*i1) > 0) break;
    for (i2; i2 != vector.begin(); ++i2)
        if((*i2) > 0) break;
    for(i1; i1 != i2; ++i1)
        rez += (*i1);
    std::cout << "initial vector: ";
    foreach(int elem, vector)
        std::cout << elem << " ";
    std::cout << "\nrezult :" << rez;

}

int main() {
   Task1();
   Task2();
}
