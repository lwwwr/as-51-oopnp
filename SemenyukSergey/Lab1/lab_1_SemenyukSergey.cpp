#include "stdafx.h"
#include <iostream>
using namespace std;
int main()
{
	const int a = 1000;
	const float b = 0.0001;
	float f1, f2, f3, f4, f5, fresult;
	f1 = (a - b)*(a - b)*(a - b);
	f2 = a * a*a;
	f3 = b * b*b;
	f4 = 3 * a*b*b;
	f5 = 3 * a*a*b;
	fresult = (f1 - f2) / (f3 - f4 - f5);
	cout << "float: " << fresult << endl;
	const double b1 = 0.0001;
	double d1, d2, d3, d4, d5, dresult;
	d1 = (a - b1)*(a - b1)*(a - b1);
	d2 = a * a*a;
	d3 = b1 * b1*b1;
	d4 = 3 * a*b1*b1;
	d5 = 3 * a*a*b1;
	dresult = (d1 - d2) / (d3 - d4 - d5);
	cout << "double: " << dresult << endl;
	int m, n = 0;
	cout << "Enter m and n: ";
	cin >> m >> n;
	cout << "m+--n " << m + --n << endl;
	cout << "m++ < ++n " << (m++ < ++n) << endl;
	cout << "n-- < --m " << (n-- < --m) << endl;
	return 0;
}