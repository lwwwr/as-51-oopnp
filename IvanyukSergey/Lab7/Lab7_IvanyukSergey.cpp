#include "pch.h"
#include "Array.h"

int main() {
	Overload Arr1, Arr2;
	Arr1.Input();
	Arr2.Input();
	std::cout << "\nArray 1:\t\t\t";
	Arr1.Print();
	std::cout << "Array 2:\t\t\t";
	Arr2.Print();
	std::cout << "(Array 1) - 'a':\t\t";
	(Arr1 - 'a').Print();
	std::cout << "(Array 1) > (Array 2):\t\t" << (Arr1 > Arr2) << std::endl;
	std::cout << "(Array 1) != (Array 2):\t\t" << (Arr1 != Arr2) << std::endl;
	return 0;
}