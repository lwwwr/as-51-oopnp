#pragma once
#include <iostream>
#include <string>
#define MAX 255

class Overload {
private:
	char *pBegin;
	static const int maxSize;
	int currSize;
public:
	void Add(char);
	bool operator > (const Overload&);
	bool operator != (const Overload&);
	Overload& operator -(char);
	void Input();
	void Print();
	Overload(const Overload&);
	Overload();
	~Overload();
};

const int Overload::maxSize = MAX;

void Overload::Input() {
	int i = 0;
	do{
		char element;
		std::cin >> element;
		Add(element);
		i++;
	} while (std::cin.peek() != '\n' && i < maxSize);
	return;
}

void Overload::Print() {
	for (int i = 0; i < currSize; i++)
		std::cout << pBegin[i] << " ";
	std::cout << std::endl;
}

Overload::Overload() : currSize(0), pBegin(nullptr) {}
Overload::Overload(const Overload &arr) {
	for (int i = 0; i < arr.currSize; i++)
		Add(arr.pBegin[i]);
}

void Overload::Add(char element) {
	char *pNewArr = new char[currSize + 1];
	bool newArr = true;
	for (int i = 0; i < currSize; i++) {
		if (element == pBegin[i]) newArr = false;
		pNewArr[i] = pBegin[i];
	}
	if(newArr) pNewArr[currSize++] = element;
	delete[] pBegin;
	pBegin = pNewArr;
}

Overload& Overload::operator-(char element) {
	Overload *pNewArr = new Overload;
	int j = 0;
	for (int i = 0; i < currSize; i++) {
		if (pBegin[i] == element) continue;
		pNewArr->Add(pBegin[i]);
		j++;
	}
	pNewArr->currSize = j;
	return *pNewArr;
}

bool Overload::operator>(const Overload &arr) {
	if (currSize < arr.currSize) return false;
	std::string str1(pBegin, currSize);
	std::string str2(arr.pBegin,arr.currSize);
	if (str1.find(str2) != std::string::npos) return true;
	return false;
}

bool Overload::operator!=(const Overload &arr) {
	return (currSize != arr.currSize);
}

Overload::~Overload() {
	delete[] pBegin;
}