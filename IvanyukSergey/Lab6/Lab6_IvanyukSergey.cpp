#include "pch.h"
#include "Worker.h"
#include "Administration.h"
#include "Engineer.h"
#include "CadreList.h"

void classes() {
	std::cout << "List:" << std::endl << std::endl;
	Administration cadre0("Sergey", "Sergeevich", "Ivanyuk", 2000, "Manager");
	cadre0.Add();
	Worker cadre1("Vladimir", "Dmitrievich", "Potapov", 2000, "19 year");
	cadre1.Add();
	Worker cadre11("Luiz", "Andreeevich", "Litt", 1986, "12 year");
	cadre11.Add();
	Worker cadre12("Mike", "Alekseevich", "Ross", 1989, "7 year");
	cadre12.Add();
	Engineer cadre2("Andrey", "Nikitich", "Hardman", 1995, "3 year", "Higher technical education");
	cadre2.Add();
	Engineer cadre21("Liza", "Vladimirovna", "Pearson", 1991, "6 year", "Higher technical education");
	cadre21.Add();
	Administration cadre3;
	cadre3.Add();
	Worker cadre31;
	cadre31.Add();
	Engineer cadre32;
	cadre32.Add();
	CadreList::View();
}

int main() {
	classes();
	return 0;
}