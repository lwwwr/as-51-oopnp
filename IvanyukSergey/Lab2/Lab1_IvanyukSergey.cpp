#include "pch.h"
#include <iostream>
#include <string>
using namespace std;
void words(string s, int k, int m);
int main() {
	string s;
	cout << "Write text with a dot at the end: ";
	getline(cin, s);
	while (s.length() >= 256) {
		cout <<"String exceeds 255 characters, please try again: ";
		getline(cin, s);
	}
	words(s, s.length(), 0);
	return 0;
}
void words(string s, int k, int m) {
	string g = "aeiouAEIOU";
	int h = g.length();
	cout << "All words that do not contain vowels: ";
	for (int i = 0; i < (s.length()); i++) {
		for (int j = 0; j <= h; j++) {
			if (s[i] == g[j]) {
				while (s[i] != ' '&& i < k) {
					i++;
				}
				m = i + 1;
				break;
			}
		}
		while (m <= i && (s[i] == ' ' || i == k - 1)) {
			if (m > 0 && (s[m - 1] == ' '&& s[m] == ' ')) {
				m++;
				continue;
			}
			cout << s[m];
			m++;
		}
	}
}