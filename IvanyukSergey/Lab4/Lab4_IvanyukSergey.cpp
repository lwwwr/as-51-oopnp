#include "pch.h"
#include <iostream>
#include <stack>
#include <fstream>

void output(ifstream &file) {
	for (int i = 1; !file.eof(); i++) {
		string str;
		getline(file, str);
		std::cout << "Line " << i << ": " << str << endl;
	}
	std::cout << "\n\n";
	file.seekg(0);
}

int main() {
	ifstream file("file.txt");
	if (!file.is_open()) {
		std::cout << "The file could not be opened!\n";
	}
	output(file);
	int k = 0;
	while (!file.eof()) {
		int i = 0, pf = 2, pd = 2;
		stack<char> bracket;
		stack<int> position_number;
		string str;
		getline(file, str);
		std::cout << "Line " << ++k << ": ";
		int num_square_brackets = 0, num_round_brackets = 0, br = 0, qu = 0, d_qu = 0;
		while (str[i]) {
			switch (str[i]) {
			case '(': {
				num_round_brackets++; i++; break;
			}
			case ')': {
				num_round_brackets--; i++; break;
			}
			case '{': {
				number_fig_brackets++; i++; break;
			}
			case '}': {
				number_fig_brackets--; i++; break;
			}
			case '[': {
				num_square_brackets++; i++; break;
			}
			case ']': {
				num_square_brackets--; i++; break;
			}
			case '\'': {
				number_quote++; i++; break;
			}
			case '\"': {
				number_double_quote++; i++; break;
			}
			default: i++;
			}
		} // counts the number of brackets, etc.
		for (i = 0; i < str.size(); i++) {
			if (str[i] == '(' || str[i] == '[' || str[i] == '{' || (str[i] == '\''&&--pf) || (str[i] == '\"'&&--pd) || (str[i] == '/'&&str[i + 1] == '*')) {
				bracket.push(str[i]);
				position_number.push(i);
			} // pushes onto the stack a symbol and its position
			else if (!bracket.empty()) {
				if (str[i] == ')') {
					if (bracket.top() == '(') {
						bracket.pop();
						position_number.pop();
						continue;
					}
					else if (num_round_brackets != 0) {
						std::cout << "Error! Close or delete the symbol \"" << str[i] << "\" in the position: " << i + 1 << ".\n";
						break;
					}
					else {
						std::cout << "Error! Close or delete the symbol \"" << bracket.top() << "\" in the position: " << position_number.top() + 1 << ".\n";
						break;
					}
				}
				if (str[i] == ']') {
					if (bracket.top() == '[') {
						bracket.pop();
						position_number.pop();
						continue;
					}
					else if (num_square_brackets != 0) {
						std::cout << "Error! Close or delete the symbol \"" << str[i] << "\" in the position: " << i + 1 << ".\n";
						break;
					}
					else {
						std::cout << "Error! Close or delete the symbol \"" << bracket.top() << "\" in the position: " << position_number.top() + 1 << ".\n";
						break;
					}
				}
				if (str[i] == '}') {
					if (bracket.top() == '{') {
						bracket.pop();
						position_number.pop();
						continue;
					}
					else if (number_fig_brackets!= 0) {
						std::cout << "Error! Close or delete the symbol \"" << str[i] << "\" in the position: " << i + 1 << ".\n";
						break;
					}
					else {
						std::cout << "Error! Close or delete the symbol \"" << bracket.top() << "\" in the position: " << position_number.top() + 1 << ".\n";
						break;
					}
				}
				if (str[i] == '\'') {
					if (bracket.top() == '\'') {
						bracket.pop();
						position_number.pop();
						pf = pf + 2;
						continue;
					}
					else if (number_quote % 2) {
						std::cout << "Error! Close or delete the symbol \"" << str[i] << "\" in the position: " << i + 1 << ".\n";
						break;
					}
					else {
						std::cout << "Error! Close or delete the symbol \"" << bracket.top() << "\" in the position: " << position_number.top() + 1 << ".\n";
						break;
					}
				}
				if (str[i] == '\"') {
					if (bracket.top() == '\"') {
						bracket.pop();
						position_number.pop();
						pd = pd + 2;
						continue;
					}
					else if (number_double_quote % 2) {
						std::cout << "Error! Close or delete the symbol \"" << str[i] << "/\" in the position: " << i + 1 << ".\n";
						break;
					}
					else {
						std::cout << "Error! Close or delete the symbol \"" << bracket.top() << "\" in the position: " << position_number.top() + 1 << ".\n";
						break;
					}
				}
				if (str[i] == '*'&&str[i + 1] == '/') {
					if (bracket.top() == '/') {
						bracket.pop();
						position_number.pop();
						continue;
					}
					else {
						std::cout << "Error! Close or delete the symbol \"" << str[i] << "/\" in the position: " << i + 1 << ".\n";
						break;
					}
				}
			}
			else if (str[i] == '*'&&str[i + 1] == '/') {
				std::cout << "Error! Close or delete the symbol \"" << str[i] << "/\" in the position: " << i + 1 << ".\n";
				break;
			}
			else if (str[i] != '(' && str[i] != '[' && str[i] != '{' && str[i] != '\'' && str[i] != '\"'&&str[i] != ')' && str[i] != ']' && str[i] != '}') continue; // skipping all unnecessary characters
			else {
				std::cout << "Error! Close or delete the symbol \"" << str[i] << "\" in the position: " << i + 1 << ".\n";
				break;
			}
		}
		if (i == str.size()) {
			if (bracket.empty()) std::cout << "This line is correct!\n";
			else std::cout << "Error! Close or delete the symbol \"" << bracket.top() << "\" in the position: " << position_number.top() + 1 << ".\n";
		}
	}
	file.close();
	return 0;
}