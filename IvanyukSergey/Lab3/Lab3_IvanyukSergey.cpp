#include <iostream>
#include <QString>
#include <QTextStream>
QTextStream Qcout(stdout);
QTextStream Qcin(stdin);
using namespace std;
void words(QString s, int m);
int main() {
    QString s;
    cout << "Write text with a dot at the end: ";
    s = Qcin.readLine();
    while (s.length() >= 256) {
        cout <<"String exceeds 255 characters, please try again: ";
        s = Qcin.readLine();
    }
    words(s, 0);
    return 0;
}
void words(QString s, int m) {
    QString g = "aeiouAEIOU",str;
    cout << "All words that do not contain vowels: ";
    for (int i = 0; i < s.length(); i++) {
        for (int j = 0; j <= g.length(); j++) {
            if (s[i] == g[j]) {
                while (s[i] != ' '&& i < s.length()) {
                    i++;
                }
                m = i + 1;
                break;
            }
        }
        if (s[i] == ' ' || i == (s.length()-1)) {
            str.append(s.mid(m, i-m+1));
            m=i;
        }
    }
    Qcout<<str.simplified();
}