#include "pch.h"
#include <iostream>
using namespace std;
float floatt();
double doublee();
void task2();
int main() {
	cout.precision(10);
	cout << "Task 1:\nfloat: " << floatt() << endl;
	cout << "double: " << doublee() << endl;
	task2();
	return 0;
}
float floatt() {
	float a = 1000, b = 0.0001, f, f1, f2, f3, f4, f5;
	f1 = (a + b) * (a + b) * (a + b);
	f2 = a * a * a;
	f3 = 3 * a * a * b;
	f4 = 3 * a * b * b;
	f5 = b * b * b;
	f = (f1 - (f2 + f3)) / (f4 + f5);
	return f;
}
double doublee() {
	double a = 1000, b = 0.0001, d, d1, d2, d3, d4, d5;
	d1 = (a + b) * (a + b) * (a + b);
	d2 = a * a * a;
	d3 = 3 * a * a * b;
	d4 = 3 * a * b * b;
	d5 = b * b * b;
	d = (d1 - (d2 + d3)) / (d4 + d5);
	return d;
}
void task2() {
	int m, n;
	cout << "Task 2:\nEnter m: ";
	cin >> m;
	cout << "Enter n: ";
	cin >> n;
	cout << "1)n---m = " << (n-- - m) << endl;
	cout << "2)m--<n = " << (m-- < n) << endl;
	cout << "3)n++>m = " << (n++ > m) << endl;
}