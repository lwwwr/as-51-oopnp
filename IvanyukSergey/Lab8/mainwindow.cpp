#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    pUi(new Ui::MainWindow){
    pUi->setupUi(this);
    setFixedSize(600,600);
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()), Qt::QueuedConnection);
    pTimer->setInterval(100);
    pTimer->start();
    pUi->setupUi(this);
}

MainWindow::~MainWindow(){
    delete pUi;
}

void MainWindow::changeEvent(QEvent *event){
    QMainWindow::changeEvent(event);
    switch (event->type()){
    case QEvent::LanguageChange:
        pUi->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    QRect rect(0, 0, 70, 30);
    painter.setPen(QPen(Qt::black, 3));
    rect.moveTo(QPoint(x+200, y+200));
    painter.drawRect(x+200, y+180, 70, 30);
    painter.setPen(QPen(Qt::green, 3));
    painter.drawRect(rect);
    x+= 5;
    y-= abs(pow(x, 3/2))/20;
    painter.rotate(90);
    QFont NewFont;
    NewFont.setFamily("Courier New");
    NewFont.setPixelSize(45);
    painter.setFont(NewFont);
    painter.setPen(QPen(Qt::black, 3));
    painter.drawText(QPoint(30, -20),"lab 8 IvanyukSergey");
}