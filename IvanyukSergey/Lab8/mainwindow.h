#include <QMainWindow>
#include <QPainter>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow:public QMainWindow {
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);
protected:
    void changeEvent(QEvent *event);
private:
    Ui::MainWindow *pUi;
    QTimer *pTimer;
    int x = 0, y = 400;
};