#pragma once

#include <QMainWindow>
#include "QPainter"
#include "QPen"
#include "QFont"
#include "QTimer"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *pParent = nullptr);
    ~MainWindow();
    void    paintEvent(QPaintEvent *pEvent);

private:
    Ui::MainWindow  *pUi;
    double  aPointX = 0;
    double  aPointY = 0;
    int     aAccelerationPoint = 5500;
};
