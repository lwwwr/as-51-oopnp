#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

const static QString gTextLab =
        "L\nA\nB\n8\n \nQ\nT\nP\na\ni\nn\nt\ne\nr";
static QTimer *gpTimer = new QTimer();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    pUi(new Ui::MainWindow) {
    pUi->setupUi(this);
    setFixedSize(700,700);
    connect(
        gpTimer,
        SIGNAL(timeout()),
        this,
        SLOT(repaint()),
        Qt::QueuedConnection
    );
    gpTimer->start(50);
}

void MainWindow::paintEvent(QPaintEvent *pEvent) {
    QFont       fontDemiBold;
    QPainter    labelLabPainter(this);
    fontDemiBold.setPixelSize(30);
    fontDemiBold.setWeight(63);
    labelLabPainter.setPen( QPen(Qt::darkBlue, 4 ) );
    labelLabPainter.setFont(fontDemiBold);
    labelLabPainter.translate(15, 20);
    labelLabPainter.drawText(
                rect(),
                Qt::AlignVCenter,
                gTextLab
    );
    QPainter figurePainter(this);
    figurePainter.setRenderHint(QPainter::Antialiasing, true);
    aPointX = sin(aAccelerationPoint)
            * aAccelerationPoint * 0.05;
    aPointY = cos(aAccelerationPoint)
            * aAccelerationPoint * 0.05;
    if(aAccelerationPoint < 1) {
        gpTimer->stop();
    }
    aAccelerationPoint -=50;
    QPolygon darkCyanPedestal;
    darkCyanPedestal << QPoint(aPointX + 300, aPointY + 360);
    darkCyanPedestal << QPoint(aPointX + 300, aPointY + 320);
    darkCyanPedestal << QPoint(aPointX + 370, aPointY + 320);
    darkCyanPedestal << QPoint(aPointX + 370, aPointY + 300);
    darkCyanPedestal << QPoint(aPointX + 440, aPointY + 300);
    darkCyanPedestal << QPoint(aPointX + 440, aPointY + 330);
    darkCyanPedestal << QPoint(aPointX + 510, aPointY + 330);
    darkCyanPedestal << QPoint(aPointX + 510, aPointY + 360);
    figurePainter.setPen(QPen(Qt::black, 1));
    figurePainter.setBrush(Qt::darkCyan);
    figurePainter.drawConvexPolygon(darkCyanPedestal);
    QRect lightGrayRect(
                aPointX + 390,  //coordinate x
                aPointY + 199,  //coordinate y
                30,             //weight
                100             //height
            );
    figurePainter.setPen(QPen(Qt::black, 1));
    figurePainter.setBrush(Qt::lightGray);
    figurePainter.drawRect(lightGrayRect);

}

MainWindow::~MainWindow() {
    delete gpTimer;
    delete pUi;
}
