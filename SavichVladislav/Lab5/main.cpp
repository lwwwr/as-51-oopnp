#include <iostream>
#include <QVector>

void Task1(const QVector<int> &vector) {
QVector<int> vect;
QVector<int> vect1;
QVector<int> vect2;
foreach(int elem, vector)
if(elem <= 0) vect1 += elem;
else
vect2 += elem;
vect1 += vect2;
std::cout << "result of TASK1: ";
foreach(int elem, vect) std::cout << elem << " ";
std::cout << std::endl;
foreach(int elem, vect1) std::cout << elem << " ";
std::cout << std::endl;
}

void Task2 (const QVector<int> &vector) {
int sum = 0;
QVector<int>::const_iterator i;
for (i = vector.begin(); i != vector.end(); ++i)
if (*i >= 0)
sum =sum + *i;else break;
std::cout << "rezult of TASK2: " << sum;
}

int main() {
QVector <int> vector = {1, 2, -1, 4, 5, 6, -8, 1, 0, 1, 1};
std::cout << "vector: ";
foreach(int elem, vector) std::cout << elem << " ";
std::cout << "\n\n";
Task1(vector);
Task2(vector);
}
