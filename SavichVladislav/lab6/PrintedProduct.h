#ifndef PRINTEDPRODUCT_H
#define PRINTEDPRODUCT_H
#include <string>
#include <iostream>
#include <map>

using namespace std;

static multimap<int, string> PrintedProductList;

class PrintedProduct
 {
protected:
	string name;
	string madeIn;
	enum eMagazine { ePrintedProduct = 0, eBook = 1, eTextBook = 2, eMagazine = 3 } currType;
public:
	PrintedProduct(string = "NO_SET", string = "NO_SET");
	virtual ~PrintedProduct();
	virtual void Add() = 0;
	virtual string Show() = 0;
	void ShowList();
	};

PrintedProduct::PrintedProduct(string _name, string _madeIn)
 {
	name = _name;
	madeIn = _madeIn;
	currType = ePrintedProduct;
	}

PrintedProduct::~PrintedProduct()
 {
	name.clear();
	madeIn.clear();
	delete this;
	}

void PrintedProduct::Add()
 {
	}
void PrintedProduct::ShowList()
 {
	for (int i = 1; i < 4; i++) {
		switch (i) {
		case eBook: cout << "Books: \n"; break;
		case eMagazine: cout << "Magazines: \n"; break;
		case eTextBook: cout << "Text Books: \n"; break;
		
		}
		for (auto item : PrintedProductList)
			 {
			if (item.first == i)
				 cout << item.second << endl;
			}
		cout << "=================\n";
		
	}
	
	}
#endif // PRINTEDPRODUCT_H