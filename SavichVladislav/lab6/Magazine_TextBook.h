﻿﻿#include "book.h"
#ifndef MAGAZINE_H
#define MAGAZINE_H

class Magazine : public Book
 {
protected:
	string type;
public:
	Magazine(string = "NO_SET", string = "NO_SET", string = "NO_SET", double = 0);
	string Show();
	void Add();
	};

Magazine::Magazine(string _name, string _madeIn, string _type, double _price) : Book(_name, _madeIn, _price)
 {
	type = _type;
	currType = eMagazine;
	}
string Magazine::Show()
{
	return name + " " + madeIn + " " + type + " " + to_string(price);
	}

void Magazine::Add()
 {
	PrintedProductList.emplace(currType, this->Show());
	}
#endif
 #ifndef TEXTBOOK_H
 #define TEXTBOOK_H
 class TextBook : public Book
{
protected:
	string type;
public:
	TextBook(string = "NO_SET", string = "NO_SET", string = "NO_SET", double = 0);
	string Show();
	void Add();
	};
TextBook::TextBook(string _name, string _madeIn, string _type, double _price) : Book(_name, _madeIn, _price)
 {
	type = _type;
	currType = eTextBook;
	}
string TextBook::Show()
{
	return name + " " + madeIn + " " + type + " " + to_string(price);
	}

void TextBook::Add()
 {
	PrintedProductList.emplace(currType, this->Show());
	}

#endif // MAGAZINE_H