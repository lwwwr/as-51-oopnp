#ifndef BOOK_H
#define BOOK_H
#include "PrintedProduct.h"

class Book : public PrintedProduct
{
protected:
	double price;
public:
	Book(string = "NO_SET", string = "NO_SET", double = 0);
	virtual void Add();
	virtual string Show();
	};

Book::Book(string _name, string _madeIn, double _price) : PrintedProduct(_name, _madeIn)
{
	price = _price;
	currType = eBook;
	}

void Book::Add()
 {
	PrintedProductList.emplace(currType, this->Show());
	}

string Book::Show()
 {
	return name + " " + madeIn + " " + to_string(price);
	}
#endif // BOOK_H