﻿#include <iostream>
#include "Magazine_TextBook.h"

using namespace std;

int main()
 {
	Magazine myMagazine("Magazine", "France", "Elle", 19.55);
	myMagazine.Add();
	TextBook myTextBook("Pravda", "CCCP", "Newspaper", 1.27);
	myTextBook.Add();
	Book myBook("Svet", "Russia", 154);
	myBook.Add();
	myTextBook.ShowList();

return 0;
}