#include "pch.h"
#include <cstring>
#include <iostream>
using namespace std;

class Array
{
private:
	char *OurArr;
	static const int MAX = 100;
	int arrSize;
	void Copy(const char*, int);
	void Continue(char);
public:
	Array();
	Array(const Array&);
	Array(const char*, int);
	~Array();
	void Input();
	void Print();
	bool operator > (char ch); 
	Array& operator * (const Array&);
	bool operator < (const Array&); 
};

bool Array::operator<(const Array& obj) 
{
	if (arrSize < obj.arrSize) return false;
	bool ok = false;
	for (int i = 0; i < obj.arrSize; i++) {
		for (int j = 0; j < arrSize; j++) {
			if (obj.OurArr[i] == OurArr[j]) {
				ok = true;
				break;
			}
		}
		if (ok) ok = false;
		else return false;
	}
	return true;
}

Array& Array::operator*(const Array& obj)
{
	Array *newArr = new Array;
	int k = 0;
	for (int i = 0; i < obj.arrSize; i++)
		for (int j = 0; j < arrSize; j++) {
			if (obj.OurArr[i] == OurArr[j]) {
				newArr->Continue(OurArr[j]);
				break;
			}
		}
	return *newArr;
}

bool Array::operator>(char ch) 
{
	for (int i = 0; i < arrSize; i++)
		if (ch == OurArr[i]) return true;
	return false;
}

Array::Array(const char *bufArr, int size) : OurArr(nullptr), arrSize(size)
{
	Copy(bufArr, size);
};

void Array::Copy(const char *bufArr, int size) 
{
	if (size > MAX) return;
	if (OurArr) delete OurArr;
	arrSize = size;
	OurArr = new char[size];
	for (int i = 0; i < size; i++)
		OurArr[i] = bufArr[i];
}

Array::Array() : OurArr(nullptr), arrSize(0) {};

Array::Array(const Array &obj) 
{
	Copy(obj.OurArr, obj.arrSize);
}

Array::~Array() 
{
	delete[] OurArr;
}

void Array::Input() 
{
	char ch;
	while (arrSize < MAX) {
		cin >> ch;
		if (ch == '.') break;
		else Continue(ch);
	}
}

void Array::Continue(const char ch) 
{
	if ((*this) > ch) return;
	char *pNewArr = new char[arrSize + 1];
	for (int i = 0; i < arrSize; i++)
		pNewArr[i] = OurArr[i];
	pNewArr[arrSize++] = ch;
	delete[] OurArr;
	OurArr = pNewArr;
}

void Array::Print() 
{
	for (int i = 0; i < arrSize; i++)
		cout << OurArr[i] << " ";
	cout << endl;
}

int main() 
{
	int check=0;
	Array arr1;
	cout << "Array1:";
	arr1.Input();
	Array arr2;
	cout << endl << "Array2:";
	arr2.Input();
menu:
	int cont = 0;
	cout << endl << "what are we to do: " << endl << "1 - Show your arrays." << endl << "2 - Affiliation check 'a' to array1." << endl << "3 - Checking array 2 for a subset of array 1." << endl << "4 - Checking intersection of array2 and array1" << endl << "0  - Exit" << endl << "Your choice - ";
	cin >> check;
	switch (check)
	{
	case 1:
	{
		cout << "Arr1: ";
		arr1.Print();
		cout << endl << "Arr2: ";
		arr2.Print();
		cout << endl << "If you want continue, press: 1; else press any button " << endl << "Your choice - ";
		cin >> cont;
		if (cont==1)
			goto menu;
		else break;
	}
	case 2:
	{
		cout << endl << "Affiliation check 'a' to array1: ";
		arr1 > 'a' ? cout << "True" : cout << "False";
		cout << endl << "If you want continue, press 1; else press any button. " << endl << "Your choice - ";
		cin >> cont;
		if (cont == 1)
			goto menu;
		else break;
	}
	case 3:
	{
		cout << endl << "Arr2 is subset arr1: ";
		arr1 < arr2 ? cout << "True\n"  : cout << "False\n";
		cout << endl << "If you want continue, press: 1; else press any button. " << endl << "Your choice - ";
		cin >> cont;
		if (cont == 1)
			goto menu;
		else break;
	}
	case 4:
	{
		cout << endl << "Intersection of arr2 and arr1: ";
		(arr1 * arr2).Print();
		cout << endl << "If you want continue, press: 1; else press any button. " << endl << "Your choice - ";
		cin >> cont;
		if (cont == 1)
			goto menu;
		else break;
	}
	case 0:
	{
		cout << "Thanks for the knowledge! Goodbye." << endl;
		break;
	}
	default:
		cout << "Invalid item, try again." << endl;
		goto menu;
	}
	system("pause");
	return 0;
}