﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();
	void paintEvent(QPaintEvent *event);

protected:
	void changeEvent(QEvent *e);

private:
	Ui::MainWindow *ui;
	QTimer *pTimer;
    int x = 0, y = 600;
};

#endif // MAINWINDOW_H
