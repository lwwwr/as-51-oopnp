﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	{
		ui->setupUi(this);
        setFixedSize(400,400);
		pTimer = new QTimer();
		connect(pTimer, SIGNAL(timeout()), this, SLOT(repaint()),Qt::QueuedConnection);
        pTimer->setInterval(50);
		pTimer->start();
	}
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}
void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QFont serifFont("Gothic", 20);
    painter.setFont(serifFont);
    painter.rotate(90);
    painter.drawText(QPoint(100,-20),"LABOR 8");
    painter.rotate(-90);

    QRect squard1(0, 0, 80, 60);
    QRect squard2(0, 0, 40, 20);
    painter.setPen( QPen( Qt::blue, 3 ) );
    x += 10 ;
    y = x;
    squard1.moveTo(QPoint(x, y));
    squard2.moveTo(QPoint(x, y));
    painter.drawRect(squard1);
    painter.drawRect(squard2);

}
